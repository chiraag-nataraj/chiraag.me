<!DOCTYPE html>
<html lang=en>
  <head>
    <title>Modeling of Negative Index Metamaterials for &#8220;Hide-in-Plain-Sight&#8221;</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php
       $URL = $_SERVER["SERVER_NAME"];
       print '<base href="http://'.$URL.'" />';
       print "\n";
    ?>
    <?php
       function myParse($arr)
       {
         return(explode(" | ", $arr));
       }
       function findIndex($arr, $size, $id)
       {
         for($j = 0; $j < $size; $j++)
         {
            if(in_array($id, $arr[$j]))
            {
              return($j);
            }
         }
       }
       $refs = file("references");
       $refs_split = array_map("myParse", $refs);
       $captions = file("captions");
       $captions_split = array_map("myParse", $captions);
       $refs_size = count($refs);
       $captions_size = count($captions);
       function cite($id)
       {
         global $refs_split;
         global $refs_size;
         $counter = findIndex($refs_split, $refs_size, $id) + 1;
         print '<sup class="cite" id="ref'.$id.'"><a href="nims/index.php#'.$id.'">'.$counter.'</a></sup>';
       }
    ?>
    <?php
       function figplace($id)
       {
         global $captions_split;
         global $captions_size;
         $fig_counter = findIndex($captions_split, $captions_size, $id) + 1;
         print '<figure>'.PHP_EOL;
         print '<img src="nims/Images/'.$id.'.png" class="nimsfig" id="'.$id.'" />'.PHP_EOL;
         print '<figcaption>Figure <a href="nims/index.php#ref'.$id.'">'.$fig_counter.'</a>: '.$captions_split[findIndex($captions_split, $captions_size, $id)][1].'</figcaption>'.PHP_EOL;
         print '</figure>'.PHP_EOL;
       }
       
       function figref($id)
       {
         global $captions_split;
         global $captions_size;
         $num = findIndex($captions_split, $captions_size, $id) + 1;
         print '<a href="nims/index.php#'.$id.'" id="ref'.$id.'">'.$num.'</a>';
       }
    ?>
    <script type="text/javascript" src="../MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    </script>
    <script type="text/javascript">
      MathJax.Hub.Config({
        tex2jax: {
          inlineMath: [['$','$'], ['\\(','\\)']],
          processEscapes: true
        }
      });
      MathJax.Hub.Config({
        TeX: { 
          equationNumbers: {  autoNumber: "AMS"  },
          extensions: ["AMSmath.js"]
        }
      });
    </script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Modeling of NIMs for &#8220;Hide-in-Plain-Sight&#8221;
      </h1>
      <h2>
        Introduction
      </h2>
      <p>
        The realization of an autonomous &#8220;hide-in-plain-sight&#8221; micro-robotic platform of centimeter scale is a goal of Army robotics research.  The goal of obscuring a micro-robotic vehicle from a viewer may be approached with the principles of transformation optics which loosely apply Einstein&#8217;s relativity to electromagnetic theory to conceptualize novel methods to curve the path of light.
      </p>
      <p>
        In prior work, modeling has been utilized to demonstrate a single-layer, wide-angle, negative-index metamaterial (NIM) through to the blue part of the visible spectrum <?php cite("Burgos2010") ?>.   The work uses a two-dimensional array of vertically oriented metal-insulator-metal (MIM) coaxial waveguides, arranged in a dense hexagonal configuration, on a flat slab <?php cite("Burgos2010") ?>.  In addition, metamaterial response has been studied using analytic waveguide modal analysis for single coaxial structures and using finite-difference time-domain (FDTD) simulations for the array of coupled coaxial waveguides.  The goal of the present research is to extend the simulations of the flat-slab NIM structure to curved surfaces.
      </p>
      <p>
        The present research will attempt to produce optical effects using the properties of negative index of refraction materials.  Such optical effects include translocation of the object, multiplication of the object (seeing more than one where there is only one), disguising the true size of the object (making it look bigger or smaller than it actually is).
      </p>
      <p>
        The assumption used in these simulations is that the materials do not absorb any electromagnetic radiation &ndash; all the EM radiation is either transmitted or reflected.
      </p>
      <h2>
        Background
      </h2>
      <h3>
        Concepts
      </h3>
      <h4>
        Permeability of a material
      </h4>
      <p>
        The permeability of a material describes the ability of the material to support the formation of a magnetic field.  It is denoted by the symbol $\mu$.  The permeability of free space, denoted by $\mu_0$, is $4\pi\times10^{-7} \dfrac{\text{newtons}}{\text{amperes}^2}$ <?php cite("Mohr2008") ?>.  Free space is a vacuum.
      </p>
      <h4>
        Permittivity of a material
      </h4>
      <p>
      The permittivity of a material describes how much resistance is encountered when forming an electric field in that material.  It is denoted by the symbol $\epsilon$.  The permittivity of free space, denoted by $\epsilon_0$, is
\begin{align}
  \epsilon_0 &amp;= \frac{1}{c_0^2\mu_0}
\end{align}
where $c_0$, which has an exact value of $2.99792458\times10^8 \dfrac{\text{meters}}{\text{second}}$, is the speed of light in a vacuum <?php cite("Mohr2008") ?>.
      </p>
      <h4>
        Maxwell&#8217;s Equations in Free Space
      </h4>
      <p>
        Maxwell&#8217;s Equations in Free Space are given below:
\begin{align}
  \vec{\nabla}\cdot\vec{E} &amp;= 0 \label{eq:divE} \\
  \vec{\nabla}\cdot\vec{B} &amp;= 0 \label{eq:divB} \\
  \vec{\nabla}\times\vec{E} &amp;= -\frac{\partial\vec{B}}{\partial t} \label{eq:curlE} \\
  \vec{\nabla}\times\vec{B} &amp;= \mu_0\epsilon_0 \frac{\partial \vec{E}}{\partial t} \label{eq:curlB}
\end{align}
<?php cite("Hecht2002") ?>. Equation \eqref{eq:divE} equates the divergence of the electric field with 0.  This implies that there is no net charge in free space.  Equation \eqref{eq:divB} equates the divergence of the magnetic field with 0.  This implies there is no net current in free space as well.  Equation \eqref{eq:curlE} equates the curl of the electric field with the negative time rate of change of the magnetic field (the $\partial t$ symbolizes a partial derivative with respect to time).  Equation \eqref{eq:curlB} equates the curl of the magnetic field with the permittivity of free space multiplied by the permeability of free space multiplied by the time rate of change of the electric field.  This set of equations governs the passage of any electromagnetic radiation through free space.  In addition, these can be used as an approximation for the passage of electromagnetic radiation through air, as the speed of light is almost the same through vacuum and air.  If one solves the set of equations above, one obtains the speed of light defined in terms of the permeability and permittivity:
\begin{align}
  c &amp;= \frac{1}{\sqrt{\mu_0\epsilon_0}}
\end{align}
      </p>
      <h4>
        Index of Refraction of a material
      </h4>
      <p>
        Light, however, does not always travel at the same speed. For example, light travels slower in water than in air, and even slower in diamond. The number which quantifies the speed at which light travels through a given medium is called the index of refraction.  The index of refraction is the ratio of the speed of light in vacuum to the speed of light in a particular material.
\begin{align}
  n^2 &amp;= \epsilon\mu \\
  &amp;= \frac{c^2}{v^2} \label{eq:ior}
\end{align}
<?php cite("Veselago1968") ?> where $\mu$, $\epsilon$, and $v$ are properties of the material for which the index of refraction is sought.  $\mu$ is the material&#8217;s permeability, $\epsilon$ is the material&#8217;s permittivity, and $v$ is the speed of light in the material.  From Equation \eqref{eq:ior}, note that the index of refraction can be positive or negative.  The commonly accepted formula for the index of refraction, given in Equation \eqref{eq:ior-bad}, implies that the index of refraction can only be positive.  Indeed, materials found in nature always have positive indices of refraction.  However, Veselago proved <?php cite("Veselago1968") ?> that a negative index of refraction is within the realm of physical possibility.
\begin{align}
  n &amp;= \frac{c}{v} \label{eq:ior-bad}
\end{align}
      </p>
      <h4>
        Snell&#8217;s Law
      </h4>
      <p>
        Snell&#8217;s Law is illustrated in Figure <?php figref("snell") ?> and given by Equation \eqref{eq:snell}
      </p>
      <?php figplace("snell") ?>
      <p>
\begin{align}
  \label{eq:snell}
  n_1\sin\theta_i &amp;= n_2\sin\theta_t
\end{align}
<?php cite("Hecht2002") ?> where $n_1$ is the index of refraction of the &#8216;outside&#8217; material (usually air), $n_2$ is the index of refraction of the material itself, $\theta_i$ is the angle of incidence (the angle with which the light ray hits the material) as measured from the vertical, and $\theta_t$ is the angle with which the transmitted ray continues through the material.  This law holds true in negative index of refraction metamaterials as well (as it must).  However, the outcome changes slightly.  In Figure <?php figref("snell-neg") ?>, Snell&#8217;s Law is illustrated for an interface between air and a negative index of refraction metamaterial.
      </p>
      <?php figplace("snell-neg") ?>
      <p>
        The variables are the same as before.  This time, however, the transmitted light ray gets refracted to the opposite side - that is, there is a negative angle of refraction.  In addition, note that the direction of wave propagation is reversed.
      </p>
      <h4>
        Fresnel Equations
      </h4>
      <p>
        The Fresnel Equations is a group of equations derived from Maxwell&#8217;s Equations which quantifies how much light is reflected and how much light is transmitted when light passes into a given medium. In order to derive these equations, however, ordinary light must be divided into two different types of polarized light - TE and TM. TE is transverse electric and means that there is no electric field in the plane of incidence. TM is transverse magnetic and means that there is no magnetic field in the plane of incidence <?php cite("Hecht2002") ?>. Figures <?php figref("TE") ?> and <?php figref("TM") ?> show these two different cases. Ordinary, unpolarized light is a mixture of these two modes in equal components.
      </p>
      <?php figplace("TE") ?>
      <?php figplace("TM") ?>
      <p>
        Here, $\vec{E}$ is the electric field, $\vec{B}$ is the magnetic field, and $\hat{k}$ is the direction of wave propagation.  The subscript &#8216;$i$&#8217; means &#8220;initial&#8221;, &#8216;$r$&#8217; means &#8220;reflected&#8221;, and &#8216;$t$&#8217; means &#8220;transmitted&#8221;.  However, Figures <?php figref("TE") ?> and <?php figref("TM") ?> are for ordinary materials.  In negative index of refraction metamaterials, the diagrams change slightly as a result of the reversed direction of wave propagation (&#8220;negative velocity&#8221;) and the reversed refraction angle due to Snell&#8217;s Law.  Figures <?php figref("TE-nim") ?> and <?php figref("TM-nim") ?> are the equivalent figures for negative index of refraction metamaterials. With this modified setup, the question is whether the Fresnel Equations are valid, which indeed they are.
      </p>
      <?php figplace("TE-nim") ?>
      <?php figplace("TM-nim") ?>
      <h3>
        Tools
      </h3>
      <h4>
        Ray Tracing
      </h4>
      <p>
        Ray Tracing is a technique used in order to simulate a viewer&#8217;s perspective of an object. Ray Tracing accomplishes this by tracing rays of light into a scene and recording when the ray hits an object. If the object is translucent, the program will use the Fresnel Equations in order to compute how much of the light is reflected and how much is transmitted based on the index of refraction and angle of incidence. Normally, forward ray tracing &ndash; that is, from the light to the eye &ndash; is extremely inefficient. Therefore, most ray tracers, by default, do backward ray tracing &ndash; that is, from the eye to the light. However, in order to simulate certain effects such as caustics, the projection of rays reflected off of one surface onto another, it is imperative that forward ray tracing be used, as backward ray tracing will not accurately simulate those effects &ndash; it a limitation of that technique.
      </p>
      <h2>
        Methods
      </h2>
      <p>
        For the purposes of this research, the ray tracing software <a href="http://povray.org" target="_blank">POV-Ray</a> was used.  Five different structures were investigated: Cube (Figure <?php figref("cube") ?>), Sphere (Figure <?php figref("sphere") ?>), Slab (Figure <?php figref("slab") ?>), Curved Slab (Figure <?php figref("curved-slab") ?>), and Saddle Slab (Figure <?php figref("saddle-slab") ?>).  Four different material gradient scenarios were investigated of each of those structures: Solid (Figure <?php figref("solid") ?>), Gradient (2 types - Figures <?php figref("grad") ?> and <?php figref("grad2") ?>), Embedded Object (Figure <?php figref("embedded") ?>), and Surface Gradient (Figure <?php figref("surface-grad") ?>). Several simulations were run of each scenario with each structure.
      </p>
      <h2>
        Results
      </h2>
      <h3>
        Solids
      </h3>
      <p>
        The results shown in Figures <?php figref("solid-cube-ior--0.1-x") ?> and <?php figref("solid-cube-ior--0.1-z") ?> indicate that given a sufficiently low index of refraction (IOR) (-0.1 in this case), a solid cube looks like it contains a spherical object, but only from certain angles.  From others, as in Figure <?php figref("solid-cube-ior--0.1-x-z") ?>, it looks like it contains nothing at all.  Note that the cube is <em>not</em> transparent - it is merely reflecting from the floor (checker pattern) and sky (black). In Figures <?php figref("solid-sphere-ior--0.5-x") ?>-<?php figref("solid-sphere-ior--1.5-z") ?>, a solid sphere appears to contain a sphere (or multiple spheres) inside it.  This effect occurs for more than one (negative) index of refraction - in this case, -0.5 as well as -1.5. Figures <?php figref("solid-slab-ior--1-over-sqrt-2-x") ?> and <?php figref("solid-slab-ior-1-over-sqrt-2-x") ?> show that while the slab with an IOR of $\dfrac{1}{\sqrt{2}}$ appears beveled, the slab with an IOR of $-\dfrac{1}{\sqrt{2}}$ is virtually invisible. Figures <?php figref("solid-curved-slab-ior--1-over-sqrt-2-x") ?> and <?php figref("solid-curved-slab-ior-1-over-sqrt-2-x") ?> show that the curved slab with the negative index of refraction disturbs the background less than the curved slab with the positive index of refraction.  The same can be said of Figures <?php figref("solid-saddle-slab-ior--1-over-sqrt-2-x") ?> and <?php figref("solid-saddle-slab-ior-1-over-sqrt-2-x") ?>.
      </p>
      <h3>
        Gradients
      </h3>
      <p>
        In Figure <?php figref("gradient-cube-ior--2.3--1") ?>, one can see how the apparent color of the cube changes with respect to Figure <?php figref("gradient-cube-cmp") ?>. In Figure <?php figref("gradient-sphere-ior--1.1--0.9") ?>, a sphere with a gradient like that of Figure <?php figref("grad") ?> in the IOR managed to disguise the cube seen in Figure <?php figref("gradient-sphere-cmp") ?>. In Figures <?php figref("gradient-slab-ior--1.1--1") ?> and <?php figref("gradient-slab-ior--2.3--1") ?>, the slab with a gradient in the IOR makes the cube shown in <?php figref("gradient-slab-cmp") ?> look larger.  Even more remarkable is that Figure <?php figref("gradient-slab-ior--1.1--1") ?> manages to make the object appear larger without hinting of its presence.  By contrast, in Figure <?php figref("gradient-slab-ior--2.3--1") ?>, the gradient leaves its mark on the object &ndash; note the slight white squares patterned on the cube. In Figures <?php figref("gradient-curved-slab-ior--1.1--0.9") ?> and <?php figref("gradient-curved-slab-ior--2.3--1") ?>, the curved slab with a gradient in IOR working outward (as in Figure <?php figref("grad") ?>) can change the number of objects perceived as compared to Figure <?php figref("gradient-curved-slab-cmp") ?>.  In Figures <?php figref("gradient-curved-slab-layer-ior-0.1-1") ?> and <?php figref("gradient-curved-slab-layer-ior--2.3--1") ?>, the curved slab with a gradient in IOR through the thickness (as in Figure <?php figref("grad2") ?>) can change the perceived shape of the object as compared to Figure <?php figref("gradient-curved-slab-cmp") ?>. In Figures <?php figref("gradient-saddle-slab-layer--1.1--0.9") ?> and <?php figref("gradient-saddle-slab-layer--2.3--1") ?>, the saddle slab with a gradient in IOR through the thickness can change how the object is perceived as compared to Figure <?php figref("gradient-saddle-slab-cmp") ?> &ndash; the cube is compressed in the x direction, making it appear as a rectangular prism.
      </p>
      <h3>
        Embedded Objects
      </h3>
      <p>
        In Figure <?php figref("embedded-cube-ior-1-over-sqrt-2") ?>, one can see that an object (Figure <?php figref("embedded-cube-cmp") ?>) can be all but hidden by covering it with a material with an index of refraction of $\dfrac{1}{\sqrt{2}}$.  Notice how a negative index of refraction is not necessarily required for this effect.  However, this material only disguises the object for a limited amount of angles - it is perfect at 45&deg;, but imperfect at any angle greater than 50&deg; or less than 40&deg;.  However, Figure <?php figref("embedded-cube2-ior--0.001") ?> shows an object (Figure <?php figref("embedded-cube2-cmp") ?>) being hidden by covering it with a material with an index of refraction of -0.001.  This material, on the other hand, works at all angles. In Figure <?php figref("embedded-sphere-cube-ior--2.3") ?>, one can see how a cube (Figure <?php figref("embedded-sphere-cube-cmp") ?>) has been made to superficially resemble a sphere by enclosing it in a spherical material of radius $\sqrt{3} + 0.00003$ cm.  In Figure <?php figref("embedded-sphere-sphere-ior--0.5") ?>, one can see how a sphere (Figure <?php figref("embedded-sphere-sphere-cmp") ?>) appears smaller when encased in a spherical material of radius 1.00003 cm. In Figure <?php figref("embedded-slab-ior--0.1") ?>, one can see how a cube (Figure <?php figref("embedded-slab-cmp") ?>) appears as a sphere when a slab 5 mm thick with an index of refraction of -0.1 is placed 2 cm in front of it.  In addition, the pattern on the object is extremely distorted. In Figure <?php figref("embedded-curved-slab--1-over-sqrt-2") ?>, the object (Figure <?php figref("embedded-curved-slab-cmp") ?>) has been distorted horizontally by a curved slab 3 mm thick with an index of refraction of $-\dfrac{1}{\sqrt{2}}$.  In Figure <?php figref("embedded-curved-slab--0.01") ?>, the object (Figure <?php figref("embedded-curved-slab-cmp") ?>) disappears when placed behind a curved slab with an index of refraction of -0.01 stretched by a factor of 1.5 in the x direction. In Figure <?php figref("embedded-saddle-slab-ior--2.3") ?>, it appears that there is more than one object.  This effect is displayed when the object (Figure <?php figref("embedded-saddle-slab-cmp") ?>) is placed behind a saddle slab 300 nm thick with an index of refraction of -2.3.
      </p>
      <h3>
        Surface Gradients
      </h3>
      <p>
        In Figure <?php figref("surface-gradient-cube-gradient-ior--1--2.3") ?>, the results show that while the object (Figure <?php figref("surface-gradient-cube-cmp") ?>) does look a bit larger and does have lines on it, as well as reflecting a lot more light, there is not much difference between the two figures. In Figure <?php figref("surface-gradient-slab-gradient-ior--1.1--0.9") ?>, the object (Figure <?php figref("surface-gradient-slab-cmp") ?>) appears larger when placed behind a slab 300 nm thick with a surface gradient from -1.1 (inside) to -0.9 (outside).
      <h2>
        Challenges
      </h2>
      <h3>
        Ray Tracing
      </h3>
      <p>
        At the outset of this project, little was known about the capability of POV-Ray ray tracing software to capture the effect of negative index of refraction materials for the purpose of simulating &#8220;hide-in-plain-sight&#8221; effects.  For example, it was not known how to force POV-Ray to automatically calculate the reflectance and transmittance at any given point given the index of refraction.  In addition, the precision which POV-Ray employed was inefficient to obtain solutions in certain cases.  By adjusting the maximum transmittance value to 1, and the maximum reflectance to 1, and enforcing a requirement to conserve energy as well as to use the Fresnel Equations, POV-Ray was adequate to automatically calculate the reflectance and transmittance at any given point.  However, the issues concerning precision were harder to fix.  However, because POV-Ray is open source, the code was accessible, allowing changes to be made to the apropriate values, and recompiling in order to obtain the desired precision.  Hence, both of these major problems have been addressed.
      </p>
      <h3>
        Finite Difference Time Domain
      </h3>
      <p>
        An elaborate technique for the estimation of the effective refractive index of a flat slab of negative index of refraction material has been presented in prior work <?php cite("Burgos2010") ?>, where the effective IOR calculation depends on the solution to Maxwell&#8217;s equations in the coordinates of a specific feature.  The solution to Maxwell&#8217;s equations may be approximated via finite-difference time domain (FDTD) methods using numerical programs such as Lumerical and MEEP.  In the present study, an effective refractive index is needed to conduct the ray tracing calculations using the POV-Ray software.  While the technique for calculating the effective refractive index begins with a feature design in an FDTD program combined with tabular data available in the literature, a challenge exists due to the fact that POV-Ray requires a specific IOR as an input without knowing if a feature design exists that will provide the necessary IOR.  A gap exists between the capability to design features in FDTD software and desired input to ray tracing software.  A need exists for efficient software solutions that bridge the gap between design of NIM configurations using FDTD methods and visualization using ray tracing methods.
      </p>
      <h2>
        Discussion
      </h2>
      <p>
        As one can see, many different optical effects can theoretically be produced by using metamaterials.  A negative index of refraction is not always required, as seen in Figures <?php figref("embedded-cube-ior-1-over-sqrt-2") ?> and <?php figref("gradient-curved-slab-layer-ior-0.1-1") ?>.  As seen in Figures <?php figref("embedded-cube-ior-1-over-sqrt-2") ?>, <?php figref("embedded-cube2-ior--0.001") ?>, and <?php figref("embedded-curved-slab--0.01") ?>, an object, here a cube, can be rendered nearly invisible.  As seen in Figures <?php figref("gradient-sphere-ior--1.1--0.9") ?>, <?php figref("gradient-curved-slab-layer-ior-0.1-1") ?>, <?php figref("gradient-curved-slab-layer-ior--2.3--1") ?>, <?php figref("gradient-saddle-slab-layer--1.1--0.9") ?>, <?php figref("gradient-saddle-slab-layer--2.3--1") ?>, <?php figref("embedded-sphere-cube-ior--2.3") ?>,  <?php figref("embedded-curved-slab--1-over-sqrt-2") ?>, and <?php figref("embedded-slab-ior--0.1") ?>, an object&#8217;s apparent shape can be changed.  As seen in Figures <?php figref("gradient-slab-ior--1.1--1") ?>, <?php figref("gradient-slab-ior--2.3--1") ?>,  <?php figref("embedded-sphere-sphere-ior--0.5") ?>, <?php figref("surface-gradient-slab-gradient-ior--1.1--0.9") ?>, an object&#8217;s apparent size can be changed.  As seen in Figures <?php figref("gradient-curved-slab-ior--1.1--0.9") ?>, <?php figref("gradient-curved-slab-ior--2.3--1") ?>, <?php figref("embedded-saddle-slab-ior--2.3") ?>, an object&#8217;s appearance can be multiplied to give the appearance of two or more such objects.  Thus, most of the goals of this project were met.
      </p>
      <p>
        A potential for achieving &#8220;hide-in-plain-sight&#8221; exists based upon the design of a NIM configuration combined with a morphing substructure.  The results presented here indicate a variety of optical effects based upon changes in IOR and macroscopic topology of the NIM slab (i.e., flat slab, curved slab, saddle slab, etc.).  It is conceived that by tailoring the object that is to be hidden to coincide with the optical effects achieved that a drastic effect on the viewer&#8217;s ability to perceive the object may be achieved, thus meeting the goal of hiding the robotic platform in plain sight.  Other unanticipated effects, such as dislocating shadows cast by an object, may also be exploited to achieve the goal of tricking the observer into not being able to see the object directly.
      </p>
      <h2>
        Conclusion and Further Research
      </h2>
      <p>
        In conclusion, the goals of this project were to investigate the use of metamaterials in hiding or disguising a micro-robotic vehicle.  Several optical illusions were targeted: change in apparent shape, change in apparent size, partial or full invisibility, duplication, and translocation.  Most were achieved: change in apparent shape, change in apparent size, duplication, and partial invisibility.  However, these results, demonstrated by ray tracing results, using POV-Ray software, must be validated.  In order to do so efficiently, a new type of software must be created - one that bridges the gap between Finite Difference Time Domain methods and Ray Tracing methods.  Currently, these two types of software exist, but there is no efficient way to directly connect the nanostructure effects investigated using an FDTD software tool to the visualization response at the structural (macroscopic) level as embodied in the ray tracing results.  As such, there has been an effort to validate these results using conventional FDTD techniques, but all signs indicate that the quickest way forward would be to integrate the two methods.
      </p>
      <h2>
        Acknowledgments
      </h2>
      <p>
        I would like to thank the following people and organizations:
      </p>
      <ul class="a">
        <li>
          <strong>
            Dr. Jaret Riddick
          </strong>
          <span>
            for taking the time and effort to mentor me throughout the summer.
          </span>
        </li>
        <li>
          <strong>
            The U.S. Army Research Lab
          </strong>
          <span>
            for providing me with this excellent opportunity.
          </span>
        </li>
        <li>
          <strong>
            Dr. Harry Atwater
          </strong>
          <span>
            for acting as Co-Mentor.
          </span>
        </li>
        <li>
          <strong>
            California Institute of Technology
          </strong>
          <span>
            for providing financial support.
          </span>
        </li>
        <li>
          <strong>
            George Washington University
          </strong>
          <span>
            for providing financial support.
          </span>
        </li>
      </ul>
      <h2>
        Figures
      </h2>
      <?php
         global $captions;
         global $captions_split;
         global $captions_size;
         for ($i = 6; $i < $captions_size; ++$i)
         {
           figplace($captions_split[$i][0]);
         }
      ?>
      <h2>
        References
      </h2>
      <?php
         global $refs;
         global $refs_split;
         global $refs_size;
         print '<ol class="citations">'.PHP_EOL;
         for ($i = 0; $i < $refs_size; ++$i)
         {
           $id = $refs_split[$i][0];
           $cit = $refs_split[$i][1];
           print '<li id="'.$id.'"><a href="nims/index.php#ref'.$id.'">^</a>'.$cit.'</li>'.PHP_EOL;
         }
         print '</ol>'.PHP_EOL;
      ?>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
