<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Home</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Home</h1>
      <h2>Welcome</h2>

<p>Welcome to my blog!</p>

<p>Some recent posts for your reading pleasure:</p>

<h2>Posts</h2>
<ul>
    
        <li>
            <a href="/posts/2020-10-18-rethinking-the-calendar-and-our-measures-of-time.php">Rethinking the Calendar and Our Measures of Time</a> - October 18, 2020
        </li>
    
        <li>
            <a href="/posts/2019-08-21-managing-multiple-email-accounts-with-mutt-and-fetchmail.php">Managing multiple email accounts with Mutt and Fetchmail</a> - August 21, 2019
        </li>
    
        <li>
            <a href="/posts/2019-08-18-kashmir-and-anti-imperialism.php">Kashmir and Anti-Imperialism</a> - August 18, 2019
        </li>
    
        <li>
            <a href="/posts/2019-07-28-converting-raspbian-to-debian-sid.php">Converting Raspbian to Debian sid</a> - July 28, 2019
        </li>
    
        <li>
            <a href="/posts/2019-05-17-privacy-and-security-on-the-modern-web.php">Privacy and security on the modern web</a> - May 17, 2019
        </li>
    
        <li>
            <a href="/posts/2019-05-06-divestment-israel-and-apartheid.php">Divestment, Israel, and Apartheid</a> - May  6, 2019
        </li>
    
        <li>
            <a href="/posts/2019-04-26-encrypted-incremental-backups-to-the-cloud.php">Encrypted, incremental backups to the cloud</a> - April 26, 2019
        </li>
    
        <li>
            <a href="/posts/2019-04-15-why-fair-trade-matters.php">Why Fair Trade Matters</a> - April 15, 2019
        </li>
    
        <li>
            <a href="/posts/2019-04-15-taxes-on-linux.php">Taxes on Linux</a> - April 15, 2019
        </li>
    
        <li>
            <a href="/posts/2019-03-15-venezuela-maduro-and-guaido.php">Venezuela, Maduro, and Guaidó</a> - March 15, 2019
        </li>
    
        <li>
            <a href="/posts/2018-11-13-why-im-voting-union-yes.php">Why I'm Voting</a> - November 13, 2018
        </li>
    
        <li>
            <a href="/posts/2018-07-02-family-separations.php">Family separations</a> - July  2, 2018
        </li>
    
        <li>
            <a href="/posts/2018-03-19-reducing-plastic-use.php">Reducing plastic use</a> - March 19, 2018
        </li>
    
        <li>
            <a href="/posts/2018-01-11-western-countries-and-democracy.php">Western countries and democracy</a> - January 11, 2018
        </li>
    
        <li>
            <a href="/posts/2017-12-04-war-on-christians.php">War on Christians</a> - December  4, 2017
        </li>
    
        <li>
            <a href="/posts/2017-09-24-military-spending.php">Military spending</a> - September 24, 2017
        </li>
    
        <li>
            <a href="/posts/2017-09-17-hillary-clintons-interview.php">Hillary Clinton's interview</a> - September 17, 2017
        </li>
    
        <li>
            <a href="/posts/2017-09-10-terrorism.php">Terrorism</a> - September 10, 2017
        </li>
    
        <li>
            <a href="/posts/2017-08-29-self-segregation.php">Self-segregation</a> - August 29, 2017
        </li>
    
        <li>
            <a href="/posts/2017-08-23-slavery.php">Slavery</a> - August 23, 2017
        </li>
    
        <li>
            <a href="/posts/2017-08-08-that-google-memo.php">That Google memo</a> - August  8, 2017
        </li>
    
        <li>
            <a href="/posts/2017-07-07-ethical-consumption.php">Ethical consumption</a> - July  7, 2017
        </li>
    
        <li>
            <a href="/posts/2017-06-23-philando-castille.php">Philando Castille</a> - June 23, 2017
        </li>
    
        <li>
            <a href="/posts/2017-03-14-discussion-of-the-ahca.php">Discussion of the AHCA</a> - March 14, 2017
        </li>
    
        <li>
            <a href="/posts/2017-02-04-immigration-restrictions-are-american.php">Immigration restrictions are American</a> - February  4, 2017
        </li>
    
        <li>
            <a href="/posts/2016-11-09-what-a-trump-presidency-might-mean.php">What a Trump presidency might mean</a> - November  9, 2016
        </li>
    
        <li>
            <a href="/posts/2016-07-26-free-trade-and-colonialism.php">Free trade and colonialism</a> - July 26, 2016
        </li>
    
</ul>


      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
