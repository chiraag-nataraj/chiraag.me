<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Taxes on Linux</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Taxes on Linux</h1>
      <article>
    <section class="header">
        Posted on April 15, 2019
        
    </section>
    <section>
        <p>Taxes have always been an annoyance, at least here in America where the tax code is as long as your intestine (and just as convoluted!). But taxes are <b>extra</b> annoying on Linux due to the scarce availability of tax prep software on the platform. When I first started doing my taxes, I copped out and used a version of Turbo Tax on a Windows virtual machine I had (my parents had TurboTax already, so it was just a matter of installing it). But that year, I resolved to make taxes work on Linux.</p>
<p>Given that my taxes are generally fairly simple (I’m a graduate student…generally it’s just a W-2 and 1099-INT), the biggest hurdle I faced was finding a way to (easily) fill in the PDF forms. Enter <code>pdftk</code>.</p>
<p><a href="https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/">pdftk</a> is a fantastic piece of software for <em>many</em> reasons. In this case, though, it’s particularly useful because of two commands: <code>dump_data_fields</code> and <code>fill_form</code>. You might be able to see where this is going.</p>
I wrote up the following (very simple) script to help make my life a bit easier:
<pre><code class="language-bash">#!/bin/bash

FILES=( `find -iname '*.pdf' | grep -v -- '-filled' | tr ' ' '&'` )

case "$1" in
    prepare)
	for i in ${FILES[@]}
	do
	    FILE=${i//&/ }
	    FIELDS=${FILE%.pdf}.fields
	    FDF=${FILE%.pdf}.fdf
	    pdftk "$FILE" dump_data_fields_utf8 output "$FIELDS"
	    if [ ! -e "$FDF" ]
	    then
		echo -e '%FDF-1.2\n1 0 obj<</FDF<< /Fields[\n] >> >>\nendobj\ntrailer\n<</Root 1 0 R>>\n%%EOF' > "$FDF"
	    fi	
	done
	;;
    fill)
	for i in ${FILES[@]}
	do
	    FILE=${i//&/ }
	    FDF=${FILE%.pdf}.fdf
	    FILLED=${FILE%.pdf}-filled.pdf
	    pdftk "$FILE" fill_form "$FDF" output "$FILLED"
	done
	;;
    final)
	for i in ${FILES[@]}
	do
	    FILE=${i//&/ }
	    FDF=${FILE%.pdf}.fdf
	    FILLED=${FILE%.pdf}-filled-final.pdf
	    pdftk "$FILE" fill_form "$FDF" output "$FILLED" flatten
	done
	;;
esac
</code></pre>
<p>The script takes one of three arguments (anything else will quietly exit).</p>
<ul>
<li>
<code>prepare</code> generates a <code>fields</code> and <code>fdf</code> file for each PDF file in that directory (that doesn’t have <code>-filled</code> in the name). Of course, it takes care not to <em>overwrite</em> existing FDF files, so you can drop new PDF files in (as you realize your taxes are getting more complicated) and re-run <code>prepare</code> without any issues.
</li>
<li>
<code>fill</code> fills in all PDFs with their corresponding FDF files and outputs to <code><name>-filled.pdf</code>.
</li>
<li>
<code>final</code> fills in all PDFs with their corresponding FDF files, <em>flattens</em> it (permanently merges PDF and FDF content together), and outputs to <code><name>-filled-final.pdf</code>.
</li>
</ul>
<p>My workflow at this point is something as follows:</p>
<ol>
<li>
Download the basic forms from the IRS (1040 and any schedules I think I may need). It often helps for me to look at my previous year’s returns to figure out which forms I used (this year, they somewhat rearranged some of the forms, but it was still helpful). I save these in a subdirectory called <code>Original Forms</code> (I also save form instructions here).
</li>
<li>
Make another subdirectory called <code>Filled Forms</code> and copy the fillable forms to that directory. The rest of my work is largely done in the <code>Filled Forms</code> folder.
</li>
<li>
Run <code>build.sh prepare</code>.
</li>
<li>
Use the <code>.fields</code> file to fill in the FDF file (syntax is just <code>&lt;&lt;/T(FORM ID)/V(VALUE)&gt;&gt;</code> - start inserting those after <code>/Fields[</code>). This is the annoying part, since often the form fields are given nonsensical names - the IRS is actually decent here, since they (largely) sequentially number them, and some of the form fields have meaningful names.
</li>
<li>
Run <code>build.sh fill</code> and open the <code>-filled</code> PDF in a new window (it will auto-reload if your PDF viewer is any good, which makes everything easier).
</li>
<li>
Once I’m <em>actually</em> done, I run <code>build.sh final</code>.
</li>
</ol>
<p>Of course, part of the problem is that to e-file, you will end up using some official software of some sort - I usually use one of the web-based efile providers (much as I loathe the idea of putting this sensitive stuff on their servers…) simply because other options don’t really exist. I still use this method to double-check my federal returns (sometimes the website can be a bit limiting, and you don’t know it until you already know which forms you’ll need) and file my state returns (since for a while, my state situation was actually annoyingly complicated).</p>
<p>This method (of course) will work for <em>any</em> fillable forms - nothing here is actually limited in any way to tax forms. But this workflow tends to work particularly well for me with my taxes, since I can gradually build up my return, adding schedules and forms as I need to.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
