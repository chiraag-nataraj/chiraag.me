<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Slavery</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Slavery</h1>
      <article>
    <section class="header">
        Posted on August 23, 2017
        
    </section>
    <section>
        <p>This notion that somehow only the South benefited from slavery is just bullshit. Their economy was more dependent on slavery, sure. But America as a whole benefited. It gave America the ability to build up an army, “expand” West (claiming more illegally stolen land for ourselves), “purchase” land from the French (as if it were the French’s to sell in the first place), and so on. All that didn’t only happen with the North’s money. It took <em>all</em> of the money available in America. And for too long, America condoned the abhorrent institution of slavery to keep the economy going and make enough money to fuel the dreams of empire. So to pretend that everything we have — <b>everything</b> — isn’t in some way connected to white supremacy and the institution of slavery is naïve at best and disingenuous at worst.</p>
<p>White supremacy is the reason this country exists in the first place — Europeans who landed here thought they inherently had more claim to the land than Native Americans because of the color of their skin. White supremacy and slavery is the reason our economy grew at a rapid pace right after the country was created (stolen from Native Americans). White supremacy is the reason we (as a country) overlooked the institution of slavery (<b>surely</b> African Americans aren’t <em>people</em>!) while it kept our economy growing. And all of our future successes — <b>all</b> of them — were derived from that first period of growing Westward (stealing more land from Native Americans because <b>duh</b> of course white people are superior to Native Americans) and a rapidly growing economy (thanks to slavery and the North’s refusal to condemn it for a long time).</p>
<p>We secured a place in the world’s economy thanks to those things. We had more land to do with as we pleased thanks to those things. We were relatively isolated from hostile forces (including during WWI and WWII) thanks to those things. And everything — from the Industrial Revolution to our success after WWII — depends on those things being true. So to pretend that we’d still be here if white supremacy weren’t a thing is the height of ignorance.</p>
<p>This country was built on white supremacy, and far too many people still believe in it.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
