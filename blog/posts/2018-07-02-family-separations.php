<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Family separations</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Family separations</h1>
      <article>
    <section class="header">
        Posted on July  2, 2018
        
    </section>
    <section>
        <p>Let’s talk family separation for a second. Yes, it’s very clear that what’s happening now is unprecedented. Yes, it’s true that Trump created the policy (not the Democrats). Yes, it’s true that we should be agitating to end it once and for all (somehow I don’t trust that the executive order actually did anything).</p>
<p>But let’s not kid ourselves. We were doing inhumane, awful things to undocumented immigrants at the border well before Trump. And the kinds of stuff Trump says could very easily have been said by a Democrat. Hell, they were. Trump (or rather, Sessions) said that the purpose of separating families was to <a href="https://www.washingtonpost.com/news/the-fix/wp/2018/06/20/the-trump-administration-changed-its-story-on-family-separation-no-fewer-than-14-times-before-ending-the-policy/?noredirect=on">send a message</a>. When asked what the policy should be regarding children seeking asylum from Central American countries, Clinton is clearly on the record stating that we need to send them back to <a href="https://www.huffingtonpost.com/entry/hillary-clinton-child-migrants_us_55d4a5c5e4b055a6dab24c2f">send a message to their parents</a>. Obama, too, has defended his actions regarding family detention, saying that we need to <a href="https://www.aclu.org/blog/smart-justice/mass-incarceration/president-obama-wants-continue-imprisoning-immigrant-families">send the parents a message</a>. I could go back further, but hopefully this is sufficient to show that this kind of dubious rationale is not only not new, but also bipartisan.</p>
<!--more-->
<p>As for the detention itself, the ACLU link above shows that Obama was detaining whole families, treating people fleeing crisis as a national security threat. Is this really so different from Trump’s assertion that Mexicans are rapists, drug dealers, and murderers? One is more obviously racist, for sure. But I would argue that both paint our neighbors to the south as “alien”, “foreign”, “dangerous”. And it would be ludicrous to not believe that this stems from a racial sentiment — I don’t think we’d be talking about “invading hordes of Europeans” were there to be a sudden influx of immigrants from Europe. We’d think of it as natural, good. But these guys from south of the border? Oh yeah, they’re <em>obviously</em> a security threat. It’s sick.</p>
<p>But of course, just “Vote Blue” and that will fix everything, right? Everything was great under Obama — families were detained together! That’s so much better. My biggest fear is that when there’s a Democrat in office, the people who supposedly care now will stop caring. Because everything’s become about Trump the person. This policy is bad because <b>Trump</b> did it. But when Obama was doing something only slightly better, nobody cared. I don’t remember Rachel Maddow openly weeping over this. I don’t remember hours and hours of debates over how we could do something this cruel all over MSNBC and CNN. These people don’t care. It’s all a sham. Either they didn’t know about these policies (hard to believe, since laypeople and activists certainly knew about them) or they did and didn’t care. They’re either shitty journalists or callous people who pretend to care when it suits the partisan narrative.</p>
<p>So please, don’t stop caring once Trump is out of office. The inhumane treatment of undocumented immigrants (including those seeking refugee status and asylum) started a long time ago and the rationales used to justify the treatment then are disturbingly similar to the one being used now.</p>
<p>If you got here and would like to help, you can donate to <a href="https://refugeerights.org/donate/">IRAP</a> and/or the <a href="https://firrp.org/donate/">Florence Immigrant and Refugee Rights Project</a> by following the links.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
