<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Managing multiple email accounts with Mutt and Fetchmail</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Managing multiple email accounts with Mutt and Fetchmail</h1>
      <article>
    <section class="header">
        Posted on August 21, 2019
        
    </section>
    <section>
        Recently, I switched to <a href="https://protonmail.com">ProtonMail</a> as a way of increasing my privacy online and moving away from the “data as currency” model of Google and Facebook (as part of this same effort, I closed my Facebook in favor of creating this blog!). However, most of what I describe here has been my setup for a while due to my penchant for two things:
<ol>
<li>
Wanting to keep my emails backed up (and a local copy always at hand).
</li>
<li>
Wanting a simple but powerful email reader so that it gets out of the way while enabling as much customization as needed.
</li>
</ol>
<p>At the same time, I wanted a system where I could easily add and remove accounts while adding/removing as little configuration as possible, and this informs the way I partition my configuration files (as will be apparent).</p>
<p>As the title suggests, there are two different parts to this setup: <kbd>fetchmail</kbd> and <kbd>mutt</kbd>.</p>
<p>Since <kbd>fetchmail</kbd> does not natively provide for encryption of the configuration file (and the file contains sensitive information), I wrote a wrapper around it which decrypts the file and passes it to <kbd>fetchmail</kbd> via <kbd>stdin</kbd>. That part will not be discussed here and can be set up after glancing at the <kbd>fetchmail</kbd> manual page (in short, do something like <kbd>gpg -d -o - /path/to/encrypted/config | fetchmail -d &lt;n&gt; -f -</kbd>).</p>
As for the configuration file itself, this should serve as a good template:
<pre><code>set postmaster "&lt;local username&gt;"
set bouncemail
set no spambounce
set softbounce
set properties ""
poll &lt;imap server&gt; with proto IMAP
       user 'username' there with password 'password' is '&lt;local username&gt;' here
       options keep ssl sslproto "TLS1+" mda "&lt;mda&gt; $HOME/.local/mail/Gmail/"
  folder "&lt;folder&gt;"
</code></pre>
<p>You can replicate the last bit (<kbd>poll …folder “&lt;folder&gt;”</kbd>) as many times as you need to add multiple accounts. <kbd>&lt;local username&gt;</kbd> is the username you want mail to be delivered under (I recommend running <kbd>fetchmail</kbd> as <em>your user</em> unless there are extenuating circumstances). <kbd>username</kbd> and <kbd>password</kbd> are self-explanatory. The exact structure of <kbd>&lt;folder&gt;</kbd> can differ based on the server (for example, with Gmail, some folders require a prefix of <kbd>[Gmail]</kbd>), but usually <kbd>Inbox</kbd> will work without any prefix. Oh, and <kbd></kbd> is a Mail Delivery Agent — you’ll want one that delivers to a <em>maildir</em>. The one I use is the stand-alone MDA from <kbd>getmail</kbd> called <kbd>getmail_maildir</kbd> and requires no configuration.</p>
<p><em><b>A special note for Protonmail Bridge users</b></em>: In the <kbd>options</kbd> section, you’ll want <kbd>keep no sslcertck sslfingerprint “48:63:2E:30:62:FC:23:B3:9A:42:CD:02:1E:FF:F8:52”</kbd> since otherwise <kbd>fetchmail</kbd> will throw an error as the Bridge’s certificate isn’t trusted.</p>
<p>At this point, <kbd>fetchmail</kbd> should be good to go. Before you try running it, you should create the maildirs you setup in the <kbd>fetchmail</kbd> config. This is really easy; if the <kbd>maildir</kbd> is <kbd>$DIR</kbd>, you can just run <kbd>mkdir -p $DIR/{cur,tmp,new}</kbd> and you’re done. Now, you can try running <kbd>fetchmail -f /path/to/config/file</kbd>. If all goes well and you don’t have any unread emails, <kbd>fetchmail</kbd> should exit normally without downloading any emails. At this point, I’d run <kbd>fetchmail -a -f /path/to/config/file</kbd> to get a copy of <em>all</em> emails, read or not. Note that <kbd>fetchmail</kbd> uses whether an email is read to determine whether or not to download it.</p>
The next piece of the puzzle is <kbd>mutt</kbd>. All of my <kbd>mutt</kbd> configuration goes in <kbd>~/.config/mutt/muttrc</kbd>. I have a master RC file that looks something like this (along with many other variables that I won’t go into here):
<pre><code>set folder="~/.local/mail"

mailboxes "+Gmail" "+Brown" "+CaltechAlumni" "+ProtonMail" "+ProtonMail-Custom"

folder-hook Brown 'source ~/.config/mutt/muttrc/muttrc.localb; macro index cb '
folder-hook Gmail 'source ~/.config/mutt/muttrc/muttrc.localg; macro index cg '
folder-hook CaltechAlumni 'source ~/.config/mutt/muttrc/muttrc.localca; macro index ca '
folder-hook ProtonMail 'source ~/.config/mutt/muttrc/muttrc.localp; macro index cp '
folder-hook ProtonMail-Custom 'source ~/.config/mutt/muttrc/muttrc.localpc; macro index cq '

source ~/.config/mutt/muttrc/muttrc.macros
</code></pre>
<p>In my case, I store all of my email in <kbd>~/.local/mail</kbd>, so I set that as the “base” location, if you will, by setting the <kbd>folder</kbd> variable. Then, <kbd>+Gmail</kbd> expands to <kbd>~/.local/mail/Gmail</kbd>. In this way, I don’t need to spell out the full location for each mailbox.</p>
Additionally, when I switch mailboxes, I load a mailbox-specific config file. For example, the Gmail one looks like this:
<pre><code>set spoolfile="~/.local/mail/Gmail"

set smtp_url="smtps://chiraag.nataraj\@gmail.com@smtp.gmail.com/"
set from="ಚಿರಾಗ್ ನಟರಾಜ್ &lt;chiraag.nataraj@gmail.com&gt;"
set realname="ಚಿರಾಗ್ ಮಂಜುನಾಥ ನಟರಾಜ್"
set imap_user=chiraag.nataraj@gmail.com
set record="^"

set signature="~/.config/mutt/signatures/signature.home"
set header_cache=~/.cache/mutt/mutt_cache.gmail

send-hook . "source 'gpg -d ~/.config/mutt/mutt_secure/gmail.password.gpg |'"

source ~/.config/mutt/muttrc/muttrc.macros
source ~/.config/mutt/muttrc/muttrc.autocrypt
</code></pre>
<p>In other words, I set the SMTP URL (for sending email from this mailbox), name, signature, and so on. Additionally, I tell it to decrypt the file containing the password for my Gmail account before opening up my editor to compose an email. This means I don’t need to needlessly decrypt the file until necessary.</p>
You might notice this <kbd>mutt.macros</kbd> thing lying around. That file looks like this:
<pre><code>macro index ca '&lt;sync-mailbox&gt;&lt;enter-command&gt;source ~/.config/mutt/muttrc/muttrc.localca&lt;enter&gt;&lt;change-folder&gt;!&lt;enter&gt;'
macro index cb '&lt;sync-mailbox&gt;&lt;enter-command&gt;source ~/.config/mutt/muttrc/muttrc.localb&lt;enter&gt;&lt;change-folder&gt;!&lt;enter&gt;'
macro index cg '&lt;sync-mailbox&gt;&lt;enter-command&gt;source ~/.config/mutt/muttrc/muttrc.localg&lt;enter&gt;&lt;change-folder&gt;!&lt;enter&gt;'
macro index cp '&lt;sync-mailbox&gt;&lt;enter-command&gt;source ~/.config/mutt/muttrc/muttrc.localp&lt;enter&gt;&lt;change-folder&gt;!&lt;enter&gt;'
</code></pre>
<p>This is just an easy way for me to switch between different mailboxes. Each of my mailbox-specific config files sources this file. In the <kbd>folder-hook</kbd>, I then ‘reset’ the macro for the <em>current</em> mailbox to simply refresh the screen (basically a no-op).</p>
This system means that if I want to add a new mailbox to <kbd>mutt</kbd>, I do the following:
<ol>
<li>
Create a new mailbox-specific config file with the necessary details.
</li>
<li>
Add the mailbox to my master configuration file.
</li>
<li>
Add a keybinding for switching to that mailbox to <kbd>mutt.macros</kbd>.
</li>
</ol>
<p>In addition, I need to do silly things like creating a mailbox-specific signature and stuff, but that’s not too bad.</p>
<p>With some work, I could probably even automate the current system for adding a mailbox, but I am fairly satisfied with how the system currently works.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
