<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Hillary Clinton's interview</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Hillary Clinton's interview</h1>
      <article>
    <section class="header">
        Posted on September 17, 2017
        
    </section>
    <section>
        <p>So <a href="https://www.youtube.com/watch?v=tteLMfFDCb4">Hillary Clinton did an interview</a> recently.</p>
<blockquote>
[1:57] “I had not drafted a concession speech! I had been working on a victory speech!”
</blockquote>
<p>That…is kinda entitled. But, I suppose, if you believe in polling as your primary deity as she seems to (and, to be fair, as <em>many</em> do across the political spectrum), she had every reason to believe she would win.</p>
<blockquote>
[2:15] “I just felt…this enormous let-down…this, kind of, loss of…feeling and direction and sadness.”
</blockquote>
<p>I understand this — especially if you’ve been campaigning for the last few months and thought you would win (again, based on polling).</p>
<blockquote>
[3:56] “[Interviewer:] You specifically bought this house for a reason. [Clinton:] I did. [Interviewer:] And this was to be… [Clinton:] Well…I know a lot about what it takes to move a President and I thought I was going to win…[Narrator:] The Clintons had acquired the house next door to accomodate White House staff and security during a second Clinton administration.”
</blockquote>
<p>Okay, seriously? Look, I understand thinking you’re going to win. But being <b>so</b> sure that you already start making preparations honestly smacks of entitlement. It smacks of smugness and this sense of “Of <b>course</b> I’m going to win!” which is honestly quite frustrating and off-putting. And for those who are angered by my reaction, do you really think we liberals and the media wouldn’t lambast Trump if he had done the same thing? Of <b>course</b> we would.</p>
<!--more-->
<blockquote>
[5:40] “[Clinton:] He was quite successful in referencing a nostalgia that would give hope, comfort, settle grievances, for millions of people who were upset about gains that were made by others… [Interviewer:] What you’re saying is millions of white people. [Clinton:] Millions of white people. Yeah. Millions of white people.”
</blockquote>
<p>Ah, fantastic. So I agree that <em>some</em> of this was due to Trump attracting that vote. But undeniably, part of it stemmed from his attacking trade deals, something Clinton couldn’t or wouldn’t do <a href="https://www.washingtonpost.com/news/the-fix/wp/2015/06/17/tracking-the-many-hillary-clinton-positions-on-trade/">consistently enough</a>. And that matters because many people <b>have</b> been hurt by free trade agreements, and Clinton did not offer a real solution for them. Neither did Trump, but he at least spoke to their hurt (as did Sanders). In fact, trade is one of the few things <a href="https://www.ontheissues.org/2016/Donald_Trump_Free_Trade.htm">Trump <b>has</b> been consistent on</a> (going all the way back to Reagan). I don’t think protectionism of the sort Trump espouse(s/d) is the right answer, but certainly looking again at our free trade agreements is not something I’m against (I just have absolutely no confidence that Trump will actually make them better). I wonder if she addresses all of this later on in the interview.</p>
<blockquote>
[6:18] “The forces that were at work, in 2016, were unlike anything I’ve ever seen or read about. It was a perfect storm!”
</blockquote>
<p>Okay. Let’s talk about Россия — I suppose we can’t get away from it. At this point, it is undeniable that Trump’s campaign <a href="https://www.businessinsider.com/donald-trump-jr-explains-why-he-met-with-russians-at-trump-tower-2017-9"><em>attempted</em> to collude with Russia</a>. But I think it is pretty amazing to see the “outrage” it’s caused when <a href="https://www.huffingtonpost.com/entry/the-us-has-been-meddling-in-other-countries-elections-for-a-century-it-doesnt-feel-good_us_57983b85e4b02d5d5ed382bd">we have done the <b>exact same thing</b> in <em>many</em> countries (including Россия)</a>. So to pretend that this is somehow uniquely awful when <b>we have done the exact same shit</b> either shows a shockingly awful knowledge of history (in which case, read up!) or a deliberate attempt to ignore it. Also, as a side note, the <a href="https://www.politico.com/story/2017/01/ukraine-sabotage-trump-backfire-233446">Clinton campaign colluded with Ukrainian officials</a>.</p>
<blockquote>
[6:38] “Oh I think the most important of the mistakes I made was using personal email.”
</blockquote>
<p>Absolutely no disagreement here, except for the assertion that it was one of her most important mistakes. But it certainly was a big mistake that was self-inflicted.</p>
<blockquote>
[8:11] “Eleven days before the election. And it raised the spectre that somehow, the investigation was being reopened…it just stopped my momentum.”
</blockquote>
<p>Eh. I would disagree with this assessment. I think it certainly didn’t help, but most of the people who already believed that she didn’t do anything wrong continued to believe that. And people who believed she was a criminal continued to believe she was a criminal. I feel like trying to blame this for her loss is reaching a bit.</p>
<blockquote>
[8:19] “At the same time he does that, about a closed investigation, there’s an open investigation into the Trump campaign and their connections with Россия. You never hear a word about it. And when asked, later, he goes ‘Well it was too close to the election’.”
</blockquote>
<p>For what it’s worth, here is <a href="https://finance.yahoo.com/news/james-comey-explained-why-sent-162942157.html">Comey’s sworn testimony</a> about the discrepancy. So Clinton seems to be misrepresenting his reasoning here.</p>
<blockquote>
[8:58] “She writes ‘his attacks caused lasting damage, making it harder to unify progressives in the general election and paving the way for Trump’s “Crooked Hillary” campaign.’.”
</blockquote>
<p>Ugh. Okay, we have to talk about this, because apparently Clinton still wants us to. Let’s look back to Clinton and her 2008 primary against Obama (which she ultimately lost).</p>
<ul>
<li>
Even when it was clear that Clinton would lose, she <a href="https://www.nbcnews.com/id/24953561/ns/politics-decision_08/t/clinton-refuses-concede-nomination/">refused to concede</a> in the hopes of gaining leverage against Obama when it came to policies
</li>
<li>
Clinton put <a href="https://www.nytimes.com/2008/06/05/us/politics/05superdelegates.html">intense pressure on superdelegates</a> to see if she could flip them through what was essentially a loyalty test
</li>
<li>
She literally fought tooth and nail using every trick in the book, once even <a href="https://www.motherjones.com/politics/2016/05/hillary-clinton-bernie-sanders-drop-out-election/">noting that Robert Kennedy was assassinated after winning the California primary</a> to justify staying in the race
</li>
<li>
She <a href="http://blogs.abcnews.com/politicalpunch/2008/05/clintons-contex.html">didn’t buy the “party unity” argument</a> when she was the losing candidate
</li>
</ul>
<p>Pretty much everything Clinton supporters threw at Sanders supporters (and sometimes at Sanders himself) was stuff Clinton said in 2008 when she was the losing candidate against Obama. So this notion that somehow, it was a ‘more noble’ fight when she was the losing candidate is complete and utter BS. It seems only the winner of the primary process cares about ‘party unity’.</p>
<p>As for Sanders setting up Clinton for Trump’s “Crooked Hillary” comments: everything he said about her is true. She <b>did</b> take money from Wall Street. She <b>did</b> give speeches to Goldman Sachs. Note how no one really disputes that (because it’s true). Many of us looked at that and said “Yeah, she’s corrupt”. And do people honestly think that Trump wouldn’t have said that stuff if Bernie hadn’t entered the race (the most extreme scenario)? Of <b>course</b> he would have. Because again, with respect to Sanders’ critiques, everything was <b>true</b>. Saying he shouldn’t have said those things is basically saying there shouldn’t have been a real primary, that Sanders should have gone easy on Clinton, when that certainly wasn’t true in 2008 when Clinton became the underdog. Again, this only seems to be a ‘problem’ when you’re…the winning candidate of the primary?</p>
<blockquote>
[9:22] “I understood that there were many Americans who, because of the financial crash, there was anger and there was resentment. I knew that. But I believed that it was my responsibility to try to offer answers to it not to fan it. I think, Jane, that it was a mistake because a lot of people didn’t want to hear my plans. They wanted me to share their anger, and I should have done a better job of demonstrating: I get it.”
</blockquote>
<p>Sure, you shouldn’t fan the anger. I get that. But the thing is that <b>she</b> didn’t want to discuss her plans — the vast majority of her TV ads, for example, <a href="https://www.cnbc.com/2017/03/08/hillary-clintons-campaign-tv-ads-were-almost-entirely-policy-free.html">didn’t discuss policy and went after Trump on moralistic grounds</a>. So to pretend that she wanted to discuss policies and that people didn’t want to hear it is…disingenuous. At least as far as ads go, she certainly didn’t seem to care about policy. Unfortunately, I can’t find a similar analysis of her speeches versus Trump’s speeches.</p>
<blockquote>
[10:09] “[Clinton:] Well, I thought Trump was behaving in a deplorable manner. I thought a lot of his appeals to voters were deplorable. I thought his behavior, as we saw in the Access Hollywood tape, was deplorable. And there were a large number of people who didn’t care — it did not matter to them. And he turned out to be a very effective reality TV star in our presidential campaign. [Interviewer:] You fed into that though. When you said ‘basket of deplorables’, you energized… [Clinton:] No, but they were already energized. [Interviewer:] But you offended some people who didn’t personally feel deplorable at all. [Clinton:] I don’t buy that. I don’t buy that. I’m sorry I gave them a political gift of any kind, but I don’t think it was determinitive.”
</blockquote>
<p>And here we have it again — Clinton goes after Trump based on his behavior. Doing that doesn’t work — how has she not learned anything? Instead of saying “Well…maybe it was a bad idea to blanketly stereotype half of Trump’s supporters” and thinking about how to reach out to the ones on the fence, she doubles-down on her earlier rhetoric. It’s unhelpful rhetoric that stereotypes broad swathes of people while not gaining any new allies and alienates people on the fence (something she still refuses to admit).</p>
<blockquote>
[11:24] “So in my debate prep, we practiced this — the young man playing Trump would stalk me. And I practiced keeping my composure. I practiced not getting rattled.”
</blockquote>
<p>Let’s say we take this at face value, that being on the same stage as Trump after the Access Hollywood tape and having him be anywhere near you is awful.</p>
<blockquote>
[11:52] “And so while I’m answering questions, my mind is going ‘Okay, do I keep my composure, do I act like a President…’…[debate excerpt]…or do I wheel around and say ‘Get out of my space! Back up you creep!’ Well you know, I didn’t do the latter, but I think in this time we’re in, particularly in this campaign, maybe I missed a few chances.”
</blockquote>
<p>So I can’t tell exactly, but it seems like she’s blaming this in part for why she lost. If that is what she’s doing, that’s…weird. The thing is, if you take Trump as an expression of toxic masculinity and everything that comes with it (the entitlement over women’s personal space, for example) — <b>that’s ingrained in society</b>. Now of course I can’t tell if she should have done that or not (and that’s totally a personal decision), but I don’t think it was that pivotal in the campaign. It certainly wasn’t as pivotal as not campaigning significantly with labor unions and whatnot in the Rust Belt. It wasn’t as pivotal as not running substantive ads. It wasn’t as pivotal as taking the Rust Belt for granted.</p>
<blockquote>
[12:25] “I am done with being a candidate, but I am not done with politics because I literally believe that our country’s future is at stake.”
</blockquote>
<p>Yes, and you’re standing in the way. You have shown that you <b>are</b> out of touch, that you <b>don’t</b> get it. Progressives are already winning local seats around the country, <a href="https://www.thenation.com/article/a-progressive-electoral-wave-is-sweeping-the-country/">including in counties that went heavily for Trump</a>. So if you really want to defeat Trump, get out of the way and let the #Resistance succeed.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
