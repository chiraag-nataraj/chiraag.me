<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - War on Christians</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>War on Christians</h1>
      <article>
    <section class="header">
        Posted on December  4, 2017
        
    </section>
    <section>
        <p>Now that the holiday season is officially upon us, I want to take some time to dispel a pervasive notion that has creeped into the public consciousness.</p>
<p>There is no War on Christians. Christians are not being persecuted.</p>
<p>Fact: The government, on all levels, is dominated by Christians.</p>
<p>Fact: The government has celebrated Christmas, Easter, Lent, and so on. Every. Single. Year.</p>
<p>Fact: Christians are the vast majority of the population. And have been. Since they killed off the Native Americans.</p>
<!--more-->
<p>If you are the vast majority, and have been every single year since this country’s founding, you don’t get to call yourself a victim. If you have dominated all levels of government since this country’s founding, you don’t get to call yourself a victim. If we’re still debating birth control, abortion, and LGBTQ+ rights based on your religious beliefs, you don’t get to call yourself a victim.</p>
<p>Calling Christians victims and alleging that there is a War on Christians <b>here</b> is an insult to the places where Christians <b>do</b> actually face persecution. It is an insult to places where Christians are rounded up for the “crime” of being Christian.</p>
<p>If Christians are suddenly rounded up and thrown in prison, or if they’re forced to register into a Nazi-like database, or if they’re singled out for special surveillance, or if they’re forced to convert or die, <b>then</b> come and tell me there’s a War on Christians and I will agree with you. But reforming our government to be more secular (and to ensure the separation of church and state) is not a War on Christians. It is just the fulfillment of the promise of our Constitution.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
