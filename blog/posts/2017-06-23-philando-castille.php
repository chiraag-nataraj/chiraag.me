<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Philando Castille</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Philando Castille</h1>
      <article>
    <section class="header">
        Posted on June 23, 2017
        
    </section>
    <section>
        <p>I think the lesson of the #PhilandoCastille murder and the subsequent verdict is that if you are a POC, you should literally never reach for anything, <b>even if you’re asked to do so</b>. Simply existing and complying with the officer’s order (to produce an ID) was enough to get him shot. Instead, indicate where the ID is and ask the officer to reach in and get it themselves.</p>
<p>Seriously. I’m not even sure what else the options are at this point if you’re a POC, especially a (young?) black male (but this also applies to (young?) brown males like myself). Don’t run away from the officer. Don’t run towards the officer. Don’t “look suspicious”. Don’t own a gun (since declaring it while being a POC will heighten tensions). Don’t be disrespectful. Don’t get angry (no matter what). Don’t talk back to the officer. Comply with everything the officer says. None of this helped Mr. Castille. He was still shot. Except wait…he owned a gun. Oh, but it was registered and he declared it to the officer <b>as is required by law</b>. This whole thing is sickening because he did everything the officer asked him to, got shot anyway, and then the jury <b>acquitted</b> the officer of all charges. They watched the video filmed right after the officer murdered Mr. Castille and said “eh”. They watched the dashcam video (that was newly released after the trial) and said “There’s still probable cause for the officer to have been afraid for his life”. My only question is: what could he have done? If what Philando did — following the rules, declaring the firearm <b>he was licensed to carry</b> as per the law, remaining calm, and reaching for his ID — was enough to get him murdered, what could he have done to prevent the officer from shooting him? I think changing his skin color would have helped. And that is the sickening reality of racism in America today.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
