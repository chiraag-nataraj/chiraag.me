<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Venezuela, Maduro, and Guaidó</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Venezuela, Maduro, and Guaidó</h1>
      <article>
    <section class="header">
        Posted on March 15, 2019
        
    </section>
    <section>
        <p>I implore my friends, especially my liberal ones, to stop and consider the possible implications a US-backed coup in Venezuela. <b>Yes</b>, Maduro is awful, No doubt about that. But a US-backed coup would most likely lead to a civil war, massive civilian casualties, and possibly a right-wing authoritarian government (e.g. a military dictatorship) — the US is great at getting those kinds of governments installed. Just. Look. At. History. Were y’all the same people who wanted to invade Iraq on a humanitarian basis? Serious question.</p>
<p>It’s been terrifying seeing so many of my friends latch onto imperialist rhetoric, and I understand the impulse to want to do <em>something</em>. As Cody says, there are non-military options that include brokering diplomatic negotiations, <em>actual</em> humanitarian aid (rather than political destabilization masquerading as humanitarian aid), and possibly getting Maduro to agree to new elections held by the UN.</p>
<p>Recognizing Guaidó isn’t helping and only further legitimizes someone <b>who was never elected to lead the country</b> (even in a sham election).</p>
<p>Let’s also take a moment to recognize that this isn’t about Maduro being authoritarian. We’re okay with authoritarians when they’re on “our side” (Saudis, Saddam Hussein, Gaddafi for a while, Fateh al Sisi, and, of course, countless examples from history, even just of the 20th century). This is about the fact that Venezuela has a ton of oil, and American companies would like to be able to go in there and extract it as they did before nationalization (rather than having to buy from PDVSA). You’re naive if you don’t think this has anything to do with the way Venezuela is singled out for “humanitarian” intervention.</p>
<p>https://www.youtube.com/watch?v=VLuRhzaNd8g&amp;rel=0</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
