<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Converting Raspbian to Debian sid</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Converting Raspbian to Debian sid</h1>
      <article>
    <section class="header">
        Posted on July 28, 2019
        
    </section>
    <section>
        <p>So a while back, I decided to get a Raspberry Pi to setup as a UPnP media server for my local network (<em>not</em> accessible from outside the network). Of course, the standard procedure is to install Raspbian, which is a customized Debian-based distribution that works well with the Raspberry Pi.</p>
<p>However, this presents an annoyance: the Raspbian repositories contain a <em>very</em> tiny subset of the Debian respositories. This means that your software choice is reduced by a lot and, given that I wanted this to be a “set it and forget it” type of media server, compiling and updating programs from source was mostly out of the question (<b>especially</b> when that software was available in Debian!). I decided to go on an adventure and convert Raspbian to Debian proper.</p>
<p>Note: I did this a while ago, so I may be missing some steps. Please let me know in the comments and I will update it with more details if need be!</p>
<p>First, I noticed that the architecture Raspbian refers to as <code>armhf</code> is actually an amalgamation of both <code>armel</code> and <code>armhf</code> in Debian. That is, <b>just adding the Debian repositories and upgrading will probably break your system</b>. This was the first issue I ran into when I naïvely just edited <code>/etc/apt/sources.list</code> to include <code>deb http://ftp.us.debian.org/debian/ sid main contrib non-free</code>. When I ran <code>sudo apt update &amp;&amp; sudo apt dist-upgrade</code>, everything completely broke.</p>
<p>This meant I needed to switch the architecture from <code>armhf</code> to <code>armel</code>. The first step to doing this was the following:
1. Run <code>sudo apt update &amp;&amp; sudo apt dist-upgrade</code>. You want a clean, fully upgraded state to start from.
2. In <code>/etc/apt/sources.list</code>, change <code>deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi</code> to <code>deb [arch=armhf] http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi</code>.
3. In the same file, add the line <code>deb [arch=armel] http://ftp.us.debian.org/debian/ sid main contrib non-free</code>.
4. In <code>/etc/apt/sources.list.d/raspi.list</code>, change <code>deb http://archive.raspberrypi.org/debian/ buster main ui</code> to <code>deb [arch=armhf] http://archive.raspberrypi.org/debian/ buster main ui</code>.
5. Run <code>sudo dpkg –add-architecture armel</code>.
6. Run <code>sudo apt update</code>.
7. Here’s the <em>really</em> messy part. You need to slowly migrate every package you can from the <code>armhf</code> version in Raspbian to <code>armel</code> in Debian sid. This involves running <code>sudo apt install <package-name>:armel</code> and manually resolving dependencies. Do <b>not</b> run <code>sudo apt upgrade</code> because, at this stage, the dependency resolver will probably just choke. My advice here is to start with core libraries and then slowly move to bigger and bigger packages. Once you’ve switched <code>dpkg</code> and <code>apt</code> over, it definitely gets easier, but it’s still a tedious process.
8. Once you’ve done this, you can now use <code>sudo apt update</code> and stuff as normal and it should “Just Work”™.</p>
<p>An alternative to this horrifically messy process is to flash <a href="https://f002.backblazeb2.com/file/chiraag-public/rpi-backup.img">this image</a> instead of the default Raspbian image. It <b>is</b> out of date, so you’ll have a large system update to run after flashing it, but it should save you the horrifically messy transition.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
