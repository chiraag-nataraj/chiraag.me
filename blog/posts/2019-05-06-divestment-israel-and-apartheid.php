<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Divestment, Israel, and Apartheid</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Divestment, Israel, and Apartheid</h1>
      <article>
    <section class="header">
        Posted on May  6, 2019
        
    </section>
    <section>
        <p>Before you read further, I want you, the reader, to head over to <a href="https://www.reddit.com/r/socialism/comments/azfjpl/israel_is_the_nationstate_of_jews_alone_netanyahu/ei7rpdm/">this comment on Reddit</a> which details, with nauseating clarity, just <em>how many times</em> unarmed Palestinians have been killed by the IDF, Israeli police officers, or other agents of the state (with sources). Go ahead, I’ll wait.</p>
<p>It has always been clear to me that the Israeli government actively wants to kill and displace Palestinians and that they have treated them as second-class citizens. Archbishop Desmond Tutu <a href="http://www.pcusa.org/site_media/media/uploads/oga/pdf/2014-journal%2C_parti-electronic.pdf">said</a>:</p>
<blockquote>
I have been to the Occupied Palestinian Territory, and I have witnessed the racially segregated roads and housing that reminded me so much of the conditions we experienced in South Africa under the racist system of Apartheid. I have witnessed the humiliation of Palestinian men, women, and children made to wait hours at Israeli military checkpoints routinely when trying to make the most basic of trips to visit relatives or attend school or college, and this humiliation is familiar to me and the many black South Africans who were corralled and regularly insulted by the security forces of the Apartheid government. It is not with rancor that we criticize the Israeli government, but with hope, a hope that a better future can be made for both Israelis and Palestinians.
</blockquote>
<p>The UN has <a href="https://www.un.org/press/en/2016/sc12657.doc.htm">criticised</a> Israeli settlements as illegal, saying:</p>
<blockquote>
Israel’s establishment of settlements in Palestinian territory occupied since 1967, including East Jerusalem, had no legal validity, constituting a flagrant violation under international law and a major obstacle to the vision of two States living side-by-side in peace and security, within internationally recognized borders.
</blockquote>
<p>and reiterated its demand that “Israel immediately and completely cease all settlement activities in the occupied Palestinian territory, including East Jerusalem.”</p>
<p>When Archbishop Tutu (who has lived through South African apartheid) is calling something apartheid and the UN is saying it is a flagrant violation of international law, it seems extremely odd to me that people would spend political (and literal) capital defending that system.</p>
<p>Assuming these people are arguing in good faith, it’s quite interesting that they spend so much political capital defending the murder of unarmed Palestinians or the building of illegal settlements. The first <em>should</em> be indefensible and the second <em>should</em> be obviously wrong, yet we have a political establishment which tries to do whatever it can to defend Israel. Why?</p>
<p>These people constantly provide Israel with military aid and technology, which is used in service of their apartheid regime. They run a free PR campaign for them when they launch a ground assault and murder civilians. They defend them during all of these transgressions, these morally indefensible acts.</p>
<p>Through all of this, what can we, as individual citizens do? Well…in the middle of all of this violence, this violent suppression of Palestinians, several well-known companies, such as HP and Motorola, enable the IDF and the rest of the Israeli state in committing their crimes against humanity.</p>
<p>Divesting from these companies (and calling on others to do likewise) sends a signal to these companies that, indeed, their enabling of Israeli apartheid is not unnoticed and that, moreover, there is a <em>financial</em> penalty they will pay should they choose to continue enabling the IDF and the rest of the Israeli state.</p>
<p>Until and unless the Israeli state stops its murder and displacement of Palestinians in service of its apartheid regime, we should do what we can to push the institutions we are part of to divest from the companies complicit in these heinous acts. Only by introducing a financial penalty will we see a material change in the currently deplorable state of affairs, and only <em>then</em> may we obtain real peace.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
