<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Discussion of the AHCA</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Discussion of the AHCA</h1>
      <article>
    <section class="header">
        Posted on March 14, 2017
        
    </section>
    <section>
        <p>People <b>will</b> lose coverage under the AHCA. And note that all of these numbers are relative to current coverage levels (not absolute numbers).</p>
<p>Donald Trump has come out <a href="https://www.newsmax.com/Politics/Trump-AHCA-Healthcare/2017/03/11/id/778177/">in favor</a> of this bill. Let us look at the promises he made. Interestingly, healthcare doesn’t appear anywhere on the <a href="https://www.whitehouse.gov/america-first-energy">Issues section</a> of the official Whitehouse page and the issues pages have been taked off of the Donald Trump <a href="https://donaldjtrump.com">website</a>, so I will have to rely on <a href="https://assets.donaldjtrump.com/Healthcare_Reform.pdf">this archive</a> that’s buried somwhere within the assets folder of his website. Let’s take a look.</p>
<p>According to <b>him</b> (in that document): “By following free market principles and working together to create sound public policy that will broaden healthcare access, make healthcare more affordable and improve the quality of the care available to all Americans.” That is his ideal system. But as the <a href="https://www.cbo.gov/publication/52486">CBO estimates</a> show, this legislation (which he supports) will <b>reduce</b> healthcare access and average premiums will most likely go up.</p>
<p>That being said, does this legislation hit on all the points that he advocates in that PDF? I’m using <a href="https://www.speaker.gov/general/american-health-care-act-fact-sheet">this fact sheet</a> as a source.</p>
<ul>
<li>
Repeal Obamacare — check.
</li>
<li>
Allow sale of insurance across state lines — nope.
</li>
<li>
Block-grant Medicaid (that’s what “giving states greater flexibility” means) — check.
</li>
<li>
Enhance HSAs — check.
</li>
<li>
Deduct premiums from taxes — nope.
</li>
<li>
Price transparency — nope.
</li>
<li>
Remove barriers to entry for drug providers — nope.
</li>
</ul>
<p>So not <b>only</b> does it explicitly do the opposite of what Trump promised (according to <b>his own words</b>), it <b>also</b> doesn’t do many of the things he advocated for. Even if you’re Donald Trump, this is a terrible bill.</p>
<p>Also note that I only used an external source <b>once</b> in this post (to show that he’s come out in favor of the bill). This is all based entirely on what he has advocated for as well as the actual text of the bill (and the promises of its most vocal supporter).</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
