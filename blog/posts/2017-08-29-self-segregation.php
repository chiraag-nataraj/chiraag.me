<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Self-segregation</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Self-segregation</h1>
      <article>
    <section class="header">
        Posted on August 29, 2017
        
    </section>
    <section>
        <p>Y’all, let’s talk about self-segregation for a second.</p>
<p>First, what do I mean by “self-segregation”? I mean the tendency I’ve seen by some of my friends to unfriend those who harbor extreme views. Unfortunately, I’ve seen this often with my liberal friends unfriending or shunning Trump supporters. I’m not going to claim that it only happens on this side, though, because I don’t have enough data on this stuff — I’ll just talk about this phenomenon generally. I think that, on an aggregate level, this is misguided. Please hear me out before calling the mob on me 😉</p>
<p>I think the people who do this have two main reasons for doing so. First, there is the mental health aspect — people don’t constantly want to feel unsafe among their friend circle, so they kick out anyone who makes them feel unsafe. The second main reason, I think, is that people don’t want these extremists to feel as though their views are valid — that is, this is meant to ostracize these people from society and make them feel as though they don’t belong, that they don’t have a place here.</p>
<!--more-->
<p>Before I discuss the first reason, though, we have to define what I mean by “unsafe”. The way I’m defining it here is that feeling unsafe means your very identity or your worthiness as a person is question. For example, an African American or Jew probably feels unsafe around a neo-Nazi, regardless of whether that neo-Nazi is screaming at you at a protest or taking your order at a restaurant. In this way, kicking someone out of your friend circle because they make you feel unsafe is a form of self-defense, a way to keep your mental health from deteriorating. And honestly, this reason for unfriending someone is perfectly reasonable. I actually agree that if someone is constantly making you feel unsafe and isn’t listening to what you’re saying, you <b>should</b> cut them loose — they <b>aren’t</b> your friend.</p>
<p>However, while I think the second rationale is laudable, I think it will backfire. Here’s why. Part of the reason white nationalists and neo-Nazis have seen a resurgence is because they’ve been cast as the “persecuted” ones. They can make the claim that everyone is out to get them. And while it is certainly true that they’ll most likely have a similar claim no matter <b>what</b> we do, actually seeking to silence them and make sure they’re not heard means people on the fence are easier to persuade. If you can point to actual instances of violence against you because of your views, it makes the rest of what you’re saying seem more credible.</p>
<p>There’s another reason, too, to be skeptical that this will work. One of the ways people change their views is when they talk to people who disagree with them and they have a productive conversation. Is this always (or even mostly) going to be the case? Of course not. But that possibility certainly exists. By cutting out people who <b>are</b> extremists from your friend circle, you force them to self-segregate into an extremist group in which the echo chamber radicalizes people further. And if everyone who has ties to this person cuts them off, they have no moderating influences in their life and there are no restraints on how radical they become. So now, someone who may have been alt-right is now a full-blown neo-Nazi. That’s not just unproductive, it’s anti-productive.</p>
<p>There’s also a crucial distinction here between people of a certain movement and allies/advocates of that movement. For example, straight allies/advocates of the LGBTQ community or white allies/advocates of BLM. Allies and advocates are often in a position of privilege with respect to that specific movement, which means that when they speak out, people are more likely to listen to them. So while it can legitimately be dangerous/unsafe for people of a certain movement to keep ties to extremists, it is often far safer for allies and advocates of that movement to keep those ties. This is crucial because it is one way of retaining that moderating influence on these extremists while also ensuring your physical and mental safety is intact.</p>
<p>So while it totally makes sense to unfriend someone for mental health reasons, I think we should retain those ties, <b>especially</b> if we claim to be allies/advocates of a movement. It’s the only way, I think, to ensure that we don’t create more extremists.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
