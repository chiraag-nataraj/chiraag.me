<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Rethinking the Calendar and Our Measures of Time</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Rethinking the Calendar and Our Measures of Time</h1>
      <article>
    <section class="header">
        Posted on October 18, 2020
        
    </section>
    <section>
        <p>We all know the Gregorian calendar, right? It’s the “standard” one, with 12 months, each one with a variable number of days from 28 to 31. And we may not love it, but we use it. But what if…there were a better way?</p>
<p>I started thinking about this at first when I watched Kurzgesagt’s <a href="https://www.youtube.com/watch?v=czgOWmtGVGs">video</a>, which argues that we should find a new year “0” that better reflects the sheer magnitude of all that we have accomplished as a species. They end up settling on 10000 BC/BCE as the appropriate year “0”, for reasons that they describe in the video. In practice, in order to use the Human Era system, we just tack on an extra “1” at the beginning of the year. So the year 2020 AD/CE becomes the year 12,020 HE (Human Era).</p>
<p>That got me thinking, though, that there must be a better way to distribute the days of the year. I mean, the sheer madness that is the Gregorian calendar would be hilarious if it weren’t so ingrained into us. Instead of each month having a fixed number of days (possibly with some minor adjustment at the end to account for imperfections), we end up with a monstrosity where you have to memorize the number of days in each month. Of course, this happened because the calendar was iteratively refined due to increased knowledge about exactly how long one revolution around the sun takes, adding/subtracting months to please Roman emperors, and so on. But if we were to design a calendar from scratch, what might it look like?</p>
<p>Well, a standard year (ignoring leap years for a second) has 365 days. The prime factors of 365 are 73 and 5, so let’s assign 5 days to a week, instead of the current 7. This leaves us with a year that has 73 full weeks. Now, unfortunately 73 is prime, but 72 — 72 is a number we can work very nicely with. 72 gives us prime factors of 3,3,2,2,2. This means that we can have 18 months with 4 weeks each, 9 months with 8 weeks each, or even 36 months with 2 weeks each if we want. Let’s go with 9 months of 8 weeks each for now. This leaves the last week — the leap week, as it were — by itself. We can call that the 10th month or leave it by itself — either way, it just hangs out at the end of the year.</p>
<p>What happens in a leap year? Just tack on the leap day to the end of the leap week. This design ensures that all of the first 9 months are completely untouched every single year. The same date falls on the same day of the year, every single year. No shifting dates. No memorizing how many days a month has. It’s nice and simple.</p>
<p>We can do a similar thing with time, too. Why exactly do we have 24 hours in a day, with 60 minutes per hour and 60 seconds per minute? Why not have nice, round, powers of 10 for each of those conversions? In this case, however, we will have to redefine the second. Let us define that 1 day has 100,000 seconds. If we set reasonable conversions of 100 seconds per minute and 100 minutes per hour, this gets us a nice, even 10 hours per day.</p>
<p>Unfortunately, this means that a second under this system is quite a bit shorter than a second in the current system. To be exact, one new second is 0.864 current seconds. That is, unlike the new calendar system which is day-for-day compatible with the current calendar system, this new system of measuring time has conversion factors.</p>
<p>In summary, here is the new system I am proposing (and have implemented on my laptop):</p>
<ul>
<li>
100 seconds per minute
</li>
<li>
100 minutes per hour
</li>
<li>
10 hours per day
</li>
<li>
5 days per week
</li>
<li>
8 weeks per month
</li>
<li>
9 months per year with a leap week to round out the year (so effectively 9.125 months or 10 months where the 10th month is highly irregular, whichever you prefer)
</li>
<li>
On leap years, the leap week gets a leap day
</li>
<li>
Add 10000 to the current year to utilize Human Era years
</li>
</ul>
<p>Also, days and months should be 0-indexed as our hours (in 24-hour time), minutes, seconds, and years are. So month numbers go from 0 to 8 (or 9 if you could the last week as a month) and day numbers go from 0 to 39.</p>
<p>This is certainly a change from our current way of measuring and dealing with time. But I tend to view this as a much more rational approach, one that better incorporates the additional knowledge we now have. Additionally, it makes the calendar as regular as humanly possible. The same holds true for the new time system — indeed, it’s far more regular than even the new calendar system. It takes adjustment, but so does <em>any</em> new system. That’s what happens when everyone is brought up on a particular system. It’s why transitioning between operating systems can be so painful, or daring to dream big about systemic change can feel scary. We find comfort in the familiar but sometimes…the familiar just <em>really</em> sucks.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
