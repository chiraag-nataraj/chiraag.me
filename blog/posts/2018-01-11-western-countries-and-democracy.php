<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Western countries and democracy</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Western countries and democracy</h1>
      <article>
    <section class="header">
        Posted on January 11, 2018
        
    </section>
    <section>
        <p>Western countries only like democracies when they can chose the people running them. Western countries only like democracies when they allow companies from those countries to go in, exploit people, and plunder resources. Western countries only like democracies when they don’t serve the people, but rather the elites and the corporations. Otherwise, those same countries who wax eloquently about the dangers of dictatorships and the need for democracy will launch an invasion on some pretext (real or made-up) and replace that democracy with a puppet state. Or, they ensure a stable state never forms in the first place. And, of course, this is tied in with capitalism and the ownership of our government by (big) corporations.</p>
<p>Don’t be distracted. What Donald Trump said today is not an anomaly — it is implicitly or explicitly believed by <b>at the very least</b> a large minority of the population. The reason these nations are getting screwed over is that companies and governments from all around the world are ensuring that either those governments are obsequious to other governments and corporations (and not actually working for their people) or that those governments don’t exist in the first place. Fixing this requires deconstructing the paradigm that we have built.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
