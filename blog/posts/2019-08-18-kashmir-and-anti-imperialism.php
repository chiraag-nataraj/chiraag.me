<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Kashmir and Anti-Imperialism</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Kashmir and Anti-Imperialism</h1>
      <article>
    <section class="header">
        Posted on August 18, 2019
        
    </section>
    <section>
        <p>Recently, Prime Minister Modi of India decided to abrogate Articles 370 and 35A of the Indian Constitution and fully annex Jammu and Kashmir. <a href="https://jacobinmag.com/2019/08/indian-occupation-kashmir-srinagar-bjp-curfew">This article</a> from inside Kashmir talks about police- and military-enforced curfews, internet and phones being blocked, and a general sense of oppression.</p>
<p>Defenders will point to Pakistan’s <a href="https://thediplomat.com/2016/07/pakistans-failed-kashmir-policy/">decades-long policy</a> of interfering in Indian-controlled parts of Jammu and Kashmir. This, in its own right, is a form of imperialism and <b>must be opposed</b>. However, this is no excuse for Indian imperialism.</p>
<p>This whole thing is complicated, right? Because there are two conflicting desires.</p>
<p>On the one hand, there is a sense that a country has to mean something, that states (and territories) shouldn’t just be able to break away. This tendency is best exemplified by the US Civil War, where the southern, slave-holding states wanted to secede to preserve slavery and the northern states refused to let them. In my personal conversations, this is the point that comes up the most, where there’s a sense of ‘India First’. “National Security Concerns”™ also sort of fall under this category, since it’s often used to justify holding on to territories even if there are strains of an independence movement within them (Jammu and Kashmir, Tibet, Hong Kong, the various Kurdistans, and so on).</p>
<p>Unfortunately, this line of thinking often ends up justifying naked imperialism, since it tends to override any decision-making based on what the people in that territory want. That is, <em>it doesn’t matter what the people of Tibet want</em> — China’s interests come first. <em>It doesn’t matter what various Kurdish populations want</em> — the national interests of Turkey, Iran, Iraq, and Syria come first (based on whichever Kurdish population we’re talking about). In the same way, I’ve had it explained that <em>it doesn’t matter what the people of Jammu and Kashmir want</em> — India’s national interests come first. Indeed, this line of thinking is extremely problematic coming from a former British colony, as this logic was used to hold together Britain’s vast holdings and justify oppressing the peoples therein.</p>
<p>On the other hand, at least in India’s case, there is a supposed committment to democracy. That is, there is a notion that “the people” should be able to determine for themselves the fate of their government. This often goes hand-in-hand with a supposed committment to “self-determination”.</p>
<p>The problem is that one cannot <em>both</em> be imperialist and committed to democracy. One can either be committed to honoring the will of Jammu and Kashmir or overriding it. And take note that <em>none of this</em> depends on whether Pakistan is itself imperialist.</p>
<p>If Jammu and Kashmir were to have a vote and voted to become independent countries (or one independent country) and Pakistan refused to honor it, that is problematic. It would be <em>just</em> as problematic if India refused to honor it. But those situations can and should be dealt with as they arise, and such hypotheticals should not prevent the expressiong of the right to self-determination.</p>
<p>As a postscript, I’ll note that “national security concerns” can justify anything. If annexing Jammu and Kashmir can be justified under that umbrella, why not just annex and invade Pakistan? Pakistan, after all, is India’s biggest national security threat. And why stop there? Why not annex and invade Afghanistan? Once India annexes Pakistan, Afghanistan will pose the same threat Pakistan currently does.</p>
<p>At this point, it is clear that the people pushing annexation of Jammu and Kashmir are doing so without regard for what the people of Jammu and Kashmir want. <b>I do not claim to speak for them</b>. All I hope is that India and Pakistan listen to what they want and honor the right to self-determination. The recent actions make clear that India, at least, most likely will not go down that path.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
