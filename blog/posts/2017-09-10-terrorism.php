<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Terrorism</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Terrorism</h1>
      <article>
    <section class="header">
        Posted on September 10, 2017
        
    </section>
    <section>
        <p>I think one thing many people miss when it comes to “terrorism” is how the word is defined versus how it’s used. Here’s the dictionary definition:</p>
<blockquote>
Terrorism: the systematic use of terror especially as a means of coercion (from Merriam-Webster)
</blockquote>
<blockquote>
Terrorism: the use of violence or the threat of violence, especially against civilians, in the pursuit of political goals. (from The Free Dictionary)
</blockquote>
<p>So this definition says nothing about <em>who</em> is doing the violence. But consider how it is (almost) always used: it is used to describe a violent act carried out by a non-state actor, usually a group of like-minded individuals with a specific political goal. But why is the use limited to non-state actors such as “terrorist groups” or individuals such as James Fields Jr? When we use drone strikes to remotely target people we call terrorists and end up killing civilians instead (<a href="https://www.ibtimes.com/nearly-90-those-killed-us-drones-were-not-intended-targets-during-five-month-span-2142183">90% of people killed by drone strikes were not intended targets during one 5 month span</a>, for example), why would that not count as terrorism? When we rain death and destruction almost continuously on 7 different countries simultaneously or take out heads of state, why is that not considered terrorism? In those cases, we are most certainly using violence (often against civilians in the case of the drone strikes) to achieve political ends (a change of government and the establishment of a more friendly regime, for example). So this fits the definitions given above, but “terrorism” is almost never used to describe it.</p>
<p>This, of course, begs the question: Why would we not call this terrorism? Well, it’s obvious. Calling what we terrorism would mean that <em>we</em> would be the #1 terrorist threat, not ISIS or Al Qaeda. Calling what we do terrorism would make the military contractors, the CIA, and the military itself complicit in that terrorism, which is unacceptable to those people. But fundamentally, calling what we do terrorism would imply that our actions aren’t automatically legitimate, which is what <em>truly</em> frightens many people. The dogma of American Exceptionalism (and of the legitimacy of the state more generally) requires that everything the state does <em>must</em> be legitimate. And the world recognizes terrorism as illegitimate, which means calling what we do terrorism would mean our government (and countless others) is doing something illegitimate. But the fundamental fact remains that what we are doing <em>is</em> terrorism.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
