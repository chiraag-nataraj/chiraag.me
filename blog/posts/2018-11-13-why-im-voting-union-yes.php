<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Why I'm Voting</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Why I'm Voting</h1>
      <article>
    <section class="header">
        Posted on November 13, 2018
        
    </section>
    <section>
        <p>As you might have noticed from my recent posts, we are coming upon an election! I know I’ve spoken with many of y’all personally over the past 2 years(!), but I want to take some time here to discuss my personal reasons for voting yes for <a href="https://brownsugse.com">SUGSE</a>.</p>
<p>Crucially, my personal experience here at Brown has been pretty great — my adviser is pretty awesome, my classes have been taught well, I don’t have a requirement to TA (but can if I want to), my stipend isn’t too bad, and so on. So this begs the question: why exactly do I want a union? And this is something I’ve thought about over the past couple of years, and the reasons have slightly evolved over time.</p>
<p>Note: Ideologically, I am fairly left-wing (at least in the context of American politics), so I’m already predisposed to supporting unions in that regard. While that certainly helps, it has mainly served as extra inspiration during certain rough patches.</p>
<!--more-->
<p>So here are some of the reasons I will be voting yes:</p>
<ul>
<li>
A formal contract: One of the first things I thought of when I heard of the union is that a formal contract can help alleviate uncertainties. While there may be certain benefits today (say, healthcare or dental care or a specific stipend), there is no guarantee that those benefits will continue existing in the future. There is currently no official contract that guarantees us certain things, whether it be a certain level of stipend, an insurance plan that covers certain things, a certain ceiling on TA hours, or anything else. This means that the university could unilaterally take away those benefits without asking us or caring what we think (and indeed, they have unilaterally changed insurance plans and removed certain programs in the past). Given that we are the ones affected, that seems a bit unfair. Fundamentally, a formal contract would help alleviate uncertainty surrounding some of these benefits and expectations and help grad students understand what is expected and required of them.
</li>
<li>
Democracy at work: Many of us tend to think of democracy as a way to ensure we are represented in our government and a way to affect change without a violent revolution. Fundamentally, a democratic system of government ensures that our voices are heard. But why should this be restricted to our system of government? What is it about government that fundamentally makes it different from the administration of a university or the board of a company? Those bodies are also making decisions that affect other people’s lives, sometimes in enormous ways. Those bodies aren’t making laws, but they currently function as benevolent dictatorships — entities we trust to make decisions about our lives. What happens when the bodies making the decisions aren’t affected by those decisions? It seems reasonable to suppose that the decisions these bodies make would change if they were affected by their own decisions or if the body itself were augmented with people who will be affected. If democracy is good for our government, it is good for our workplace.
</li>
<li>
Accountability: As you all may have guessed from literally <em>any</em> of my posts, I’m not entirely a fan of the Trump administration — one thing it has exposed for me is the sheer lack of accountability in our government. And as I realized this, I started thinking about similar structures and power dynamics at play within other institutions, particularly at universities such as Brown. It can be hard to hold people in power accountable, particularly when they have a lot of control over your life (as university administrators/deans/etc can have). Individually, we are likely to be ignored or retaliated against if we bring forward complaints about our advisers or other professors — the system is designed to protect those in power and shield them from criticism. The only way, as I understand it, to introduce true accountability into the system is through a union, an organization which can channel criticisms into a force that they cannot simply ignore.
</li>
</ul>
<p>There are many other reasons which end up somewhat falling under one or more of these three umbrella reasons. Childcare, for example, used to exist here at Brown but was replaced with an (inadequate) subsidy system and so would fit under all three of these reasons — a formal contract guaranteeing a certain level of childcare support would require members to authorize changes to it, democracy at work would mean that we (who are the ones actually affected by this change) would have had a say in the process, and real accountability would effectively have helped ensure that they couldn’t make the change without at least trying to justify it in a meaningful way (which would potentially open up a dialogue for how to achieve what they wanted without hurting grad students in the process).</p>
<p>Fundamentally, as far as I can tell, there are no downsides to us having a voice in the decisions that affect us. And that is really what our union is about — giving grad student workers a voice in the decisions that affect us.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
