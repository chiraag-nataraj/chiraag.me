<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Why Fair Trade Matters</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Why Fair Trade Matters</h1>
      <article>
    <section class="header">
        Posted on April 15, 2019
        
    </section>
    <section>
        <p>I want to take some time to talk about fair trade, both within the context of our current system as well as how it might look going forward.</p>
<p>Let’s start with a fact: Our current economic system, whatever you want to call it, is exploitative. It is built on exploitation and slavery, and there isn’t really any way to contest that. From <a href="http://fortune.com/big-chocolate-child-labor/">chocolate</a> to <a href="https://www.ituc-csi.org/exploitation-is-widespread-in">textiles</a> to <a href="https://www.theguardian.com/global-development/2018/jan/23/thai-seafood-industry-report-trafficking-rights-abuses">seafood</a>, I could endlessly list industries - almost every single one is touched by this kind of exploitation on an <em>industry-wide</em> scale. Enter fair trade.</p>
<p>Fair trade is a system that has developed over the past couple of decades which attempts to bring supply-chain transparency and worker empowerment to various industries. For the moment, the certification bodies largely target agriculture and textiles. The essential idea is that a third-party certification body will verify that companies are treating their workers ethically all along the supply chain. There are also often environmental components to the certification, ensuring the company does not wantonly destroy the environment during the course of operations. Finally, the fair trade premium is used by workers for community initiatives, and this will look different depending on the community.</p>
<p>The fundamental promise of fair trade is the third-party certification aspect. Nominally, companies cannot just buy these certifications - they have to <em>actually</em> empower their workers, treat them well, and so on. This is in stark contrast to self-“certifications”, where companies claim to be doing the right thing, but don’t actually get audited by a third-party. Such labels might include “ethically sourced”, “sustainably sourced”, and so on - they’re <em>meaningless</em> without third-party verification (I’m looking you, Whole Foods - your “Whole Trade” label is entirely meaningless).</p>
<p>Many people will agree with me up until here - they will say “Oh yeah, fair trade is great!”. But when it come time to actually <em>purchase</em> fair trade products and otherwise move away from more exploitative companies, they might be unwilling to do so. They’ll complain about the price differential or about the expense of the certification (which sometimes prices out smaller farmers). And hey, look, I get it. If you’re poor and literally can’t afford it, it would be absurd for me to sit here and chastise you for not buying fair trade.</p>
<p>But I often hear these objections from people who <em>very much</em> can afford it. I hear it from people who don’t want to do the research, who don’t want to investigate the companies behind the food they purchase. When people are literally being enslaved for your Snickers bar, it seems ridiculous to me that you wouldn’t stop purchasing that item. When people are working with toxic chemicals for your clothing, it seems absurd to me that you wouldn’t switch brands to a company that <em>doesn’t</em> do that.</p>
<p>All of this being said, there <b>are</b> some criticisms of fair trade. For one, the price raises some people’s defenses. To that, I only ask: How much is preventing exploitation worth to you? Another is that sometimes the certification bodies will replicate existing structures and reinforce them - for example, some work more with plantation models than co-ops when it comes to e.g. tea, thus reinforcing the plantation model. However, that model already exists and is already dominant (we can thank Western imperialism for that), and if the certification body can help ensure workers there have more rights and are treated better, it works as an interim solution.</p>
<p>I would, however, like to see more emphasis placed on worker-owned co-ops as an alternative to traditional predatory multinational corporations, both within <em>some</em> segments of fair trade and within our economy as a whole. It is clear that the traditional corporation model breeds exploitation for the sake of profit, and we must move away from that structure if we are to ensure any kind of justice for historically oppressed populations. The blatantly horrific structures of imperialism merely transitioned to an exploitative corporate model as the old imperialist structures fell away, and seeking justice for communities who were under the thumb of brutally oppressive governments requires dismantling those structures.</p>
<p>I bring all this up because there is currently a campaign at Brown University (run by Brown Fair Food Campaign) to get the university to purchase more ethical food - specifically, to increase the amount of fair trade items we buy. Initially, we would like to get Brown to switch out their bananas for fair trade ones and introduce fair trade tea. But long-term, we would fundamentally like to alter the decision-making process for selecting which products we buy such that it takes ethical considerations (especially the company’s treatment of its workers) into account.</p>
<p>If you would like to sign the petition as a student at Brown, please go here: <a href="https://goo.gl/forms/rINtSa7eAWN3oyId2">https://goo.gl/forms/rINtSa7eAWN3oyId2</a>.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
