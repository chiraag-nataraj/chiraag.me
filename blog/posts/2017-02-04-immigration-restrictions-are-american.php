<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Immigration restrictions are American</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Immigration restrictions are American</h1>
      <article>
    <section class="header">
        Posted on February  4, 2017
        
    </section>
    <section>
        <p>After the immigration and refugee ban was announced, a narrative emerged — immigration restrictions are “un-American”. However, looking at this country’s history, it is quite clear that that is not the case. Let us go on a tour of American immigration policy from when the country was founded until the present day.</p>
<p>For a long time, no one really wanted to come here — immigration was pretty low and we were effectively left alone to our own devices. The first immigration law that was passed was the Page Act of 1875, which forbid any immigrants considered “undesirable”: forced laborers from Asia, Asian prostitutes, and convicts. Note how the exclusions are already targeted towards specific areas of the world — two of the three restrictions target Asia specifically. The next major immigration law was the Chinese Exclusion Act of 1882, quite literally banning all immigration from China. The reason? People blamed Chinese immigrants for depressed wages. The state which pushed this law most heavily? California.</p>
<!--more-->
<p>Next, we have the era of restricting multiple groups of people at once. The Immigration Act of 1917 enforced literacy requirements on all immigrants and banned Asian immigration. The Immigration Act of 1924 was enacted to ostensibly limit annual immigration from every country to 2% of that country’s population living at the US in 1890, but was really used to limit Jews, Slavs, and other Eastern and Southern European immigrants from coming here. As if this weren’t bad enough, this Act also severely restricted African immigration and banned Arab immigration altogether (in addition to retaining the Asian immigration ban).</p>
<p>In the early 1930’s, we established a “Mexican Repatriation Program”, but under that guise we forcibly deported thousands of people. We banned Jewish refugees seeking asylum from Nazis. We threw Japanese-Americans in internment camps while largely ignoring German-Americans and Italian-Americans. We carried out “Operation Wetback” when we forcibly deported more than a million Mexicans. American immigration restrictions were codified under the Immigration and Nationality Act of 1952 — racial restrictions were removed, but quotas based on national origin stayed. Furthermore, a tiered system of “desirability” was developed based on ethnicity and which skills immigrants offered.</p>
<p>Starting in 1965, the immigration system was finally overhauled. The Immigration and Nationality Act of 1965 abolished the quota system based on national origin. The Immigration Act of 1990 increased legal immigration by 40%, and the Armed Forces Immigration Adjustment Act of 1991 allowed foreign service members who had served 12 or more years in the US Armed Forces to qualify for permanent residency.</p>
<p>We have only had a reasonably liberal immigration policy since 1965. That’s it. Until then, our immigration system was racist, exclusionary, and nativist. It discriminated against women, certain national origins, races, and religions. So while President Trump’s ban may be immoral, unhelpful, and bigoted, do not whitewash history by saying it is un-American. It is the most American thing in the world.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
