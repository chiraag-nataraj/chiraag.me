<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Military spending</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Military spending</h1>
      <article>
    <section class="header">
        Posted on September 24, 2017
        
    </section>
    <section>
        <p>The most depressing news for me from the last couple of days was the <a href="https://www.cbsnews.com/news/senate-overwhelmingly-backs-bill-to-pump-700-billion-into-military/">$700 billion increase in military spending over the next 10 years</a>.</p>
<p>If you take a look at the <a href="https://www.senate.gov/legislative/LIS/roll_call_lists/roll_call_vote_cfm.cfm?congress=115&session=1&vote=00199">vote distribution</a>, you’ll see that the vast majority of both major parties voted for the bill. The same damn people who insisted (and sometimes <b>still</b> insist) that single-payer is too expensive, that tuition-free college is too expensive, that we can’t afford to build our infrastructure — those same goddamn people had the nerve to vote for this bill. The same people who deride Sanders and other progressives as pie-in-the-sky idealists who don’t know how to make actual policy vote for an increase in the military budget that would <a href="https://popularresistance.org/senate-military-spending-increase-alone-could-fund-free-college/"><b>more than cover</b></a> providing tuition-free college.</p>
<!--more-->
<p>And then there’s the debate. Or rather, the lack thereof. There’s no discussion of how we will pay for it — it’s assumed that we’ll “find a way”. There’s no discussion of whether it’s needed, or whether there are better ways to spend the money. There’s no discussion of what the military will spend it on.</p>
<p>And there is very good reason to be suspicious of giving the military additional money — they have literally <a href="http://www.thefiscaltimes.com/2015/03/19/85-Trillion-Unaccounted-Should-Congress-Increase-Defense-Budget">“lost” $8.5 <b>trillion</b></a> (they don’t know how the effing hell they spent it). And you’re telling me that, given that, we should <b>increase</b> their damn budget? But again, there are absolutely zero questions. There aren’t publicized debates or town hall discussions to see what the public thinks. There’s no discussion of how this will be paid for. There’s no discussion of whether this is the best goddamn use of our money.</p>
<p>And the next time you want to praise the effing Democrats for being part of “The Resistance”, just remember that they gave Trump’s military $80 billion/year more to play with — the same military whose head (the President) they supposedly distrust and think is cuckoo, wacko, bonkers (oh wait, that must be my British English seeping in xD), etc. But apparently they trust him enough to let his military buy more weapons, rain death on more countries, and generally create more death, chaos, destruction, and terror in other countries.</p>
<p>Remember that both the Democrats and Republicans voted for more terrorism. Republicans have the excuse that the President is “their” party. What the eff is the Democrats’ excuse?</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
