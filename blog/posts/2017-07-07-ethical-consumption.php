<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Ethical consumption</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Ethical consumption</h1>
      <article>
    <section class="header">
        Posted on July  7, 2017
        
    </section>
    <section>
        <p>I wanted to take some time to discuss something I’ve noticed recently.</p>
<p>The first incident was a week or so ago when I was in a restaurant with my family and there happened to be M&amp;Ms on the table. My sister asked if I wanted some and I declined, saying that I didn’t feel good eating chocolate that had most likely been harvested with child labor (and possibly child slavery). My dad then told me to stop talking about it.</p>
<p>The second incident happened yesterday when a couple of us went to the movies. Something very similar to the M&amp;M incident happened, but regarding Godiva (which apparently might be a bit better than Mars at sourcing their chocolate?). And in this case, it was kind of seen as a joke or something that could be ignored.</p>
<p>Now it’s not that these people aren’t liberals — my parents and sister are pretty liberal, as are my friends here at Brown. But a curious phenomenon happens when it comes to what we buy.</p>
<!--more-->
<p>We talk a big game about ensuring workers are treated fairly and ensuring the environment isn’t destroyed, but when it comes to our own purchasing choices, we don’t seem to care. Not only that, we balk when confronted with information which might possibly make that brand/product unpalatable. And often, the first reaction is to try to shut the other person down: “Stop talking about it”, “You’re a piece of crap”, and so on.</p>
It’s not for lack of information either — there are several apps that are convenient and easy to use, providing the information we ostensibly want at the tap of a button. Some examples are (Google Play Store links given):
<ul>
<li>
<a href="https://play.google.com/store/apps/details?id=com.app.p5972GE">Better World Shopper</a>
</li>
<li>
<a href="https://play.google.com/store/apps/details?id=com.buycott.android">Buycott</a>
</li>
<li>
<a href="https://play.google.com/store/apps/details?id=au.org.goodonyou.goodonyou">Good On You</a>
</li>
</ul>
<p>Yet so many of us don’t actually want to make those choices and consciously change our purchasing habits to reward companies that are sustainable, that treat their workers well, and so on. So many of us don’t <b>want</b> that information because it shatters our image of that company.</p>
<p>Here’s the thing though: I don’t think it’s possible to care about people and the planet and <b>not</b> seek out this information. I feel like if you actually do care, you <b>should</b> seek out this information (again, available at the tap of a button). Like, if you don’t care, that’s another thing entirely — convincing you to buy sustainably if you don’t care about the environment just doesn’t make sense. But if you care about the environment — you donate to NWF and Sierra Club, recycle, and maybe even compost — why <b>wouldn’t</b> you try to make better decisions as to the products you buy? Why <b>wouldn’t</b> you boycott companies which are destroying rainforests or polluting the water? Why <b>wouldn’t</b> you boycott companies which spew out huge amounts of CO2? Why <b>wouldn’t</b> you support companies which source their materials sustainably? Why <b>wouldn’t</b> you support companies which ensure they minimize water and air pollution? It just makes sense. And if you care about worker’s rights, why <b>wouldn’t</b> you boycott companies which employ slave labor? Why <b>wouldn’t</b> you boycott companies which exploit the hell out of their workers? And why wouldn’t you support companies which ensure their workers are treated well and aren’t exposed to dangerous chemicals? Again, it just makes sense.</p>
<p>I understand that in some cases, you don’t have much of an option for various reasons. For example, I am currently insured by Independence Blue Cross, a company which gets an “X” from <a href="https://betterworldshopper.org">Better World Shopper</a> (they rank among the worst 20 companies in their database). But at this point in time, there isn’t much I can do since I am under my parents’ insurance. Even in the future, the insurance company that covers me will be decided by my employer, so I may still not have much of a choice. And maybe some of these products are very expensive. For example, sustainable textiles are generally pretty expensive (as I recently found out). But even there, you have gradations of companies — for example, some clothing companies are better than others at reasonable working conditions in their factories. If you end up buying from even a “C” or “B” company (on the Better World Shopper scale), that’s still far better than a “D” or “F” (or “X”) company.</p>
<p>My point is that if you’re a liberal who cares about this stuff, there’s really no reason not to try to switch to better and more sustainable alternatives when possible. This also includes <a href="https://www.ran.org/banking_on_climate_change">banking</a> and <a href="https://www.fastcompany.com/40417097/how-to-invest-your-money-responsibly-sustainably-or-for-impact-theyre-not-the-same">investing</a>, including retirement accounts. Given that so many sustainable (and affordable!) alternatives exist, it seems somewhat hypocritical for someone who purportedly cares about these things to not make the switch.</p>
<p>Thanks Joseph Fichera for getting me to think about this yesterday 😊</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
