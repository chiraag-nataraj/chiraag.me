<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - What a Trump presidency might mean</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>What a Trump presidency might mean</h1>
      <article>
    <section class="header">
        Posted on November  9, 2016
        
    </section>
    <section>
        <p>I’ve seen a lot of posts going around about what a Trump presidency might mean for them and I wanted to jump in a bit. This is going to be a fairly long post, but I feel I need to just kind of get this out there. I’ve mostly been posting sarcastic stuff or exaggerated stuff about Trump’s victory. But I wanted to honestly discuss what the results of this election tell me.</p>
<p>The one silver lining is that Clinton won the popular vote. So yes, she won the popular vote and lost the electoral vote.</p>
<p>I would like to think that I and people like me (people with brown skin) are welcome in this country. But with the election of Trump, I’m not so sure. Pretty much half the country seems to endorse the rhetoric that “Muslims” (read: people with brown and black skin) should be placed under increased surveillance in this country. Pretty much half the country seems to endorse the rhetoric that “Muslims” should not be trusted. Of course my friends all know me as an atheist Hindu, but would a cop on the street be able to tell that? Would a random person on the street be able to tell that? If I were not already in my PhD program, I would very likely consider moving to a different country — Canada, most likely. I know many people say this as an election joke, but given what this country has stated about me and people who look like me? I would very much like to act on that right now.</p>
<!--more-->
<p>I am fearful for my LGBTQ friends who will have to deal with the ramifications of a Trump/Pence ticket. Pence has been <b>virulently</b> anti-LGBTQ, and Trump hasn’t been much better. Pence supports conversion therapy, which is basically electric fucking torture, to “cure the gays”. Trump has said he’ll put people on SCOTUS who will overturn marriage equality. But again, I feel this needs to be looked at in a broader context. It was quite clear throughout the campaign that Trump and Pence were not pro-LGBTQ. So what does it say when this country elected these guys to lead it? What does it say about the <b>culture</b> of this country when Trump/Pence can run on this platform and win <b>handily</b>?</p>
<p>I could go on and on, but I think you get the idea. I understand that there are probably many people who voted for Trump out of sheer hate for Clinton. And you know what? I actually kinda get that. But whether or not you <b>meant</b> to endorse his platform and positions by voting for him, that’s exactly what you did. And at this point, I’m not sure how I can still live in this country while recognizing that so much of the country cares so much about voting against someone who’s corrupt that they voted in…someone who’s corrupt (in a different sense) <b>and</b> strictly worse for LGBTQ people, brown Americans, women, Latino Americans, and so on and so forth.</p>
<p>Again, I understand that there are quite a few people who voted for Trump out of sheer hate for Clinton. But now I’m begging you. Please, stand with your LGBTQ friends and family against bigotry. Please, stand with your brown friends and family (yes, there are brown people who voted for Trump) against bigotry and stereotyping. Please, stand with your Latino friends and family against bigotry and stereotyping.</p>
<p>Show me that this is merely a repudiation of Clinton, not an endorsement of Trump.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
