<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Reducing plastic use</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Reducing plastic use</h1>
      <article>
    <section class="header">
        Posted on March 19, 2018
        
    </section>
    <section>
        <p>Spurred by Frances’ recent post, I wanted to document a couple of ways here that I’ve been trying to reduce my plastic usage as well as ask for more ideas.</p>
<p>These are all fairly easy to start doing, and most will end up saving you money in the long run.</p>
<ol>
<li>
I switched to using a <a href="https://www.brushwithbamboo.com/shop/bamboostraws/">bamboo straw</a>. The hardest thing is remembering to carry it with me. This is easy at the moment — since I usually have my backpack with me, I just keep it in there.
</li>
<li>
I have reusable shopping bags which I use whenever I go grocery shopping.
</li>
<li>
For vegetables and fruits, I use <a href="https://www.amazon.com/Earthwise-Reusable-Mesh-Produce-Bags/dp/B005E2QRPG/">mesh bags</a> and always prefer loose produce to packaged stuff. This has allowed me to eliminate plastic from my vegetable consumption and drastically reduce the plastic from my fruit consumption (I can’t do much about berries).
</li>
<li>
For non-perishable goods, I’ve started buying in bulk, since that reduces the overall amount of plastic I use (volume versus surface area scaling) — I’ve found that <a href="https://nuts.com">Nuts.com</a> and <a href="https://spicesinc.com">Spices Inc</a> are pretty great for this.
</li>
<li>
I’ve switched from liquid soap (for showers) to bar soap, which eliminates the plastic involved there. I’m debating switching to bar soap for hand soap as well in the future, once my current supply of liquid soap is finished. I can use the same soap as shampoo, but I haven’t found any good hair conditioner bars.
</li>
<li>
For tea, I usually buy loose-leaf or make sure the bags are made with natural fiber (the packaging for the tea itself is often cardboard, so that’s usually not a concern).
</li>
<li>
I’m debating switching to lotion bars as well, but that would only be after my current supply is finished (and that will take a while…) — again, any recommendations?
</li>
<li>
My toothbrush has a replaceable head, so I don’t ever need to throw out the entire brush — the only improvement would be such a system with brush heads made of natural fibers instead of plastic-derived materials…<a href="https://thegoodwellcompany.com/products/be">Be. Brush</a> comes to mind.
</li>
<li>
I’m planning on switching to a <a href="https://www.ifyoucare.com/household/compostable-trash-bags/">compostable trash bag</a> instead of the standard plastic ones. Does anyone have experience with these?
</li>
<li>
I’ve been trying to rely on containers made with PCR (post-consumer recycled) plastic as much as possible whenever plastic is the only option, but…that’s not always possible :/
</li>
<li>
Instead of zip-locs, I’ve been using <a href="https://rezip.com/collections/food-on-the-go">(re)zip bags</a> which are meant to be reusable (so they’re more durable and made from food-grade silicone).
</li>
<li>
I bought a <a href="https://stojo.co/collections/biggie">food-grade silicone collapsible cup</a> (I would recommend the one I linked to rather than the one I bought, which was only 12oz, too small to take to some places).
</li>
<li>
I have a glass water bottle I use pretty much everywhere (at this point, I just always have it in my backpack) — obviously metal or otherwise works here as well (especially if you keep dropping your water bottle).
</li>
<li>
I am now getting my bread from a local bakery — I bring my bulk foods bag from Whole Foods (it’s a cloth bag with a drawstring tie) and have them put it in that.
</li>
<li>
When I get green chilies from my local Indian store, I bring my own bag and take it from the loose chilies he has (instead of the packaged ones).
</li>
</ol>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
