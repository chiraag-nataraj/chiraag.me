<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - That Google memo</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>That Google memo</h1>
      <article>
    <section class="header">
        Posted on August  8, 2017
        
    </section>
    <section>
        <p>Here is the <a href="https://gizmodo.com/exclusive-heres-the-full-10-page-anti-diversity-screed-1797564320">original memo</a>. I’m just going to try to comment on the whole thing. Bear with me here.</p>
<p>Background: I actually agree with this — we <em>do</em> all have invisible biases, and we all <em>would</em> do well to examine ourselves to see what those biases are. Discussion with those who disagree with us (hello CCC!) can definitely help us figure out what those biases.</p>
<p>Google’s Biases: I don’t know enough about Google to comment on this.</p>
<p>Left Biases: Can confirm. Am a socialist ;)</p>
<p>Right Biases: I presume these are decently correct.</p>
<p>Rest of that paragraph: Again, I don’t know enough about Google’s culture specifically, so I can’t really speak to that. I do know, though, that in many “left”-majority spaces, there isn’t as much tolerance for dissenting views as I’d like. Notably, though, this applies to people on both sides of the nominal “left”, which is interesting (see: basically any news show where the supposed “liberal” comes on the TV to berate Sanders supporters for not falling in line behind corporate Democrats). So while I agree it is a problem, I don’t think it is only applied to people to the right of the nominal “left”.</p>
<!--more-->
<p>Possible non-bias causes of the gender gap in tech: I’m skeptical of this, but I presume this person will back up these assertions (or at least explain which biological traits they’re referring to) in the following paragraphs. One thing I’d like to point out is that all of these things seem testable, so it would be great if someone knows of any papers that back up this person’s assertions. Also, “Many of these differences are small and there’s significant overlap between men and women, so you can’t say anything about an individual given these population level distributions.”. If that is true, then we shouldn’t see much of a discrepancy in the involvement of men and women in, say, tech (or any other industry).</p>
<p>Personality differences: Okay, so these are bold claims without any papers or links to research to back them up. Also, they <em>do</em> come across as sexist, given that a standard trope has been “Women are highly emotional and men don’t have any emotion at all” (for example).</p>
<p>Men’s higher drive for status: “Status is the primary metric that men are judged on, pushing many men into these higher paying, less satisfying jobs for the status that they entail.” I actually agree with that, but I don’t think it’s biological as this person seems to imply. I do agree it is present in most cultures, but, for example, there were (are?) some matriarchal societies among Native Americans — did they also judge men and women based on similar metrics?</p>
<p>Non-discriminatory ways to reduce the gender gap: Some of these things seem like valuable things to do anyway, but again, if the differences are small (as this person claimed before), then none of these should have a huge effect. For example, fostering cooperation is great, regardless of whether it gets more women into tech — it lowers stress and builds a sense of teamwork and cameraderie. The male gender role <b>is</b> currently inflexible, and we should <em>all</em> be working to dismantle that.</p>
<p>The harm of Google’s biases: If there are certain groups which are historically underrepresented in tech, then it makes sense to have programs specifically for them. That doesn’t mean there shouldn’t be other, more general programs, but I don’t see the problem with having a set of classes designed specifically for people who are underrepresented. Regarding the reconsidering a group if it’s not diverse enough…what does “too diverse” mean? That is, when <em>should</em> they reconsider? Regarding the OKRs, I’m not sure what this person is insinuating — maybe someone can provide more context/info? Regarding the Communism screed, I’ll just say this: The current liberal ideology (most vociferously espoused by the Democrats) focuses on race and gender because they are bought by the same corporations that the GOP is bought by. They don’t focus on class conflict precisely because that would require going against their donors and they don’t want to do it. Now of course, there are some socialists (like myself) who see the world as an intersection of privilege of all types, from class privilege to racial privilege to gender privilege to abled privilege and so on and so forth. You might have certain privileges (I’m cis male, abled, and not too poor, for example) while lacking others (I’m brown-skinned, for example). So in this worldview, race and gender and all this other stuff plays a role, but it’s no more or less important than class. Also, contrary to what some socialists believe, I don’t think everything can be resolved by overthrowing capitalism. Doing so will certainly help, but it won’t get us all the way there. Anyway, back to this memo.</p>
<p>Why we’re blind: I’m not sure why this person thinks liberals and progressives liked IQ tests, given that they were originally created to show that African Americans are inferior (as a race) to white Americans (this was initially used to deny them the ability to serve in the military). As for sex differences, I’d really need to see papers or other research showing what this person claims is true. I thought they might point to something in the memo, but that appears not to be the case. There are certainly some physical differences (due to hormonal differences), but the idea of “male” and “female” brains just isn’t that accurate (https://www.newscientist.com/article/dn28584-a-welcome-blow-to-the-myth-of-distinct-male-and-female-brains/). As for having programs specifically for women, it makes sense if you accept the idea that, as a society, we have been discriminating against women for a long time. Just removing those barriers isn’t enough, as things like wealth tend to accrue over time (which then changes the opportunities you have access to). As for society not giving a shit about men, <b>yes</b>. I agree with that. If a man needs support, he’s usually fucked due to societal norms. I agree with that! But that doesn’t mean we take away the programs we have in place to undo the decades and centuries of discrimination against women. Regarding PC culture, again, I agree that there is somewhat of a problem, but I can guarantee you that even this person (and probably most of you) would agree that there are some things that should probably have you ostracized. So it’s not really that it’s a thing — it’s that you don’t agree with the specific topics being placed off-limits.</p>
<p>Suggestions:</p>
<ul>
<li>
De-moralize diversity: Eh. The problem is that equality of opportunity (the underlying issue for which diversity is being used as a short-hand metric) <b>is</b> a moral issue, and it should be one that most people agree on (even if we don’t agree on how to get there).
</li>
<li>
Stop alienating conservatives: Sure. I do think that we need a more free atmosphere in terms of topics that are on-limits in most contexts.
</li>
<li>
Confront Google’s biases: Sure? Again, I don’t really know much about their culture, so I’d have to take this author at their word.
</li>
<li>
Stop restricting programs: Again, this totally depends on why those programs exist in the first place (most likely to correct for historical discrimination).
</li>
<li>
Costs and benefits of diversity: I don’t think those things are equivalent, since one of those is a <b>good</b> for society (someone has a job, makes money, pays taxes, and generally is a productive member of society), while the others are unequivocally bad, and we should be trying to reduce them. But yes, in a way, we <em>should</em> try to equalize those metrics as well — by bringing down the proportion of men who fall into that stuff.
</li>
<li>
Psychological safety: YES.
</li>
<li>
De-emphasize empathy: Hell no. Empathy is what makes us human, and it’s what actually forces us to care for others. De-emphasizing it will almost <em>certainly</em> make things worse.
</li>
<li>
Prioritize intention: The problem is that whether or not you intend something has no bearing on the outcome of that action. We <em>should</em> be more forgiving, but that doesn’t mean we should stop trying to educate one another and try to avoid hurting other people (especially once you’ve been told that “x” bothers this person).
</li>
<li>
Human nature: I’d love to see some research on this. This person seems to appeal to ‘science’ without citing scientific articles or papers or anything. They just say “This is known to science” and don’t bother to actually back up their claim.
</li>
<li>
Unconscious Bias training: I have absolutely no idea what that training says, so I can’t comment on it.
</li>
</ul>
<p>Welp. That went longer than I expected.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
