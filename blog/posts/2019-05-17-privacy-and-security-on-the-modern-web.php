<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Privacy and security on the modern web</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Privacy and security on the modern web</h1>
      <article>
    <section class="header">
        Posted on May 17, 2019
        
    </section>
    <section>
        <p>The modern web is fantastic — it offers super advanced functionality and APIs to the point where Progressive Web Apps are actually plausible replacements for full-blown native programs (although I personally am against them, it’s clear that they satisfy the use cases for the vast majority of users, given their popularity).</p>
<p>But the modern web is also a privacy nightmare. Trackers, analytics, you name it, it’s there. Javascript APIs to sniff location, bluetooth state, <em>battery state</em> — it’s ludicrous. Browser fingerprinting, where the interested party uses attributes of your browser (such as screen size, color depth, operating system, and so on) to track you (possibly <em>without cookies</em>), is more of a concern than ever. All of this on top of the fact that <em>browser makers themselves</em> have access to a wealth of information about the user, something that doesn’t escape the notice of e.g. Google or Microsoft.</p>
<p>So what can we do? What can we, the humble users of these products, do to protect ourselves from all this tracking? Well…as someone who’s been trying to fight this stuff since I learned about it, I’ve come up with a couple of tricks, which seem to work fairly well.</p>
<p>Of course, standard disclaimer: Please take this with a grain of salt! There may be things I’m missing and haven’t thought about. If you’re in a situation where a slip-up may cost you your life, <em>please seek out solutions built for your specific situation</em> — this is more for the general user who cares about privacy but doesn’t face dire consequences should they be unmasked.</p>
<ol>
<li>
<strong>Use Firefox</strong>. It’s by far the most privacy-respecting browser out there of the mainstream browsers and they’ve been integrating some awesome stuff from Tor (via the “Tor Uplift”) that we’ll cover in later points. Some of the later mitigation techniques <em>rely</em> on Firefox (or a fork which implements those points). See my note about Chromium-based privacy browsers at the end for more information.
</li>
<li>
<strong>Use a VPN</strong>. I won’t go into it here (maybe I’ll do that in a future post), but one of the biggest identifiers you will <em>always</em> send is your IP address — it’s how your browser is able to get any websites at all. The problem is that your IP address can easily be tied to a general vicinity (so doing things like disabling the Geolocation API as we do below won’t really help, since they can just look up your IP address). Using a VPN (look for a <strong>no-logging</strong> VPN) can help by effectively disguising your location. As a bonus, using one that is fairly popular means that your IP address becomes mostly worthless, at least on its own. And since that’s largely the one identifier you <em>have</em> to send, masking it effectively is <em>crucial</em> to a proper privacy routine.
</li>
<li>
<strong>Disable various Javascript APIs</strong>. In Firefox, you can disable the following APIs by going to <kbd>about:config</kbd>:
<ul>
<li>
Beacon API (Set <kbd>beacon.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Battery API (Set <kbd>dom.battery.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Performance API (Set <kbd>dom.enable_performance</kbd> to <kbd>false</kbd>)
</li>
<li>
Resource Timing API (Set <kbd>dom.enable_resource_timing</kbd> to <kbd>false</kbd>)
</li>
<li>
Clipboard events API (Set <kbd>dom.event.clipboardevents.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Context menu event (Set <kbd>dom.event.contextmenu.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
High resolution timestamp (Set <kbd>dom.event.hirestimestamp.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Asynchronous Clipboard API (Set <kbd>dom.event.asyncClipboard.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
FileHandle API (Set <kbd>dom.event.filehandle.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Gamepad API (Set <kbd>dom.gamepad.enabled</kbd>, <kbd>dom.gamepad.extensions.enabled</kbd>, and <kbd>dom.gamepad.haptic_feedback.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
IndexedDB API (Set <kbd>dom.indexedDB.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Pointer Lock API (Set <kbd>dom.pointer-lock.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Service Worker API (Set <kbd>dom.serviceWorkers.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
<del>
Storage API (Set <kbd>dom.storage.enabled</kbd> to <kbd>false</kbd>)
</del>
<ins>
Leave this one enabled! Disabling it tends to break many websites
</ins>
</li>
<li>
Vibration API (Set <kbd>dom.vibrator.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Oculus VR API (Set <kbd>dom.vr.oculus.enabled</kbd> and <kbd>dom.vr.oculus.invisible.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
WebAudio API (Set <kbd>dom.webaudio.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Notifications API (Set <kbd>dom.webnotifications.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Window.event API (Set <kbd>dom.window.event.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Geolocation API (Set <kbd>geo.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
Web Speech API (Set <kbd>media.webspeech.synth.enabled</kbd> to <kbd>false</kbd>)
</li>
<li>
WebGL (Set <kbd>webgl.disabled</kbd> and <kbd>webl.disable-webgl</kbd> to <kbd>true</kbd>)
</li>
</ul>
There are more preferences for other APIs which I did not include as they are disabled by default, and some of these may break some websites or your particular workflow, so play around with enabling and disabling these APIs until you reach a state that works for you.
</li>
<li>
<strong>Enable Contextual Identities (Containers)</strong>. In order to prevent various methods of client-side tracking (think: cookies, local storage, etc), enable and <em>use</em> Firefox Containers. It is basically a generalization of Private Browsing mode, where each container has its own set of cookies, local storage, and so on. Containers are built-in to more recent versions of Firefox, but to enable them and use them effectively, you should install <a href="https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/" target="_blank" rel="noopener noreferrer">Multi-Account Containers</a>. Using that extension, you can create new containers, open a tab in a specific container, and so on. A great companion extension is <a href="https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/" target="_blank" rel="noopener noreferrer">Temporary Containers</a>, which can be set up to automatically create and delete containers based on certain rules. For maxmium privacy, enable “Deletes History” Temporary Containers (in “Advanced”) and tell the extension to isolate subdomains. <strong>Warning</strong>: This <em>will</em> break logins on several webpages, so either be prepared to create exceptions for certain websites or have a separate profile for logging into sites.
</li>
<li>
<strong>Enable First-Party Isolation</strong>. First-Party isolation automates (to some extent) the segregation provided by Containers and can act as another layer of defense, <em>especially</em> in the case that you don’t use Temporary Containers or another extension like it to automate the segregation. Imagine that sites A and B both have a Facebook tracker embedded. When Facebook stores a cookie through site A, the cookie will be called <kbd>A.facebook.com</kbd>. When Facebook stores a cookie through site B, the cookie will be called <kbd>B.facebook.com</kbd>. This is a very simplistic explanation, but it hopefully helps get the point across of the value of First-Party Isolation — it helps ensure that common third parties (read: Facebook, Google, Amazon) cannot easily track you across sites — <em>even</em> if they’re all loaded in the same container. To enable First-Party Isolation, go to <kbd>about:config</kbd> and set <kbd>privacy.firstparty.isolate</kbd> to <kbd>true</kbd>.
</li>
<li>
<p>
<strong>Enable fingerprinting resistance</strong>. This preference helps ensure that your computer looks “the same” as any other computer using this preference. That is, it tries to help foil the browser fingerprinting techniques discussed in the opening of this post. Keep in mind that this <em>may</em> cause issues with certain websites, and unfortunately there is no easy way to disable the preference on a specific site. That being said, I personally have not run into too many issues when browsing with this preference enabled. To enable fingerprinting resistance, go to <kbd>about:config</kbd> and set <kbd>privacy.resistFingerprinting</kbd> to <kbd>true</kbd>. If you want to be notified of websites attempting to read your canvas data (the HTML5 Canvas is yet <em>another</em> fingerprinting mechanism, so fingerprinting resistance obfuscates your canvas data), you should also set <kbd>privacy.resistFingerprinting.autoDeclineNoUserInputCanvasPrompts</kbd> to <kbd>false</kbd> — this can help if something like WhatsApp Web (which generates a QR code) silently fails.
</p>
<p>
<strong>Note</strong>: This preference <em>can</em> break some things. Most notably, it may prevent you from installing certain extensions the usual way (e.g. by clicking on the “Add to Firefox” button on addons.mozilla.org. You can get around this two ways:
</p>
<ul>
<li>
Temporarily disable the preference, install the extension, and toggle it back.
</li>
<li>
Right click on the “Add to Firefox” button and click “Inspect Element”. Copy the <kbd>href</kbd> attribute and paste it — this will bypass the user agent check that addons.mozilla.org does and allow you to install the extension.
</li>
</ul>
<p>
That being said, <em>most</em> extensions (in my experience) install just fine even when fingerprinting resistance is enabled.
</p>
</li>
<li>
<strong>Enable built-in tracking protection</strong>. It’s useful to have a decent content blocker enabled as a fallback to when you need to disable some of the extensions below (e.g. uBlock Origin or uMatrix) because they’re too aggressive (it happens rarely, but it <em>does</em> happen). To enable this, go to <kbd>about:preferences</kbd> and select the Privacy tab. You should enable Firefox’s built-in tracking protection using the strict list in all windows (select “Custom” and tick the appropriate boxes). You can also tell Firefox to reject all third-party cookies as well to at least get <em>some</em> additional privacy if you’re not going to use Cookie AutoDelete. If you’re using Cookie AutoDelete, you can go ahead and uncheck the Cookie box entirely, since having multiple places cookies are handled can get a bit confusing and hard to manage.
</li>
<li>
<strong>Change your search engine</strong>. All of this is useless if you’re still using Google as your search engine! I recommend either <a href="https://startpage.com">Startpage</a> or <a href="https://duckduckgo.com">DuckDuckGo</a>. In Firefox, you can do this by going to <kbd>about:preferences</kbd> and clicking on the Search tab.
</li>
<li>
<strong>Install privacy-enhancing extensions</strong>. While the last few tricks only worked on Firefox (now you can see why I recommend it), this tip should work on both Chromium-based and Firefox-based browsers. The list of extensions I use as well as what they do is given below:
<ul>
<li>
<strong>Cookie AutoDelete</strong>: Automatically delete cookies once they’re no longer needed. <a href="https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>Decentraleyes</strong>: Intercept and serve scripts and stylesheets from popular content delivery networks (CDNs), thereby ensuring you never connect to them. <a href="https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>firewall</strong>: Allow editing of user agent, headers, etc. <a href="https://addons.mozilla.org/en-US/firefox/addon/firewall-2017/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>HTTPS Everywhere</strong>: Easily disable all unencrypted connections (prevents eavesdropping, man-in-the-middle-style attacks). <a href="https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>Privacy Possum</strong>: Obfuscates data commonly used by trackers (mainly a fallback mechanism if everything else fails). <a href="https://addons.mozilla.org/en-US/firefox/addon/privacy-possum/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>Request Control</strong>: Remove tracking parameters from URLs, disable certain classes of resources from loading. <a href="https://addons.mozilla.org/en-US/firefox/addon/requestcontrol/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>Skip Redirect</strong>: Automatically skip to the final request (useful when you have a tracking URL that redirects to the actual website after logging that you’re visiting it — see: Google Search). <a href="https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>uBlock Origin</strong>: Flexible content blocker (blocks ads, malware, etc). <a href="https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
<li>
<strong>uMatrix</strong>: More advanced content blocker (you can block or allow certain requests from certain domains on certain domains — for example, you can allow facebook to load, but <em>only</em> on facebook.com (and block it everywhere else)). <a href="https://addons.mozilla.org/en-US/firefox/addon/umatrix/" target="_blank" rel="noopener noreferrer">Firefox</a>
</li>
</ul>
</ol>
<p>There are probably things I missed — stuff I did long ago that I set once and completely forgot about. Let me know if I missed something — if there are additional APIs I should be disabling or additional extensions I should be using.</p>
<p>I’ll also try to keep this updated as additional APIs (and methods to disable them) crop up or as additional privacy-enhancing extensions appear.</p>
<p><em>Note</em>: Many of the mitigation techniques I describe here (for example, First-Party Isolation, fingerprinting resistance, and containers) are only available on Firefox. While you can obtain some measure of protection through the extensions listed at the end (which exist on Firefox-based and Chromium-based browsers) and while there are <em>some</em> fingerprinting mitigations available in e.g. <a href="https://github.com/Eloston/ungoogled-chromium/blob/master/docs/flags.md">ungoogled-chromium</a>, I have come to the conclusion that if you want privacy, you <em>need</em> to use something Firefox-based (whether Firefox or a fork/derivative that keeps up with the latest features privacy-wise). There is simply no match for the depth and breadth of features and options available to Firefox users to lock down the browser.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
