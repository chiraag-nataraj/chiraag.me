<!DOCTYPE html>
<?php include("../hsts.php") ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include("../base.php") ?>
    <title>Chiraag's Musings - Free trade and colonialism</title>
  </head>
  <body>
    <?php include("../navigation.php") ?>
    <div id="content">
      <h1>Free trade and colonialism</h1>
      <article>
    <section class="header">
        Posted on July 26, 2016
        
    </section>
    <section>
        <p>There are many arguments against free trade deals that we’ve seen.</p>
<p>One of the most popular is that they bleed jobs from here and ship them overseas. While that is <b>somewhat</b> true, as has been pointed out many times, automation is a much bigger factor in the loss of many manufacturing jobs, both here and abroad. Another one applies specifically to the TPP and concerns privacy rights, IP rights, and letting corporations usurp government regulations by challenging them in a “court”.</p>
<p>But I want to address today another objection, one that I haven’t seen much. One of the standard arguments for free trade deals is that they will “create jobs”, if not here, then at least in other countries. They will help them “develop”, they say. They will “lift people out of poverty”. They will help them make use of their resources and labor.</p>
<p>It may not look like it, but it’s a rehashing of the same old tired argument that white people (or countries and companies run by white people) will come in to “save” the poor savages and help them “develop”. When you say that these multinational corporations will bring jobs and bring technology, it implies that the native people are not able to do it on their own. It implies that they’re not able to actually make anything, that they need guidance from the generous white people.</p>
<!--more-->
<p>When trade deals are used to justify multinational corporations swooping in, undercutting any local businesses that exist, and tying all local labor to their particular model and particular way of doing things, those same deals prevent any significant local industries from developing to compete. Those same deals prevent local talent from flourishing. Those same deals allow multinational corporations to act like parasites, sucking any talent that exists from the local people and preventing any real industry from developing there.</p>
<p>This then further justifies the narrative that those people are incapable of doing anything and thus they need multinational corporations to come in to provide the technology and everything else, and that those people are only good as laborers.</p>
<p>And then, we look at those countries and think “Why…how backward those countries are!”. No shit, Sherlock. If you have outside companies coming in, destroying any local industries that exist, sucking all the labor out of the locals, and keeping most of the profit, <b>of course</b> there won’t be much money to really develop anything local. Of <b>course</b> there won’t be any money to feed towards infrastructure and scientific progress and god knows how many other things.</p>
<p>Not to mention that many times those same corporations are as much drivers of corruption as they are victims.</p>
<p>Trade is good. Obviously not everyone can produce everything and be self-sustaining. But we <b>have</b> to place limits on how corporations take advantage of resources, including labor. And we <b>have</b> to realize that “free trade” is intimately linked to the colonialist attitudes that preceded it.</p>
    </section>
</article>

      <hr />
      <?php include("../footer.html") ?>
    </div>
  </body>
</html>
