<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Research Overview</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>Research</h1>
      <h2>
        A first-principles investigation of refractory alloy systems united by a common computational framework &mdash; <a href="https://repository.library.brown.edu/studio/item/bdr:ay8pgsa7/" target="_blank">Brown Digital Repository</a>
      </h2>
      <p>
        The cluster expansion formalism for alloys is used to investigate phenomena in several refractory alloys: Co<sub>3</sub>W (superalloy), NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr (high entropy alloys). Density functional theory (DFT) is used to calculate the energies of many small structures which are used to construct a cluster expansion model for each alloy system. These models are then used as Hamiltonians for Monte Carlo simulations in order to explore the dependence of the antiphase boundary (APB) energy on temperature and Al and Hf impurity concentration in metastable FCC Co<sub>3</sub>W as well as phase segregation behavior in NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr.
      </p>
      <p>
        First, the effects of Al and Hf impurities on the (111) antiphase boundary (APB) energy of metastable FCC Co<sub>3</sub>W are investigated via ab initio calculations. It is found that Hf increases the APB energy far more than Al, particularly at higher concentrations of the impurity, and both systems exhibit little variation with respect to temperature. It is further shown that at higher concentrations of Hf, and most noticeably for Co<sub>3</sub>(W<sub>0.5</sub>Hf<sub>0.5</sub>), Hf and W tend to segregate into alternating planes, unlike the corresponding Co<sub>3</sub>(W<sub>0.5</sub>Al<sub>0.5</sub>), which explains the different impacts of the two impurities on the APB energy. Finally, the ratio of (111) to (100) APB energies is studied for sacrificial W compositions to understand cross slip behavior in both ternary systems.
      </p>
      <p>
        Second, the phase segregation behavior of three key refractory high entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr) is studied using first-principles calculations. Phase segregation and intermetallic phases documented in the experimental literature are reproduced in all three high entropy alloys and thermodynamic integration is used to calculate the configurational entropy of these alloys as a function of temperature. Our results show that at low temperatures, the configurational entropy of these materials is largely independent of N, which suggests that alloy design guidelines based on the ideal entropy of mixing require further examination.
      </p>
      <h2>A systematic analysis of phase stability in refractory high entropy alloys utilizing linear and non-linear cluster expansion models &mdash; <a href="https://doi.org/10.1016/j.actamat.2021.117269" target="_blank">Acta Materialia</a></h2>
      <p>
        The phase segregation behavior of three key refractory high entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr) is studied using first-principles calculations. Several linear and non-linear methods are utilized to generate surrogate models for three key refractory high entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr) via the cluster expansion formalism. The characteristics of each of the generated models is explored and the regression methods are compared. Finally, these surrogate models are utilized to generate Monte Carlo trajectories in order to explore the link between phase segregation and previously documented mechanical degradation in these materials.
      </p>
      <p>
        Phase segregation and intermetallic phases documented in the experimental literature are reproduced in all three high entropy alloys. NbTiVZr forms vanadium and zirconium clusters at lower temperatures (250 K) which disperse into the single-phase matrix by 1000 K. HfNbTaTiZr forms HfZr, NbTa, and possibly TiZr intermetallic phases at lower temperatures (250 K). Unlike the other HEAs studied here, HfNbTaTiZr does not lose short-range ordering in the solid state until around 3500 K, which is above its melting temperature. AlHfNbTaTiZr forms NbTa and AlHfTiZr phases at lower temperatures (250 K), which are not observed at higher temperatures (1000 K).
      </p>
      <h2>First-principles study of the effect of Al and Hf impurities on Co<sub>3</sub>W antiphase boundary energies &mdash; <a href="https://doi.org/10.1016/j.actamat.2021.117075" target="_blank">Acta Materialia</a></h2>
      <p>
        The effects of Al and Hf impurities on the (111) antiphase boundary (APB) energy of metastable FCC Co<sub>3</sub>W are investigated via ab initio calculations. Cluster expansions are used to predict the total energies of supercells containing non-dilute concentrations of impurities using Monte Carlo simulations at relevant temperatures, giving APB energies as a function of impurity concentration and temperature for each ternary system. The cluster expansions are validated by comparing with direct energy calculations of supercells of pure L1<sub>2</sub> Co<sub>3</sub>W and the effects of each impurity are compared. Two sets of compositions are explored for each system — constant ratio (constant ratio between Co and W) and sacrificial W (constant Co). It is found that sacrificial W compositions stabilize the L1<sub>2</sub> structure over a wider range of compositions than constant ratio compositions (given the restriction to the FCC lattice) in both systems and should be preferred. In sacrificial W compositions, Hf increases the APB energy far more than Al, particularly at higher concentrations of the impurity, and both systems exhibit little variation with respect to temperature. It is further shown that at higher concentrations of Hf, and most noticeably for Co(W<sub>0.5</sub>Hf<sub>0.5</sub>), Hf and W tend to segregate into alternating planes, unlike the corresponding Co(W<sub>0.5</sub>Al<sub>0.5</sub>), which explains the different impacts of the two impurities on the APB energy. Finally, the ratio of (111) to (100) APB energies is studied for sacrificial W compositions to understand cross slip behavior in both ternary systems.
      </p>
      <h2>Temperature-dependent configurational entropy calculations for refractory high-entropy alloys &mdash; <a href="https://doi.org/10.1007/s11669-021-00879-9" target="_blank">Journal of Phase Equilibria and Diffusion</a></h2>
      <p>
        The cluster expansion formalism for alloys is used to construct surrogate models for three refractory high-entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr). These cluster expansion models are then used along with Monte Carlo methods and thermodynamic integration to calculate the configurational entropy of these refractory high-entropy alloys as a function of temperature. Many solid solution alloy design guidelines are based on the ideal entropy of mixing, which increases monotonically with N, the number of elements in the alloy. However, our results show that at low temperatures, the configurational entropy of these materials is largely independent of N, and the assumption described above only holds in the high-temperature limit. This suggests that alloy design guidelines based on the ideal entropy of mixing require further examination.
      </p>
      <h2>Undergraduate Thesis &mdash; <a href="research/undergrad-thesis.pdf" target="_blank" type="applications/pdf">Full thesis</a></h2>
      <p>
	In order to study the fracture mechanics of heterogeneous materials as an avenue for attempting to characterize heterogeneous materials, it is useful to utilize alternative measures which are easier to measure while providing an understanding of the underlying phenomena. The behavior of heterogeneous materials under peeling action will be explored as a proxy for understanding fracture mechanics.
      </p>
      <p>
	The first objective is to perform a peeling experiment to investigate how different patterns on a substrate affect adhesion. Several different patterns will be tested and the force required to peel the tape off at constant velocity will be measured. I will attempt to mathematically model the results from that experiment as well as design further experiments to test hypotheses about phenomena that may arise.
      </p>
      <p>
	The second objective is to computationally model the peel front during the peeling process to track how it changes. This will thus give an idea of how the heterogeneities affect the peel front.
      </p>
      <h2>Optimizing Spinal Cord Injury Therapy &mdash; <a href="research/kinect-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of this research is to utilize depth sensor technology to track body motion. Currently, many algorithms exist to do &mdash; one of the most popular ones is SCAPE, which has two stages. The first stage is the initialization phase, whereby the generic human model is modified to resemble the actual human. The second stage is the tracking phase, whereby the generated model is then used along with keypoints to track the motion of the person. The key contribution of this research is in the first phase. Normally, the initialization is done by hand by selecting several keypoints on both the captured image and the model. This research seeks to automate that process using a combination of edge detection and point-to-point correspondence given that the initial pose is roughly known.
      </p>
      <h2>Hydraulics for Micro-Robotic Actuation &mdash; <a href="research/hydraulics-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of the proposed student research is to investigate hydraulic actuation for small and micro robotic systems, and develop control algorithms for their autonomous operation.  The objective is to implement this idea in a test system to prove the viability of the concept.  In order to ensure that the pump is as efficient as possible, equations and parameters are used in order to optimize the efficiency and force/pressure output of the pump.  Furthermore, parametrized CAD drawings of the different parts of the pump are utilized in a programmatic manner to facilitate changing parameters.  Once the pump has been optimized and fully developed, it will be used to power an actuator which can trigger an action such as e.g. an arm bringing down a hammer.  For the case of an arm and hammer, pistons can be placed both above and below the joint to allow the arm to be raised and lowered.
      </p>
      <h2>Modeling of Negative Index Metamaterials for &#8220;Hide-in-Plain-Sight&#8221; &mdash; <a href="research/nims-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of this research was to investigate the possibility of creating &#8220;hide-in-plain-sight&#8221; effects using negative index metamaterials (NIMs). The objective was to determine if and how it is possible to use NIMs in order to conceal or disguise an object.  In order to investigate these questions, POV-Ray, a raytracing software, was used to model negative index of refraction metamaterials in various configurations. Investigations were conducted based upon varying geometry and gradients of index of refraction to study the effect on an observer's perception of an object enclosed in metamaterial. Results indicate that reflectance and transmittance of a given object are the same whether the index of refraction is positive or negative. Furthermore, using certain combinations of metamaterials and ordinary materials, a variety of optical effects can be achieved. Finally, at certain viewing angles, NIMs are not necessary in order to produce &#8220;hide-in-plain-sight&#8221; effects --- positive index of refraction metamaterials would also work. These results portend that &#8220;hide-in-plain-sight&#8221; may not be far off in the future. This research was presented at the 2012 Annual Southern California Conference for Undergraduate Research.
      </p>
      <h2>Complexity in Nonlinear Dynamic Systems &mdash; <a href="research/chaos-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective was to create an auditory representation and interpretation of chaos, where a pattern called a strange attractor was reproduced in the music. A strange attractor is a particular manifestation of chaos in which one may not know the exact coordinates, but knows the coordinates will fall around a certain pattern. Chaos is a complex, dynamic system which is often analyzed using conventional, quantitative techniques. With my background and passion for music I pursued a research venture into the beautiful, but alien (to the world of math), realm of music in order to completely understand the nature of chaos. By adding this new, richer dimension into the mix, my hope is that one can immediately pick out certain aspects of chaos which are not immediately visible from conventional graphs. This research was presented at the Carderock Division of the Naval Surface Warfare Center.
      </p>
      <h2>Swarm Robotics &mdash; <a href="research/swarm-report.pdf" target="_blank" type="text/html">Paper</a></h2>
      <p>
	The objective was to implement swarm robotics using particle swarm optimization, where a group of robots uses localized computations to achieve a group task in an optimal fashion. This project was presented at two conferences, one of which was a national ASEE conference [Chiraag Nataraj, Sanjeev Reddy, Mark Woods, B. Samanta, and C. Nataraj, 2010, Swarm Robotics: A Research Project With High School Students As Active Participants,'' American Society of Engineering Education Annual Conference, Louisville, KY, June 2010, Paper No. AC 2010-1655.]
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
