<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Minimalist Wireless Networking</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>

  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>Minimalistic Wireless Networking With systemd</h1>
      <h2>Why</h2>
      <p>
	There are already many guides out there for setting up <kbd>systemd-networkd</kbd> and friends. But I want to talk about going beyond just simple networking here and describe my setup. Hopefully it will be useful to others!
      </p>
      <h2>systemd-networkd and friends</h2>
      <p>
	Setting up <kbd>systemd-networkd</kbd> and friends is pretty easy.
      </p>
      <h3>Configure systemd-networkd to manage your interface</h3>
      <p>
	First, run <kbd>networkctl</kbd> to determine which interfaces exist on your system. On mine, for example, the output is as follows:
      </p>
      <textarea title="networkctl" class="code">
$ networkctl
IDX LINK             TYPE               OPERATIONAL SETUP     
  1 lo               loopback           carrier     unmanaged 
  2 enp59s0          ether              no-carrier  configuring
  3 tunl0            tunnel             off         unmanaged 
  4 gre0             ipgre              off         unmanaged 
  5 gretap0          ether              off         unmanaged 
  6 erspan0          ether              off         unmanaged 
  7 ip_vti0          tunnel             off         unmanaged 
  8 wlp60s0          wlan               routable    configured

8 links listed.
      </textarea>
      <p>
	Anything listed as "configuring" or "configured" is being managed by <kbd>networkctl</kbd>. Wired interfaces are listed with type "ether" and wireless interfaces are listed with type "wlan". If you are using something like Network Manager or wicd, you will probably see everything listed as unmanaged.
      </p>
      <p>
	The essence of configuring <kbd>systemd-networkd</kbd> is so-called "network files" in <kbd>/etc/systemd/network/</kbd>. So go ahead and create something like <kbd>/etc/systemd/network/25-wireless.network</kbd> (it can have an arbitrary name) and open it up in your favorite text editor (as root or with <kbd>sudoedit</kbd>). The contents of this file are fairly simple:
      </p>
      <textarea title="networkfile" class="code">
[Match]
Name=wlp60s0

[Network]
DHCP=ipv4
LinkLocalAddressing=no

[DHCP]
RouteMetric=20
      </textarea>
      <p>
	Make sure to replace <kbd>wlp60s0</kbd> with your wireless interface name from above! Feel free to play around with the options or add your own (see <kbd>man systemd.network</kbd> for all of the available options — there are lots!). At this point, don't quite reload any services just yet — we need to set up <kbd>wpa_supplicant</kbd> first.
      </p>
      <h3>Configure wpa_supplicant to connect to a wireless network</h3>
      <p>
	This part is definitely the trickiest part. You have to create a configuration file at <kbd>/etc/wpa_supplicant/wpa_supplicant-&lt;interface&gt;.conf</kbd>. So in my case, the file is called <kbd>/etc/wpa_supplicant/wpa_supplicant-wlp60s0.conf</kbd>. At the top of the file, put
      </p>
      <textarea title="wpaconf" class="code">
ap_scan=1
ctrl_interface=/var/run/wpa_supplicant
      </textarea>
      <p>
	Next, you need to add network configuration blocks for the networks you want <kbd>wpa_supplicant</kbd> to connect to. The key thing to remember here is that defaults are your friend 😉 You can use my "iw enhanced" script from the <a href="programs/index.php">Some Scripts &amp; Programs</a> page to figure out what the authentication scheme is for your particular wireless network. Some examples are:
      </p>
      <h4>WPA-PSK</h4>
      <p>
	This type shows up as "Authentication Suites: PSK" in the output from <kbd>iw</kbd> and is the simplest to setup. A configuration block for this type looks like:
      </p>
      <textarea title="wpapsk" class="code">
network={
    ssid="<ssid>"
    psk=<output of wpa_passphrase here>
}
      </textarea>
      <h4>WPA-EAP</h4>
      <p>
	This type shows up for me at school (username/password combo). A configuration block for this type looks like:
      </p>
      <textarea title="wpaeap" class="code">
network={
    ssid="<ssid>"
    key_mgmt=WPA-EAP
    eap=PEAP
    identity="<username>"
    password="<password>"
}
      </textarea>
      <h4>Other</h4>
      <p>
	There are countless more examples in <kbd>/usr/share/doc/wpa_supplicant/examples</kbd>, and the documentation in general is pretty good. Go take a look there or search the web for tutorials on configuring <kbd>wpa_supplicant</kbd> if you're confused.
      </p>
      <h3>Testing</h3>
      <p>
	Take a deep breath. Now stop your network manager if you have one (Network Manager, wicd, connman, etc) using the <kbd>systemctl</kbd> command (this will differ based on which one your using — tab completion is your friend here). Once you have done that, you should restart <kbd>systemd-networkd</kbd> and start <kbd>wpa_supplicant@&lt;interface&gt;</kbd> (so in my case, it's <kbd>wpa_supplicant@wlp60s0</kbd>) and <kbd>systemd-resolved</kbd>. Within about a second or two, <kbd>wpa_supplicant</kbd> should connect to your network (you can verify by checking if <kbd>networkctl</kbd> shows "configured" and "routable" for your wireless interface or just trying to browse the internet).
      </p>
      <p>
	If this didn't work, check to make sure <kbd>systemd-networkd</kbd> is actually managing your interface (check the output of <kbd>networkctl</kbd>). If it is, next check to see that <kbd>wpa_supplicant@&lt;interface&gt;</kbd> is running (if it shows "failed", then something's probably wrong with your configuration file). If <kbd>wpa_supplicant</kbd> is running but not connected, it means the network configuration is incorrect (wrong password, wrong ssid, etc).
      </p>
      <h2>Running scripts upon connection</h2>
      <p>
	So previously, one would often run scripts using <kbd>if-up</kbd>,<kbd>if-down</kbd>, etc. However, there is a cleaner way to run stuff on network <em>connection</em>, which is often what one wants anyway. For example, in my case, I want to bring up a VPN when I'm connected to most wireless networks…except my home network (long story short, I run a VPN at the router level on my home network, so a device-level VPN is unnecessary). A <kbd>wpa_supplicant</kbd> action script can be used to do this kind of stuff very easily and conveniently, <em>especially</em> when combined with <kbd>systemd</kbd>.
      </p>
      <h3>Setting up the systemd unit file</h3>
      <p>
	In order to do this, we need to create a new service, <kbd>wpa_cli@.service</kbd>. Why? Because action scripts are run by <kbd>wpa_cli</kbd> and not the main <kbd>wpa_supplicant</kbd> process. So open up <kbd>/etc/systemd/system/wpa_cli@.service</kbd> and put in the following:
      </p>
      <textarea title="wpacli" class="code">
[Unit]
Description=Auto-connect VPN on network change (%i)
Requires=wpa_supplicant@%i.service
After=wpa_supplicant@%i.service
JoinsNamespaceOf=wpa_supplicant@%i.service
StartLimitBurst=100

[Service]
Type=simple
ExecStartPre=/usr/bin/file -E /var/run/wpa_supplicant/%i
ExecStart=/sbin/wpa_cli -i%i -a /usr/local/bin/launch_vpn
PrivateTmp=true
Restart=always
ProtectSystem=strict
ProtectHome=true
NoNewPrivileges=yes
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectControlGroups=true
MemoryDenyWriteExecute=true
LockPersonality=true
CapabilityBoundingSet=

[Install]
WantedBy=wpa_supplicant@%i.service
      </textarea>
      <p>
	(The <kbd>ExecStartPre=</kbd> makes sure the socket exists before starting the service, and <kbd>StartLimitBurst</kbd> ensures it is restarted if the socket doesn't yet exist).
      </p>
      <h3>Setting up the script</h3>
      <p>
	I called the script <kbd>/usr/local/bin/launch_vpn</kbd>, but you're free to call it whatever you want (just make sure to change the service file above accordingly!). So go ahead and open up that file in your favorite editor and put in something like the following:
      </p>
      <textarea title="launchvpn" class="code">
#!/usr/bin/env bash

IFACE="$1"
CMD="$2"

if [ "$CMD" = "CONNECTED" ]
then
    SSID=`wpa_cli -i $IFACE status | grep ^ssid= | cut -f2- -d=`
    if [[ "$SSID" != "Chiraag-VPN" ]]
    then
       systemctl start openvpn@us6-TCP-chaanakya.service
    fi
else
    systemctl stop openvpn@us6-TCP-chaanakya.service
fi
      </textarea>
      <p>
	Now of course, you can change this to be whatever you want — I'm just providing a template. One nice thing to note here is that if you're starting <kbd>systemd</kbd> units, you don't need to worry about conditionally stopping them — calling stop on a stopped unit does nothing and exits cleanly.
      </p>
      <h3>Testing</h3>
      <p>
	Now if you start <kbd>wpa_cli@&lt;interface&gt;.service</kbd>, that script should automatically run on connect and disconnect! You can easily try this by suspending and resuming your computer or by toggling the <kbd>rfkill</kbd> for your wireless interface. You should be able to debug the script by checking the journal, so if it's not doing what you expect, check there first.
      <h2>Final Notes</h2>
      <p>
	Of course, <kbd>systemd</kbd> haters will note that this was always possible, and that's certainly true. Personally, the impetus for me to even check out running <kbd>wpa_supplicant</kbd> as a standalone process was the introduction of <kbd>systemd-networkd</kbd> — until then, I had always relied on wicd or Network Manager to carry me along; those tools can sometimes get in the way of things like this (I remember I tried something similar a while back using <kbd>if-up</kbd> scripts, but those don't get the network SSID for obvious reasons, so it became too much of a hassle).
      </p>
      <p>
	If everything works, go ahead and disable your previous network manager from starting at boot and enable starting <kbd>systemd-networkd</kbd>, <kbd>wpa_supplicant@&lt;interface&gt;</kbd>, <kbd>wpa_cli@&lt;interface&gt;</kbd>, and <kbd>systemd-resolved</kbd> at boot. As one final step, you should link <kbd>/etc/resolv.conf</kbd> to <kbd>/var/run/systemd/resolve/stub-resolv.conf</kbd> to let <kbd>systemd-resolve</kbd> manage it (that way when you switch networks, everything will work as you expect).
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
