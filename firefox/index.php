<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Firefox Tips and Tricks</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
	Firefox Tips and Tricks
      </h1>
      <p>
	Firefox 57 will invalidate many of the existing useful extensions (like FaviconizeTab, which was a personal favorite). Some will see replacements. Others, however, can be replicated by existing customization options, albeit well-hidden ones.
      </p>
      <h2>
	Setup
      </h2>
      <p>
	In order for many of these tricks to be useful, you will need to create a file called <kbd>userChrome.css</kbd>. First, find your profile folder by going to <kbd>about:support</kbd> and clicking on <kbd>Open Directory</kbd> next to <strong>Profile Directory</strong> under <strong>Application Basics</strong>. Then, navigate to the <kbd>chrome</kbd> directory and copy the <kbd>userChrome-example.css</kbd> file to <kbd>userChrome.css</kbd>. Now open the file in your favorite text editor.
      </p>
      <h2>
	Disabling the cache
      </h2>
      <p>
	For privacy reasons, it can be beneficial to disable the cache in Firefox (cache-based tracking is very much a thing). There are three separate caches: memory, disk, and offline (also appcache, but I have no idea what that does...). Disabling the disk and memory cache are easy: simply toggle <kbd>browser.cache.disk.enable</kbd> and <kbd>browser.cache.memory.enable</kbd>. Disabling the offline cache in Firefox, however, can break some things in <kbd>about:preferences#privacy</kbd> (namely, the "Cookies and Site Data" part, which will hang on "Calculating size..."). The fix for this is to leave the offline cache enabled within Firefox but disable writing to the relevant directory. On Linux, this is done using <kbd>chmod u-w</kbd>. To find the offline cache directory, look for "Local Directory" in <kbd>about:profiles</kbd> or look for the disk location in <kbd>about:cache</kbd>. On Linux, this is usually under <kbd>~/.cache/mozilla/firefox/&lt;profile&gt;</kbd>. You should disable writing to the <kbd>OfflineCache</kbd> directory, which should effectively disable the offline cache without breaking Firefox. If Firefox is still doing memory caching even after toggling <kbd>browser.cache.memory.enable</kbd>, you should be able to set the pref <kbd>browser.cache.memory.capacity</kbd> to 0 (you'll have to right-click and create a new Integer preference) with no ill effects.
      </p>
      <p>
	<em>Side note:</em> You can check to see if things are being cached by opening the "Network" tool (under the "Web Development" submenu) and reloading the page. If anything shows up as "cached" in the output, you know that some cache is still working (it also doesn't necessarily show up in <kbd>about:cache</kbd>, which is very confusing...).
      </p>
      <h2>
	Reducing the width of your non-pinned tabs
      </h2>
      <p>
	Before Firefox 57, there were some addons that would reduce the size of your tabs (e.g. FaviconizeTab). If you want to replicate this, add this at the bottom of <kbd>userChrome.css</kbd>:
      </p>
      <textarea title="Faviconize Tabs" class="code">
.tabbrowser-tab:not([pinned])
{
   max-width: 40px !important;
   min-width: 40px !important;
   
}
      </textarea>
      <h2>
	Hiding the close button on non-pinned tabs
      </h2>
      <p>
	With all the different ways to close tabs, from keyboard shortcuts to mouse shortcuts, who needs the close button to appear on the tab?
      </p>
      <textarea title="Remove Close Button" class="code">
.tabbrowser-tab .tab-close-button
{
   display:none!important;
}
      </textarea>
      <h2>
	Hide back and forward buttons
      </h2>
      <p>
	If you have other ways of navigating the web and don't need to see the back and forward buttons (that is, they're just taking up valuable screen real estate), you can hide them like this:
      </p>
      <textarea title="Hide Back and Forward Buttons" class="code">
#back-button
{
   display: none;
}

#forward-button
{
   display: none;
}
      </textarea>
      <h2>
	Disabling Pocket completely
      </h2>
      <p>
	If you prefer to not use Pocket at all and wipe all traces of it from your browser, simply go to <kbd>about:config</kbd> and set <kbd>extensions.pocket.enabled</kbd> to <kbd>false</kbd>.
      </p>
      <h2>
	Privacy on Firefox
      </h2>
      <p>
	This part and the next one are more about protecting your privacy. I use the following set of extensions to help protect my privacy on the web.
      </p>
      <ul class="a">
	<li>
	  <span>
	    <a href="https://github.com/Cookie-AutoDelete/Cookie-AutoDelete" target="_blank">
	      Cookie AutoDelete
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://decentraleyes.org" target="_blank">
	      Decentraleyes
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://disconnect.me" target="_blank">
	      Disconnect
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/mozilla/multi-account-containers" target="_blank">
	      Firefox Multi-Account Containers
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://addons.mozilla.org/en-US/firefox/addon/firewall-2017/" target="_blank">
	      firewall
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://www.eff.org/https-everywhere" target="_blank">
	      HTTPS Everywhere
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/Mirclus/idn_warner" target="_blank">
	      IDN-Warner
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/cowlicks/privacypossum" target="_blank">
	      Privacy Possum
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/tumpio/requestcontrol" target="_blank">
	      Request Control
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/sblask/webextension-skip-redirect" target="_blank">
	      Skip Redirect
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/stoically/temporary-containers" target="_blank">
	      Temporary Containers
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/gorhill/uBlock" target="_blank">
	      uBlock Origin
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://github.com/gorhill/uMatrix" target="_blank">
	      uMatrix
	    </a>
	  </span>
	</li>
	<li>
	  <span>
	    <a href="https://violentmonkey.github.io/" target="_blank">
	      Violentmonkey
	    </a>
	  </span>
	</li>
      </ul>
      <h2>
	Ephemeral profiles
      </h2>
      <p>
	Having truly temporary profiles which only last as long as the session is a great way to preserve privacy. And, with a little bit of work, you can ensure things like settings get copied over. Here, I provide a Linux-specific script which sets up a temporary firefox profile and runs it with the sandboxing tool <a href="https://github.com/netblue30/firejail/" target="_blank">firejail</a>. You can find the script at my firejail-profiles <a href="https://github.com/chiraag-nataraj/firejail-profiles/blob/master/private-profile.sh" target="_blank">repo</a>. The way it works is as follows:
      </p>
      <ul class="a">
	<li>
	  <span>
	    The script takes one mandatory argument and four optional arguments. The mandatory argument is the path to a <kbd>.private</kbd> file. More information on this below. The optional arguments come <em>before</em> the mandatory argument and are as follows:
	  </span>
	  <ol class="a">
	    <li>
	      <span>
		<kbd>-p</kbd> is the path to an existing firefox profile, which will be used under certain circumstances.
	      </span>
	    </li>
	    <li>
	      <span>
		<kbd>-t</kbd> enables the creation of a temporary profile.
	      </span>
	    </li>
	    <li>
	      <span>
		<kbd>-c</kbd> tells the script to copy certain files from the existing profile to the temporary one.
	      </span>
	    </li>
	    <li>
	      <span>
		<kbd>-n</kbd> tells the script to create a network namespace based on the specified interface.
	      </span>
	    </li>
	  </ol>
	</li>
	<li>
	  <span>
	    The <kbd>.private</kbd> file is a series of variables which customize the script. There is an <a href="https://github.com/chiraag-nataraj/firejail-profiles/blob/master/private-profiles/firefox.private" target="_blank">example file</a> located in my firejail-profiles repo and there is more extensive discussion of what this kind of file entails in the repo's <a href="https://github.com/chiraag-nataraj/firejail-profiles/blob/master/README.md" target="_blank">README</a>.
	  </span>
	</li>
	<li>
	  <span>
	    If you just want to launch an existing firefox profile, simply provide the path to the profile with <kbd>-p</kbd>.
	  </span>
	</li>
	<li>
	  <span>
	    If you want to create a private profile from scratch (i.e. without copying files from an existing profile), simply pass <kbd>-t</kbd>.
	  </span>
	</li>
	<li>
	  <span>
	    If you want to create a private profile and copy over files from an existing profile, provide the path to that profile with <kbd>-p</kbd> and pass <kbd>-t</kbd> and <kbd>-c</kbd>.
	  </span>
	</li>
	<li>
	  <span>
	    If you want to disable the generation of a <code>private-lib</code> filter, simply set <var>$PRIVLIB</var> to 0 in firefox.private.
	  </span>
	</li>
	<li>
	  <span>
	    If you want to disable the use of systemd, simply set <var>$USE_SYSTEMD</var> to 0 in <code>firefox.private</code>.
	  </span>
	</li>
      </ul>
      <p>
	So there you have it! Creating ephemeral firefox profiles on linux is fairly easy.
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
