var MathJax = {
    TeX: {
	equationNumbers: { autoNumber: "AMS" },
        extensions: ["autoload-all.js"]
    }
};
