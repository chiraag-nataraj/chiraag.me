<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Introduction to LaTeX
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src="latex/mathjax-config.js">
    </script>
    <script type="text/javascript" src="../mathjax/MathJax.js?config=TeX-AMS-MML_SVG">
    </script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
	An Introduction to LaTeX
      </h1>
      <h2>
        What is LaTeX?
      </h2>
      <p>
        LaTeX is a free, open source, document preparation system.  It lets users concentrate on the content and the logical structure of a document rather than the nitty-gritty details of every item.
      </p>
      <h2>
        How do I get it?
      </h2>
      <h3>
        Windows
      </h3>
      <p>
        <a href="http://miktex.org/" target="_blank" type="text/html">
          MikTeX
        </a>
        is a free option for Windows users.  If you would like, you can also try using TeX Live (mentioned below), but I have personally found MikTeX to be easier on Windows.
      </p>
      <h3>
        Mac OSX/Linux
      </h3>
      <p>
        <a href="http://tug.org/texlive/" target="_blank" type="text/html">
          TeX Live
        </a>
        is the de-facto standard for Linux, Mac OSX, and other Unix-type systems.  For Mac, download TeX Live from the TeX Live link.  For Linux, an easier method may be searching your repositories for texlive related packages.
      </p>
      <h2>
        How do I start using it?
      </h2>
      <h3>
        Basics
      </h3>
      <p>
        LaTeX is a text-driven post-processor.  What does that mouthful mean?  It means you must give it a plain-text file with little commands which it then interprets.  It figures out all of the things you told it to do, does it, and creates an output file - either a DVI file or a PDF file.  LaTeX commands are always started with a backslash ("\").  Arguments to those commands are given with curly braces ("{"/"}") or square brackets ("["/"]").
      </p>
      <h3>
        The Editor
      </h3>
      You can technically use any plain text editor to write a LaTeX document.  However, certain LaTeX-specific editors make it really easy to compile the LaTeX document and even help you with writing it.  I recommend
      <a href="http://www.winedt.com/" target="_blank" type="text/html">
        WinEdt
      </a>
      on Windows, TeXShop (included with TeX Live download) on Mac OSX, and gedit/kedit/emacs on Linux.
      <h3>
        Your first LaTeX document
      </h3>
      <p>
        Open up the recommended editor for your operating system.  Copy and paste the following stuff into the empty text file:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="basic">Basic example</label>
            <textarea id="basic" class="code" rows="4" cols="26" readonly="readonly">
\documentclass{article}
\begin{document} 
My first \LaTeX document!
\end{document}
            </textarea>
          </td>
          <td class="code">
            My first \( \LaTeX \) document!
          </td>
        </tr>
      </table>
      <p>
        Save this as test.tex.  Then, find the button that says something like LaTeX in your editor.  You may have to change the compiler from TeX to LaTeX in some editors.  The editor may or may not automatically display the generated document.  If it doesn't, try looking for a button called Preview or something similar.  Now let me explain each part of the sample document.
        <kbd>
          \documentclass{article}
        </kbd>
        means that LaTeX will use the standard class called article.  There are also classes called book, report, letter, etc., but we will concern ourselves only with article, as it is the one most people use.
        <kbd>
          \begin{document}
        </kbd>
        and
        <kbd>
          \end{document}
        </kbd>
        must surround everything that you want typeset.  This is because there are special things that can go in the
        <strong>
          preamble
        </strong>
        , the part before the
        <kbd>
          \begin{document}
        </kbd>
        .
        <kbd>
          \LaTeX
        </kbd>
        is a command that tells LaTeX to write the logo of LaTeX.  That's it!  Every document must have a
        <kbd>
          \documentclass
        </kbd>
        statement and a
        <kbd>
          \begin{document}
        </kbd>
        and
        <kbd>
          \end{document}
        </kbd>
        .
      </p>
      <h3>
        Terminology
      </h3>
      <ul class="a">
        <li>
          <span>
            Environment - something which is invoked using a
            <kbd>
              \begin{}
            </kbd>
            and
            <kbd>
              \end{}
            </kbd>
            construct.  Examples are document, equation, letter, etc.
          </span>
        </li>   
        <li>
          <span>
            Package - an extension which lets the user perform functions which he/she would otherwise be unable to do using LaTeX
          </span>
        </li>
      </ul>
      <h2>
        Tips and Tricks
      </h2>
      Here, I will provide snippets of LaTeX documents which illustrate the usage of particular environments.  They should be put in the
      <strong>
        body
      </strong>
      of a LaTeX document, which means they should go between the
      <kbd>
        \begin{document}
      </kbd>
      and the
      <kbd>
        \end{document}
      </kbd>
      .
      <h3>
        Equations
      </h3>
      <p>
        Part of the reason LaTeX is awesome is that it is extremely awesome at typesetting mathematical expressions.  I highly recommend you use the
        <kbd>
          amsmath
        </kbd>
        package.
        The
        <kbd>
          amsmath
        </kbd>
        package is one such package which adds a couple of new environments which are better than the stock LaTeX ones.  Use the
        <kbd>
          equation
        </kbd>
        environment to typeset equations.  Here's an example:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="eqx1">Equation example 1</label>
            <textarea id="eqx1" class="code" rows="3" cols="20" readonly="readonly">
\begin{equation} 
  y = x^2 
\end{equation}
            </textarea>
          </td>
          <td class="code">
            \begin{equation}
            y = x^2
            \end{equation}
          </td>
        </tr>
      </table>
      <p>
        Obviously, there are more complicated examples, like this one:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="eqx2">Equation example 2</label>
            <textarea id="eqx2" class="code" rows="3" cols="20" readonly="readonly">
\begin{equation}
  e^{\pi i} + 1 = 0
\end{equation}
            </textarea>
          </td>
          <td class="code">
            \begin{equation}
            e^{\pi i} + 1 = 0
            \end{equation}
          </td>
        </tr>
      </table>
      <p>
        You can get Greek letters by prefixing the name of the letter with a backslash.  For example, &pi; is
        <kbd>
          \pi
        </kbd>
        .  To get uppercase letters, simply capitalize the first letter of the name.  For example, &Pi; is
        <kbd>
          \Pi
        </kbd>
        .
      </p>
      <h3>
        Equation arrays
      </h3>
      <p>
        Now the
        <kbd>
          equation
        </kbd>
        environment is fine for individual equations scattered throughout a document, but what if you want a series of equations displayed nicely in a little array?  That's what the
        <kbd>
          align
        </kbd>
        environment is for.  Now some people may tell you to use the
        <kbd>
          eqnarray
        </kbd>
        environment, but
        <kbd>
          eqnarray
        </kbd>
        is obsolete and
        <kbd>
          align
        </kbd>
        is much better.  Here's a simple example proving the quadratic formula:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="eqnarr">Equation array example</label>
            <textarea id="eqnarr" class="code" cols="75" rows="9" readonly="readonly">
\begin{align} 
  ax^2 + bx + c &amp;= 0\\
  x^2 + \frac{b}{a}x + \frac{c}{a} &amp;= 0\\ 
  x^2 + \frac{b}{a}x &amp;= -\frac{c}{a}\\ 
  x^2 + \frac{b}{a}x + \frac{b^2}{4a^2} &amp;= -\frac{c}{a} + \frac{b^2}{4a^2}\\ 
  \left(x + \frac{b}{2a}\right)^2 &amp;= \frac{b^2 - 4ac}{4a^2}\\ 
  x + \frac{b}{2a} &amp;= \frac{\sqrt{b^2 - 4ac}}{2a}\\ 
  x &amp;= \frac{-b + \sqrt{b^2 - 4ac}}{2a} 
\end{align}
            </textarea>
          </td>
          <td class="code">
           \begin{align} 
           ax^2 + bx + c &amp;= 0\\
           x^2 + \frac{b}{a}x + \frac{c}{a} &amp;= 0\\ 
           x^2 + \frac{b}{a}x &amp;= -\frac{c}{a}\\ 
           x^2 + \frac{b}{a}x + \frac{b^2}{4a^2} &amp;= -\frac{c}{a} + \frac{b^2}{4a^2}\\ 
           \left(x + \frac{b}{2a}\right)^2 &amp;= \frac{b^2 - 4ac}{4a^2}\\ 
           x + \frac{b}{2a} &amp;= \frac{\sqrt{b^2 - 4ac}}{2a}\\ 
           x &amp;= \frac{-b + \sqrt{b^2 - 4ac}}{2a} 
           \end{align}
          </td>
        </tr>
      </table>
      <p>
        I've used a couple of new items here.  The
        <kbd>
          &amp;
        </kbd>
        is called an
        <strong>
          alignment operator
        </strong>
        .  It tells LaTeX where to align the equations.  We will also use this in tables (coming up next).  The
        <kbd>
          \frac
        </kbd>
        command tells LaTeX to format the two following arguments (enclosed in
        <kbd>
          {
        </kbd>
        <kbd>
          }
        </kbd>
        ) as a fraction.  The
        <kbd>
          \sqrt
        </kbd>
        command tells LaTeX that the argument should be placed inside a square root symbol.  The
        <kbd>
          ^
        </kbd>
        symbol tells LaTeX to put the argument in a superscript.  Similarly, a
        <kbd>
          _
        </kbd>
        can be used to tell LaTeX to put the argument in a subscript.  Quick note: If the argument for a superscript or subscript is more than one character, be sure to surround it with
        <kbd>
          {}
        </kbd>
        .  If you are not sure whether to use the curly braces or not, use them - they can't hurt!
      </p>
      <h3>
        Tables
      </h3>
      <p>
        Tables are one of the only annoying things to learn in LaTeX.  They can be confusing at first, but with practice, you can make professional tables.  Here's a simple example:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="tbl">Table example</label>
            <textarea id="tbl" class="code" rows="4" cols="20" readonly="readonly">
\begin{tabular}{l l} 
  1&amp;2 \\ 
  3&amp;4 \\
\end{tabular}
            </textarea>
          </td>
          <td class="code">
            \begin{array}{l l} 
            1&amp;2 \\ 
            3&amp;4 \\
            \end{array}
          </td>
        </tr>
      </table>
      <p>
        There are a couple of things to explain here.  First, the
        <kbd>
          tabular
        </kbd>
        environment takes one argument - a space-delimited list of columns and alignments.  For example, I chose
        <strong>l</strong>eft and
        <strong>l</strong>eft for my two columns.  You could also choose
        <strong>c</strong>enter or
        <strong>r</strong>ight in terms of basic formatting (I will explain more options in the next section).  The
        <kbd>
          &amp;
        </kbd>
        is used in this case to separate columns.  The
        <kbd>
          \\
        </kbd> is used to separate rows.  But what if you want borders?  Here's an example with borders:
      </p>
      <table class="latex">
        <tr>
          <th>
            Source
          </th>
          <th>
            Result
          </th>
        </tr>
        <tr>
          <td class="code">
	    <label for="tblbrdr">Table with borders example</label>
            <textarea id="tblbrdr" class="code" rows="7" cols="25" readonly="readonly">
\begin{tabular}{|l|l|} 
  \hline 
  1&amp;2\\
  \hline 
  3&amp;4\\
  \hline 
\end{tabular}
            </textarea>
          </td>
          <td class="code">
            \begin{array}{|l|l|} 
            \hline 
            1&amp;2\\
            \hline 
            3&amp;4\\
            \hline 
            \end{array}
          </td>
        </tr>
      </table>
      <p>
        The command
        <kbd>
          \hline
        </kbd>
        simply produces a horizontal line, which we then use to our own purposes.  Also notice that I added
        <kbd>
          |
        </kbd>
        between the columns to generate the column dividers.  You can similarly add double borders by adding
        <kbd>
          ||
        </kbd>
        instead of
        <kbd>
          |
        </kbd>
        or by adding two
        <kbd>
          \hline
        </kbd>
        commands in a row.  And now we come to the issue of text wrapping in tables.
      </p>
      <h3>
        Advanced Tables
      </h3>
      <p>
        Now text wrapping requires a whole different column alignment specifier.  Here's an example:
      </p>
      <label for="wrp">Wrapping example</label>
      <textarea id="wrp" class="code" cols="80" rows="8" readonly="readonly">
\begin{tabular}{|p{5cm}|p{5cm}|} 
  \hline 
  longlonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglong&amp;longlonglonglonglonglonglonglonglonglonglonglonglonglong\\
  \hline 
  longlonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglong&amp;longlonglonglonglonglonglonglonglonglonglonglonglonglong\\
  \hline
\end{tabular}
      </textarea>
      <p>
        The
        <kbd>
          p{5cm}
        </kbd>
        tells LaTeX that you want the column to be 5 centimeters in width.  This is a good time to talk about units of measurement in LaTeX.  LaTeX can use pt, mm, cm, in, ex, and em, among others.  pt is about 1/72.27 inches and is the standard unit of measurement.  ex is about the height of a lowercase
        <kbd>
          x
        </kbd>
        in the current font, and em is about the width of an uppercase
        <kbd>
          M
        </kbd>
        in the current font.  I only ever use cm or in, but it's up to you.  If you want even more units of measurement, head over to the
        <a href="http://en.wikibooks.org/wiki/LaTeX/Useful_Measurement_Macros" target="_blank" type="text/html">
          Useful Measurement Macros
        </a>
        page on the LaTeX wiki.  The other column specifiers are
        <kbd>
          m{
          <em>
            width
          </em>
          }
        </kbd>
        which aligns the text vertically in the middle, and
        <kbd>
          b{
          <em>
            width
          </em>
          }
        </kbd>
        , which aligns text at the bottom of the row.
      </p>
      <p>
        However, let's now try to do multi-page tables.  You'll find that it doesn't work very well - the table will run off the page and won't continue onto the next one.  The package
        <kbd>
          longtable
        </kbd>
        will come to the rescue here.  Here's a simple table running onto three pages.
      </p>
      <label for="lngtbl">Longtable example</label>
      <textarea id="lngtbl" class="code" readonly="readonly" cols="25" rows="20">
\begin{longtable}{|p{5cm}|p{5cm}|} 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\ 
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\ 
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\
  a &amp; b\\ 
\end{longtable}
      </textarea>
      <p>
        Again, remember to type
        <kbd>
          \usepackage{longtable}
        </kbd>
        in your preamble.  Otherwise, this will not work!  There are many other things you can do with tables, like having one column span multiple rows and having one row span multiple columns, but they are outside the scope of this document.
      </p>
      <h3>
        Lists (numbered and unordered)
      </h3>
      <p>
        Lists are quite easy to make in LaTeX.  Use the
        <kbd>
          itemize
        </kbd>
        environment for unordered lists and
        <kbd>
          enumerate
        </kbd>
        for ordered lists.  Here is an example of each.
      </p>
      <h4>
        Unordered List
      </h4>
      <label for="ulx">Unordered List Example</label>
      <textarea id="ulx" class="code" readonly="readonly" cols="80" rows="10">
\begin{itemize} 
  \item A 
  \item B 
  \item C 
  \item You can have lists inside other lists: 
  \begin{itemize} 
    \item The bullet changes and indents 
  \end{itemize} 
  \item Back to the original list 
\end{itemize}
      </textarea>           
      <h4>
        Ordered List
      </h4>
      <label for="olx">Ordered List Example</label>
      <textarea id="olx" class="code" readonly="readonly" cols="80" rows="10">
\begin{enumerate} 
  \item A 
  \item B 
  \item C
  \item You can have lists inside other lists: 
  \begin{enumerate} 
    \item The numbering style changes and indents 
  \end{enumerate} 
  \item Back to the original list 
\end{enumerate}
      </textarea>
      <p>
        You can have up to four levels of bullets/enumerations.
      </p>
      <h3>
        Other languages
      </h3>
      <p>
        Let's say you are doing your Spanish homework with LaTeX.  There's a problem.  How are you going to get the accents and everything?  There's an easy solution (given below).
      </p>
      <label for="lngx">Spanish Example</label>
      <textarea id="lngx" class="code" cols="80" rows="3" readonly="readonly">
Mi nombre es Chiraag.  ?` Qu\'e es tu nombre?  Mi casa est\'a en Armenia (no en realidad).  Mi mam\'a ense\~na una clase en la universidad de Rochester (no en realidad).
      </textarea>
      <p>
        As you can see, upside down question marks are made by appending a backtick (`) to the regular question mark.  The tilde on top of the n is made by putting a backslash before the tilde.  Accents are made by preponing an apostrophe with a backslash, followed by the letter which you want the accent to be on.  Again, you can find more information about which accents are supported by default in LaTeX
        <a href="http://en.wikibooks.org/wiki/LaTeX/Accents" target="_blank" type="text/html">
          here.
        </a>
      </p>
      <h3>
	Images
      </h3>
      <p>
        Now let's say you have this really nice report with tables, lists, and all, but you now want to add images.  How would you do that?  Well, it's actually quite easy.  First, you will need to use the
        <kbd>
          graphicx
        </kbd>
        package to enable the following command.  Next, download a random image (or find one on your computer) to use for the demo.  Then, copy this code.
        <strong>
          Be sure to replace the image_name in the code with the name of your image
        </strong>
        .  Also, the LaTeX file will need to be in the same directory as the image.
      </p>
      <label for="imgx">Image Example</label>
      <textarea id="imgx" class="code" cols="46" rows="1" readonly="readonly">
\includegraphics[width=\textwidth]{image_name}
      </textarea>
      <p>
        A couple of notes.  First of all, if you are creating a
        <strong>
          DVI
        </strong>
        file, you can
        <strong>
          only use eps files
        </strong>
        .  If you are creating a
        <strong>
          PDF
        </strong>
        file, you can
        <strong>
          only use formats other than eps files
        </strong>
        .  Please keep this in mind when creating your LaTeX files.  Second of all, notice that you don't need to include the extension of the image file - LaTeX will automatically detect which one you want to use.  The easiest way to make the LaTeX file compilable with both
        <kbd>
          latex
        </kbd>
        and
        <kbd>
          pdflatex
        </kbd>
        is to have both an eps version and a jpg version of your image.  Then, LaTeX will automatically use the eps file and PDFLaTeX will automatically use the jpg file.  There are many more options, such as rotation and scaling.  Attributes that you can set include:
      </p>
      <ul class="a">
        <li>
          <span>
            height=
            <em>
              height
            </em>
          </span>
        </li>
        <li>
          <span>
            width=
            <em>
              width
            </em>
          </span>
        </li>
        <li>
          <span>
            angle=
            <em>
              angle to rotate (counterclockwise is positive)
            </em>
          </span>
        </li>
        <li>
          <span>
            viewport=
            <em>top_left top_right bottom_left bottom_right
            </em>
          </span>
        </li>
        <li class="nosymbol">
          <ul>
            <li>
              <span>
                Using the
                <kbd>
                  viewport
                </kbd>
                option with the regular
                <kbd>
                  \includegraphics
                </kbd>
                command will
                <em>
                  move
                </em>
                the picture
              </span>
            </li>
            <li>
              <span>
                Using the
                <kbd>
                  viewport
                </kbd>
                option with the
                <kbd>
                  \includegraphics*
                </kbd>
                command will
                <em>
                  crop
                </em>
                the picture
              </span>
            </li>
          </ul>
        </li>
      </ul>
      <h3>
        Advanced LaTeX usage - typesetting Carnatic (South Indian classical) music with LaTeX (in Kannada)
      </h3>
      <p>
        (Note: I am working on rewriting this section, since KannadaLaTeX seems to be dead and there are better options now, including XeLaTeX.) First, you will need to download <a href="http://sarovar.org/projects/kannadatex/" target="_blank" type="text/html">KannadaLaTeX.</a> If you would like to typeset the music in other languages, you will have to search for the appropriate LaTeX packages. Next, use this template to get you started (this template will only work for KanndaLaTeX - you will need to modify it to suit your needs if you are doing this in another language):
      </p>
      <label for="carnatic">Advanced Example</label>
      <textarea id="carnatic" class="code" cols="80" rows="30" readonly="readonly">
\documentclass[12pt]{article}
\def\kannada{\bgroup\fontfamily{lel}\selectfont}
\def\endkannada{\egroup}
\long\def\kansl#1{\bgroup\fontfamily{lel}\fontshape{sl}\selectfont #1\egroup}
\long\def\kanbsl#1{\bgroup\fontfamily{lel}\fontseries{bx}\fontshape{sl}\selectfont #1\egroup}
\newcommand{\uo}[1]{$\overset{.}{\textrm{\begin{kannada} #1 \end{kannada}}}$}
\newcommand{\lo}[1]{\d{{#1}}}
\usepackage{kanlel}
\usepackage{amsmath}
\begin{document}
\noindent Name of Kriti/Varna/etc.

\noindent Name of composer

\noindent rAga: Name of raaga

\noindent tALa: Name of taala
\section{palalxvi}
\begin{center}
  \begin{tabular}{*}
    %%Pallavi goes here
  \end{tabular}
\end{center}
\section{anupalalxvi}
\begin{center}
  \begin{tabular}{*}
    %%Anupallavi goes here
  \end{tabular}
\end{center}
\section{mukAtxyi savxra}
\begin{center}
  \begin{tabular}{*}
    %%Muktai swara goes here
  \end{tabular}
\end{center}
\section{caraNa}
\begin{center}
  \begin{tabular}{*}
    %%Charana goes here
  \end{tabular}
\end{center}
\end{document}
      </textarea>
      <p>
        Replace "*" with a number of columns equaling the number of beats in the taala + the number of vertical bars that are usually drawn for that taala.  For example, the sequence for Vilamba Adi taala would be "c c c c c c c c c c c c c c c c c c c c" - one "c" for every beat (16), one "c" for every spacer (to separate the notation into 4 groups of 4) (there are 4).  Basically, the sequence is "beat beat beat beat beat beat beat beat line beat beat beat beat line beat beat beat beat".  This template is set up for a varna, but you can adapt it to any purpose - kriti, thillana, etc.  When you are filling in each column, you can just type two ampersands in a row to create an empty column (it will act as a separator).  For reference, I have uploaded the source for the Abhogi varna "Evari bodha" and the rendered result.
      </p>
      <p>
        <a href="latex/abhogi_varna.tex" type="text/x-tex">
          Source
        </a>
      </p>
      <p>
        <a href="latex/abhogi_varna.pdf" type="application/pdf">
          Result
        </a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
