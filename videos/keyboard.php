<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Keyboard Performances
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src='videos/eventlisteners.js'></script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Keyboard Performances
      </h1>
      <p>
        These are the links to some of my keyboard performances. You can select an individual piece by clicking in the left hand column. If you have trouble playing these, you can also find them in YouTube by searching for "Chiraag Nataraj."  Thanks for visiting!
      </p>
      <?php
      $nameurls = array(
	"Rajarathnam's maDikEri maMju (ಮಡಿಕೇರಿ ಮಂಜು) (AKKA 2010)" => "https://www.youtube-nocookie.com/embed/FAUh3R5IznA",
	"tananam tananam (ತನನಂ ತನನಂ) with Chiraag and Rochitha (2008)" => "https://www.youtube-nocookie.com/embed/rSslay7Pi3Q",
	"Hindi song chookar mere man ko (छूकर मेरे मन को) (2005)" => "https://www.youtube-nocookie.com/embed/jcw1DpjDW_o",
	"Kannada song bAre bAre (ಬಾರೆ ಬಾರೆ) (2005)" => "https://www.youtube-nocookie.com/embed/J0nDAwBSj-Y",
	"Kannada song dONi sAgali (ದೋಣಿ ಸಾಗಲಿ) (2005)" => "https://www.youtube-nocookie.com/embed/ebXhMIYs87U",
	"Hindi song na ye chand hoga (ना ये चाँद होगा) (2005)" => "https://www.youtube-nocookie.com/embed/WsFG4tGaLDw",
	"Hindi song meri bhIgi bhIgi si (मेरी भीगी भीगी सी) (2005)" => "https://www.youtube-nocookie.com/embed/AkzK2VllMAk"
      );
      print '<ul>';
      $k = 0;
      foreach($nameurls as $name => $url)
      {
	print '<li>';
	print '<label for="vid'.$k.'" class="vidlist" data-url="'.$url.'">';
	print $name;
	print '</label>';
	print '<input type="checkbox" id="vid'.$k.'" />';
	print '<div class="videoPlayback" id="videoPlayback'.$k.'">';
	print '<noscript>';
	print '<iframe class="ytembed" src="'.$url.'?rel=0" allowfullscreen></iframe>';
	print '</noscript>';
	print '</div>';
	print '</li>';
	$k += 1;
      }
      print '</ul>';
      ?>
      <p>
        <a href="videos/index.php" type="text/html">
          Back
        </a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
