<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Personal Feedback</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>

  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>This may seem a little odd...</h1>
      <p>
	You probably clicked here out of sheer curiosity, wondering what the feedback is for. No, this isn't for the site. This is feedback about <em>me</em>. Anonymous feedback. No accounts, no email addresses. Just feedback about me. It can be positive. It can be negative. Ideally if it's negative, it will be constructive criticism, but really, anything goes.
      </p>
      <p>
	I know it's sometimes hard to directly talk with people and give them feedback to their faces, especially negative feedback or criticism. I've experienced this personally, having been on the receiving end of some of the feedback quite recently. I'm aiming to make it easier and more comfortable for people to give me feedback.
      </p>
      <form method="post" action="/feedback.php">
	<label for="feedback"><b>Feedback</b></label>
	<textarea id="feedback" name="feedback" rows="20" style="width: 100%;" required="required"></textarea>
	<input type="submit" value="Submit Feedback">
      </form>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
