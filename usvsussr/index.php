<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>US versus USSR</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
    <?php
    function myParse($arr)
    {
      return(explode(" | ", $arr));
    }
    function findIndex($arr, $size, $id)
    {
      for($j = 0; $j < $size; $j++)
      {
        if(in_array($id, $arr[$j]))
        {
          return($j);
        }
      }
    }
    $refs = file("references");
    $refs_split = array_map("myParse", $refs);
    $size = count($refs);
    $refcounts = array();
    foreach($refs_split as $r)
    {
      $refcounts[$r[0]] = 0;
    }
    function cite($id)
    {
      global $refs_split;
      global $size;
      global $refcounts;
      $counter = findIndex($refs_split, $size, $id) + 1;
      print '<sup class="cite" id="ref'.$id.'n'.$refcounts[$id].'"><a href="usvsussr/index.php#'.$id.'">'.$counter.'</a></sup>';
      $refcounts[$id] += 1;
    }
    ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        US vs. USSR
      </h1>
      <p>
        Propaganda.  Repression.  Fear.  These are the words that people think of when one mentions the USSR.  But were we truly so different? Both nations engaged in heavy propaganda &mdash; the only difference was who the enemy was.  Both nations engaged in paternalistic imperialism.  However, our responses as a nation to similar actions on both sides were extremely different.  When the USSR did something horrifying, we condemned them as "godless savages" and said that they were terrible beings.  When we did something almost as horrifying (and sometimes <em>more</em> horrifying), we wrote it off as "national security", "for the common good", "necessary evil", and so on.  In other words, when they did something bad, they were <em>bad people</em>, whereas when we did something bad, we were <em>good people</em> doing <em>bad things</em>.  This kind of rhetoric has enormous implications for international relations and how we as a nation perceive the world.
      </p>
      <p>
        For example, we always condemn the USSR for creating the <em>gulags</em> and condemning people to essentially a lifetime of slavery and eventually, death of a most horrible kind &mdash; usually from starvation.  And yes, they were horrible.  However, is our current "criminal justice" system so different?  People can be held in prison indefinitely without a trial (see Guantanamo Bay) and in horrific conditions.  Torture is apparently completely acceptable (we still haven't held anyone accountable for the Bush-era torture programs) for obtaining answers.
      </p>
      <p>
        Furthermore, while we espouse the idea of free ideas and condemn Russia (and earlier, the USSR) for jailing people who disagree with the government, are we truly any better?  When an illegal program being run by the government is disclosed to the people who are affected by it, what does our wonderful government do?  It attempts to lock the whistleblower up, possibly for life.  Take, for example, the cases of Chelsea Manning and Edward Snowden.  Chelsea Manning is currently serving 35 years in prison<?php cite("tate2013")?>.  Edward Snowden had to flee to Russia to avoid the federal government.  And what were their terrible crimes?  They spoke out against the injustices that were going on.  If they had just stated what was going on, no one would have believed them.  Hence, they did what they felt was right and released documents <em>showing</em> the terrible abuses of power.  In the case of Edward Snowden, what he uncovered may, in fact, be illegal under the Constitution.  They are public menaces because they believe that what they are being forced to do is wrong?  That sounds incredibly Orwellian.
      </p>
      <p>
        Then take the DEA &mdash; the Drug Enforcement Administration.  The current drug laws are not enforced evenly across the board.  Poor people are much more likely to be arrested for possession. And among poor people, minorities, especially African Americans, are at especially high risk of being arrested, <em>even though all evidence points to fairly similar levels of drug use across racial lines</em><?php cite("mccabe2007") ?>.  And how are these prisons maintained?  They're dank, dirty, dingy, falling apart, prisoners crowded far past the point of decency.  California, for example, has 143,643 prisoners in a system designed to hold 84,130.  Many prisons are holding more than 200% of their designed capacity<?php cite("prisons2014") ?>.  This is inhumane, yet we are fine with it.  One does not see people condemning the drug laws that led to the drastic increase in prisoners (and thus to the overcrowding we have today) to the same degree they condemn e.g. the ban on homosexuality in Russia.  Sure, you hear about it every now and then, on the far fringes of society, but it's not a priority issue.  And one certainly does not hear people condemning the US as a whole as a result of this terrible policy which has led to the absurd problem that we have.  People condemn our <em>actions</em>, but never extend that to saying that the US is full of <em>bad people</em>.
      </p>
      <p>
        The <em>gulag</em> system implemented in the Soviet Union was terrible in that it effectively condemned people to a slow, drawn-out, painful death.  But how is our current prison system any better?  Not only does it effectively do the same, it can in fact be worse at times.  Rape and sexual assault are rampant problems, as are drug use (yes, <em>drug use</em> &mdash; oh, the irony!), theft, (regular) assault, threats of suicide, depression, mania, and so on.  The other difference is how people get into the system.  To get into the <em>gulag</em> system, one had to criticize Comrade Stalin or his ideas.  To get into our prison system?  Smoke some pot.  Neither makes sense, but at least the critic has some potential of stirring up trouble.  What is the pot smoker going to do, convert everyone into a bunch of potheads?
      </p>
      <p>
        Then, we have evidence for the imperialistic nature of both nations.  Obviously, the USSR was extremely imperialist, accumulating territories left and right.  However, the US isn't much better.  Take, for instance, Iran.  In 1952, the US government decided that the democratically elected government of Iran at the time was not friendly to the US.  It wasn't friendly to the USSR either, but that apparently was besides the point.  We launched a covert CIA operation to overthrow the government of Iran and replace it with one much friendlier to US interests &mdash; the Shah<?php cite("precht1988") ?>.  This of course led (at least indirectly) to the Ayatollah overthrowing the Shah and forcibly taking control in the name of restoring the government to the people.
      </p>
      <p>
        Then, you have examples such as the Philippines and Cuba.  During the Spanish-American War, we helped the Philippines gain independence from Spain by helping Emilio Aguinaldo, a young revolutionary, fight against and defeat the Spanish.  However, after that war, we immediately denounced Emilio as a traitor, captured him, killed him, and took control of the Philippines<?php cite("philippines2014") ?>.  We retained control of the Philippines until the end of World War II, when we finally `granted' independence to the Philippines &mdash; as if independence is something that can be granted.
      </p>
      <p>
        During that same war, we helped Cuba escape from the clutches of the Spanish as well.  This time, we ruled over Cuba for only 5 years.  However, we granted them independence only after they agreed to the Platt amendment, which essentially allowed us to do whatever we wanted and/or "needed" to do to Cuba for the "maintenance of good governance".  We used this provision to quell rebellions against discrimination as well as to exploit Cuba to obtain all of its resources &mdash; our corporations owned 60% of the Cuban sugar industry and imported 95% of the total Cuban crop.  In addition, we intervened several times militarily because their government (<em>their</em> government) took actions <em>we</em> didn't like.  When one of their presidents, Grau, nullified the Platt Amendment, we denied to even recognize the existence of his government and labeled him "communistic" &mdash; because apparently wanting to use the money generated by processing the raw resources in the country to benefit the citizens of that country is communist<?php cite("cuba2014") ?>.
      </p>
      <p>
        Lest the reader get the idea that this is all over and done with and that we are not like that anymore, one would do well to look at recent history.  The Iraq War was about many things: (supposed) WMDs, a cruel dictator, oil, family rivalry.  There are, however, some signs pointing to oil being the primary factor.  In order to get the full picture, one must go back in time, to a time before Saddam Hussein was in power.  The US helped Saddam Hussein gain power and supported him in the Iraq-Iran war (not only that, we were aware that Saddam was using chemical weapons and <em>still</em> supported him).  However, there was one thing the US government did not anticipate &mdash; Saddam Hussein nationalized the oil industry in Iraq, which meant that most of the money from the oil was going to &hellip; Iraq!  There was no way we could allow that &mdash; our precious corporations which depend on having people and countries to prey on would die without fresh blood.  Hence, as soon as Saddam Hussein was killed, the US forced Iraq to privatize the oil industry and to let American and British companies in<?php cite("iraq2013") ?>.  Hence, the US and Britain are getting rich off of Iraqi oil money.  Let us also not forget that the US established the new government in Iraq after taking out the old one and held "elections".
      </p>
      <p>
        Then of course, we have many countries in Central and South America where we have supported dictators and military dictatorships simply because they were friendly towards us.  Notable examples include: Institutional Revolutionary Party (Mexico), Manuel Cabrera<?php cite("forster1994") ?>, Jorge Ubico<?php cite("forster1994") ?>, and the military junta<?php cite("mcallister2010") ?>(Guatemala), Fulgencio Batista<?php cite("chase2010") ?>(Cuba), Revolutionary Government Junta<?php cite("larsen2010") ?>(El Salvador), National Reorganization Process<?php cite("feitlowitz1998") ?>(Argentina), the Somoza family<?php cite("gould2010") ?>(Nicaragua), Fran&ccedil;ois and Jean-Claude Duvalier<?php cite("nicholls1990") ?>(Haiti), Omar Torrijos and Manuel Noriega<?php cite("koster1990") ?>(Panama), and Augusto Pinochet<?php cite("winn2010") ?>(Chile), just to name a few.  And this is just in Central and South America.  This doesn't even touch on the vast number of dictatorships we have helped ascend to power in other parts of the world, such as the Middle East, Africa, and the Far East.
      </p>
      <p>
        At least the USSR was clear in its intentions.  The US does the exact same things, yet professes to believe in exactly the opposite things.  It professes to believe in equality, yet promoted <em>de jure</em> racism.  It professes to believe in privacy, yet has set up the largest spying network on the planet (FBI + CIA + NSA + partnerships with similar organizations around the world).  It insists that it believes in democracy, yet sponsors dozens upon dozens of dictators and military juntas around the globe. The USSR performed many of the same actions, but never sought to present an image of benevolence.  It never pretended to believe in democracy.  It never attempted to convince the world that it believed in privacy.
      </p>
      <p>
        The real difference between the US and the USSR was in how they went about doing the horrible things they did.  The US was very covert &mdash; launching CIA operations, sending in a hit squad, actions of that nature.  On the other hand, the USSR was extremely overt &mdash; if they wanted something, they just went and took it, not caring what the rest of the world thought.  The actual actions were not that different &mdash; they had <em>gulags</em>, we have jails.  They had a dictator, we supported and brought to power dictators all around the world.  They jail people who speak out against them, we would do the same if we could get our hands on the person.  They jail homosexuals, we jail [African-American] drug users (disproportionately I might add).  They invade countries without cause, we do the same (well&hellip; we make up causes).
      </p>
      <p>
        If we want to be taken seriously as a country, we must &mdash; we <em>must</em> &mdash; walk the walk before we talk the talk.  Otherwise, we have no credibility in the international community.  We can't profess to believe in principles when we fight to destroy those very principles in other countries.  How can we decry those exact same (or similar) actions performed by other countries?  The cognitive dissonance is just too astounding to even comprehend.  In order to have a cohesive foreign policy that is logical and consistent with our beliefs as a nation, it is imperative that we reexamine many of our fundamental guiding principles when it comes to international aid and conflict.
      </p>
      <h1>
        References
      </h1>
      <?php
      global $refs_split;
      global $size;
      global $refcounts;
      
      print '<ol class="citations">'.PHP_EOL;
      for ($i = 0; $i < $size; ++$i)
      {
        $id = $refs_split[$i][0];
        $cit = $refs_split[$i][1];
	$nrefs = $refcounts[$id];
	print '<li id="'.$id.'">';
	for($j=0; $j < $nrefs; $j++)
	{
          print '<a href="usvsussr/index.php#ref'.$id.'n'.$j.'">^</a>';
	}
	print $cit.'</li>'.PHP_EOL;
      }
      print '</ol>'.PHP_EOL;
      ?>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
