<!DOCTYPE html>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Chiraag Nataraj&rsquo;s (ಚಿರಾಗ್ ನಟರಾಜಿನ) Web Page
    </title>
    <meta charset="utf-8" />
    <link href="main.css" type="text/css" rel="stylesheet" />
    <?php include('base.php') ?>
  </head>  
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <div id="container">
        <div id="cn_photo">
          <img src="chiraag_photo.jpg" alt="Photo of Chiraag" class="cn_photo" />
        </div>
        <div id="home_main_content">
          <h1>
            Welcome to my website!
          </h1>
          You have reached the homepage of Chiraag Nataraj.  I am a postdoctoral researcher at
          <a href="https://sandia.gov" target="_blank" type="text/html">
            Sandia National Labs.
          </a>
        </div>
      </div>
      <div id="#spacer">
        <hr />
      </div>
      <h2>
        Research
      </h2>
      <p>
        Further details can be found on my <a href="research/index.php">research overview page.</a>
      </p>
      <h3>
        A first-principles investigation of refractory alloy systems united by a common computational framework
      </h3>
      <p>
        My PhD thesis at <a href="https://brown.edu" target="_blank">Brown Unviersity</a> with adviser Professor Axel van de Walle. The cluster expansion formalism for alloys is used to investigate phenomena in several refractory alloys: Co<sub>3</sub>W (superalloy), NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr (high entropy alloys). Density functional theory (DFT) is used to calculate the energies of many small structures which are used to construct a cluster expansion model for each alloy system. These models are then used as Hamiltonians for Monte Carlo simulations in order to explore the dependence of the antiphase boundary (APB) energy on temperature and Al and Hf impurity concentration in metastable FCC Co<sub>3</sub>W as well as phase segregation behavior in NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr. <a href="https://repository.library.brown.edu/studio/item/bdr:ay8pgsa7/" target="_blank">My full PhD dissertation can be found at the official Brown Digital Repository.</a>
      </p>
      <h3>
        A systematic analysis of phase stability in refractory high entropy alloys utilizing linear and non-linear cluster expansion models
      </h3>
      <p>
        Research conducted at <a href="https://llnl.gov" target="_blank">Lawrence Livermore National Lab</a> during an internship. The phase segregation behavior of three key refractory high entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr) is studied using first-principles calculations. <a href="https://doi.org/10.1016/j.actamat.2021.117269" target="_blank">This work was published in Acta Materialia.</a>
      </p>
      <h3>
        First-principles study of the effect of Al and Hf impurities on Co<sub>3</sub>W antiphase boundary energies
      </h3>
      <p>
        Research conducted at <a href="https://brown.edu" target="_blank">Brown University</a> during the course of my PhD. The effects of Al and Hf impurities on the (111) antiphase boundary (APB) energy of metastable FCC Co<sub>3</sub>W are investigated via ab initio calculations. <a href="https://doi.org/10.1016/j.actamat.2021.117075" target="_blank">This work was published in Acta Materialia.</a>
      </p>
      <h3>
        Temperature-dependent configurational entropy calculations for refractory high-entropy alloys
      </h3>
      <p>
        Research conducted at <a href="https://llnl.gov" target="_blank">Lawrence Livermore National Lab</a> during an internship. The cluster expansion formalism for alloys is used to construct surrogate models for three refractory high-entropy alloys (NbTiVZr, HfNbTaTiZr, and AlHfNbTaTiZr). These cluster expansion models are then used along with Monte Carlo methods and thermodynamic integration to calculate the configurational entropy of these refractory high-entropy alloys as a function of temperature. <a href="https://doi.org/10.1007/s11669-021-00879-9" target="_blank">This work was published in a special edition of the Journal of Phase Equilibria and Diffusion.</a>
      </p>
      <h3>
        Peeling studies as a proxy for understanding fracture mechanics and other interesting phenomena
      </h3>
      <p>
        My undergraduate thesis at <a href="http://caltech.edu" target="_blank">Caltech</a> with adviser Professor Kaushik Bhattacharya from 2014 to 2015. Peeling experiments and computer simulations are used to investigate fracture and crack propagation in heterogeneous materials. <a href="research/undergrad-thesis.pdf" target="_blank" type="applications/pdf">The full thesis can be found here.</a>
      </p>
      <h3>
        Monitoring Patient Motion Using Kinect Sensors to Optimize Spinal Cord Injury Therapy
      </h3>
      <p>
        A project I carried out at <a href="http://caltech.edu" target="_blank">Caltech</a> in the Burdick Lab during the summer of 2014. The key contribution of this research is in the automation of the initialization phase of the SCAPE algorithm to enable the utilization of depth sensor technology to track body motion. As part of the research team, I focused on implementing the new algorithm for the initialization, while a graduate student worked on implementing the SCAPE algorithm and calibrating the sensors. <a href="research/kinect-report.pdf" target="_blank" type="applications/pdf">The full report can be found here.</a>
      </p>
      <h3>
        Hydraulics for Micro-Robotic Actuation
      </h3>
      <p>
        A project I carried out at the Army Research Lab during the summer of 2013. Parameterized CAD drawings of the different parts of a pump for a robotic arm actuator are utilized in a programmatic manner to facilitate optimizing parameters. These CAD models are then utilized to 3D print a functioning pump in one piece. <a href="research/hydraulics-report.pdf" target="_blank" type="applications/pdf">The full report can be found here.</a>
      </p>
      <h3>
        Modeling of NIMs for &#8220;Hide-In-Plain-Sight&#8221;
      </h3>
      <p>
        A project I carried out at the Army Research Lab during the summer of 2012. The possibility of creating &#8220;hide-in-plain-sight&#8221; effects using negative index metamaterials (NIMs) was explored with the goal of concealing or disguising an object. POV-Ray, a raytracing software, was used to model NIMs in various configurations, geometries, and gradients of index of refraction to study the effect on an observer's perception of an object enclosed in metamaterial. <a href="research/nims-report.pdf" target="_blank" type="applications/pdf">The full report can be found here.</a>
      </p>
      <h3>
        Chaotic Dynamics
      </h3>
      <p>
        Interpretation of nonlinear dynamic chaos through music, a project I carried out at <a href="http://www.navsea.navy.mil/nswc/carderock/default.aspx" target="_blank" type="text/html">Naval Surface Warfare Center,</a> Philadelphia, Summer 2010.
      </p>
      <ul class="a">
        <li>
          <span>
            Here is a
            <a href="research/chaos-report.pdf" target="_blank" type="application/pdf">
              report
            </a>
            on the project; if you use it, please refer to it as
            <em>
              Chiraag Nataraj, &#8220;Understanding Complexity in Nonlinear Dynamic Systems Using Musical Compositions,&#8221; Technical Report, Naval Surface Warfare Center, Philadelphia, PA, September 1, 2010.
            </em>
            An email to me would also be appreciated.
          </span>
        </li>
        <li>
          <span>
            Some of my music compositions arising out of chaotic oscillations!
          </span>
          <ul>
            <li>
              <a href="chaos/chiraag_chaos_henon.mp3" target="_blank" type="audio/mpeg">
                The Henon map
              </a>
            </li>
            <li>
              <a href="chaos/chiraag_chaos_logistic.mp3" target="_blank" type="audio/mpeg">
                The Logistic map
              </a>
            </li>
            <li>
              <a href="chaos/chiraag_chuaMusikChaos_500_chord_instr.mp3"  target="_blank" type="audio/mpeg">
                Chua circuit
              </a>
            </li>
            <li>
              <a href="chaos/chiraag_chaos_LorenzMusikChaos_438_chord_instr.mp3"  target="_blank" type="audio/mpeg">
                Lorenz attractor
              </a>
            </li>
          </ul>
        </li>
      </ul>
      <h3>
        Swarm Robotics
      </h3>
      <p>
        A project I carried out at Autonomous Systems Laboratory,
        <a href="http://www.villanova.edu/engineering/research/centers/cendac/" target="_blank" type="text/html">
          Villanova University
        </a>
        during the summers of 2008 and 2009.  I worked with a research group; my responsibility was to get a group of NXT robots working in a swarm in autonomous fashion.  I did it with a version of Java.  A report that was eventually published as a paper at the 2010 Annual American Society of Engineering Education Conference is
        <a href="research/swarm-report.pdf" target="_blank" type="application/pdf">
          here.
        </a>
        A video of a successful run can be found at various resolutions:
      </p>
      <ul class="a">
	<li>
	  <a href="swarm/swarm-256x144.mp4" target="_blank" type="video/mp4">256x144</a>
	</li>
	<li>
	  <a href="swarm/swarm-426x240.mp4" target="_blank" type="video/mp4">426x240</a>
	</li>
	<li>
	  <a href="swarm/swarm-640x360.mp4" target="_blank" type="video/mp4">640x360</a>
	</li>
	<li>
	  <a href="swarm/swarm-854x480.mp4" target="_blank" type="video/mp4">854x480</a>
	</li>
	<li>
	  <a href="swarm/swarm-1280x720.mp4" target="_blank" type="video/mp4">1280x720</a>
	</li>
	<li>
	  <a href="swarm/swarm-1920x1080.mp4" target="_blank" type="video/mp4">1920x1080</a>
	</li>
      </ul>
      <h2>
        Music
      </h2>
      <h3>
        Tabla
      </h3>
      <p>
        I have been learning tabla since I was 7 years old, and my current guru and mentor is the renowned artist <a href="http://www.tabla.org" target="_blank" type="text/html">Pandit Samir Chatterjee</a>. I had a tabla concert in October 2008 with <a href="https://musicalbeats.net" target="_blank" type="text/html">Ustad Shafaatullah Khan</a> on the sitar (<a href="kimberton_concert_excerpts.mp3" target="_blank" type="audio/mpeg">you can listen to a 10 minute clip here</a>), but my major solo concert was my ರಂಗಪ್ರವೇಶ (raMgapravEsha) in August 2011, and the <a href="videos/rangapravesha.php">full video is available here.</a>
      </p>
      <h3>
        Other
      </h3>
      <p>
        I have also had Carnatic vocal training (my most recent guru was Ganakalashree M. S. Sheela) and Western classical piano lessons (at the Bryn Mawr Conservatory of Music) for several years. I love to sing in both Kannada (my mother tongue) and Hindi. Many of these performances are available on my <a href="videos/vocal.php">vocal performances page.</a>
      </p>
      <hr />
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
