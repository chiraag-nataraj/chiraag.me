<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Rants</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Rants
      </h1>
      <p>
	A collection of mini-rants which weren't deserving of their own pages.
      </p>
      <h2>
	Venezuela (March 15, 2019)
      </h2>
      <p>
	I implore my friends, especially my liberal ones, to stop and consider the possible implications a US-backed coup in Venezuela. <b>Yes</b>, Maduro is awful, No doubt about that. But a US-backed coup would most likely lead to a civil war, massive civilian casualties, and possibly a right-wing authoritarian government (e.g. a military dictatorship) &mdash; the US is great at getting those kinds of governments installed. Just. Look. At. History. Were y'all the same people who wanted to invade Iraq on a humanitarian basis? Serious question.
      </p>
      <p>
	It's been terrifying seeing so many of my friends latch onto imperialist rhetoric, and I understand the impulse to want to do <em>something</em>. As Cody says, there are non-military options that include brokering diplomatic negotiations, <em>actual</em> humanitarian aid (rather than political destabilization masquerading as humanitarian aid), and possibly getting Maduro to agree to new elections held by the UN.
      </p>
      <p>
	Recognizing Guaido isn't helping and only further legitimizes someone <b>who was never elected to lead the country</b> (even in a sham election).
      </p>
      <p>
	Let's also take a moment to recognize that this isn't about Maduro being authoritarian. We're okay with authoritarians when they're on "our side" (Saudis, Saddam Hussein, Gaddafi for a while, Fateh al Sisi, and, of course, countless examples from history, even just of the 20th century). This is about the fact that Venezuela has a ton of oil, and American companies would like to be able to go in there and extract it as they did before nationalization (rather than having to buy from PDVSA). You're naive if you don't think this has anything to do with the way Venezuela is singled out for "humanitarian" intervention.
      </p>
      <h2>
	Why I'm voting #UnionYES! (November 13, 2018)
      </h2>
      <p>
	As you might have noticed from my recent posts, we are coming upon an election! I know I've spoken with many of y'all personally over the past 2 years(!), but I want to take some time here to discuss my personal reasons for voting yes.
      </p>
      <p>
	Crucially, my personal experience here at Brown has been pretty great &mdash; my adviser is pretty awesome, my classes have been taught well, I don't have a requirement to TA (but can if I want to), my stipend isn't too bad, and so on. So this begs the question: why exactly do I want a union? And this is something I've thought about over the past couple of years, and the reasons have slightly evolved over time.
      </p>
      <p>
	Note: Ideologically, I am fairly left-wing (at least in the context of American politics), so I'm already predisposed to supporting unions in that regard. While that certainly helps, it has mainly served as extra inspiration during certain rough patches.
      </p>
      <p>
	So here are some of the reasons I will be voting yes:
      </p>
      <ul>
	<li>
	  A formal contract: One of the first things I thought of when I heard of the union is that a formal contract can help alleviate uncertainties. While there may be certain benefits today (say, healthcare or dental care or a specific stipend), there is no guarantee that those benefits will continue existing in the future. There is currently no official contract that guarantees us certain things, whether it be a certain level of stipend, an insurance plan that covers certain things, a certain ceiling on TA hours, or anything else. This means that the university could unilaterally take away those benefits without asking us or caring what we think (and indeed, they have unilaterally changed insurance plans and removed certain programs in the past). Given that we are the ones affected, that seems a bit unfair. Fundamentally, a formal contract would help alleviate uncertainty surrounding some of these benefits and expectations and help grad students understand what is expected and required of them.
	</li>
	<li>
	  Democracy at work: Many of us tend to think of democracy as a way to ensure we are represented in our government and a way to affect change without a violent revolution. Fundamentally, a democratic system of government ensures that our voices are heard. But why should this be restricted to our system of government? What is it about government that fundamentally makes it different from the administration of a university or the board of a company? Those bodies are also making decisions that affect other people's lives, sometimes in enormous ways. Those bodies aren't making laws, but they currently function as benevolent dictatorships &mdash; entities we trust to make decisions about our lives. What happens when the bodies making the decisions aren't affected by those decisions? It seems reasonable to suppose that the decisions these bodies make would change if they were affected by their own decisions or if the body itself were augmented with people who will be affected. If democracy is good for our government, it is good for our workplace.
	</li>
	<li>
	  Accountability: As you all may have guessed from literally <em>any</em> of my posts, I'm not entirely a fan of the Trump administration &mdash; one thing it has exposed for me is the sheer lack of accountability in our government. And as I realized this, I started thinking about similar structures and power dynamics at play within other institutions, particularly at universities such as Brown. It can be hard to hold people in power accountable, particularly when they have a lot of control over your life (as university administrators/deans/etc can have). Individually, we are likely to be ignored or retaliated against if we bring forward complaints about our advisers or other professors &mdash; the system is designed to protect those in power and shield them from criticism. The only way, as I understand it, to introduce true accountability into the system is through a union, an organization which can channel criticisms into a force that they cannot simply ignore.
	</li>
      </ul>
      <p>
	There are many other reasons which end up somewhat falling under one or more of these three umbrella reasons. Childcare, for example, used to exist here at Brown but was replaced with an (inadequate) subsidy system and so would fit under all three of these reasons &mdash; a formal contract guaranteeing a certain level of childcare support would require members to authorize changes to it, democracy at work would mean that we (who are the ones actually affected by this change) would have had a say in the process, and real accountability would effectively have helped ensure that they couldn't make the change without at least trying to justify it in a meaningful way (which would potentially open up a dialogue for how to achieve what they wanted without hurting grad students in the process).
      </p>
      <p>
	Fundamentally, as far as I can tell, there are no downsides to us having a voice in the decisions that affect us. And that is really what our union is about &mdash; giving grad student workers a voice in the decisions that affect us.
      </p>
      <h2>
	Family separations (July 2, 2018)
      </h2>
      <p>
	Let's talk family separation for a second. Yes, it's very clear that what's happening now is unprecedented. Yes, it's true that Trump created the policy (not the Democrats). Yes, it's true that we should be agitating to end it once and for all (somehow I don't trust that the executive order actually did anything).
      </p>
      <p>
	But let's not kid ourselves. We were doing inhumane, awful things to undocumented immigrants at the border well before Trump. And the kinds of stuff Trump says could very easily have been said by a Democrat. Hell, they were. Trump (or rather, Sessions) said that the purpose of separating families was to <a href="https://www.washingtonpost.com/news/the-fix/wp/2018/06/20/the-trump-administration-changed-its-story-on-family-separation-no-fewer-than-14-times-before-ending-the-policy/?noredirect=on">send a message</a>. When asked what the policy should be regarding children seeking asylum from Central American countries, Clinton is clearly on the record stating that we need to send them back to <a href="https://www.huffingtonpost.com/entry/hillary-clinton-child-migrants_us_55d4a5c5e4b055a6dab24c2f">send a message to their parents</a>. Obama, too, has defended his actions regarding family detention, saying that we need to <a href="https://www.aclu.org/blog/smart-justice/mass-incarceration/president-obama-wants-continue-imprisoning-immigrant-families">send the parents a message</a>. I could go back further, but hopefully this is sufficient to show that this kind of dubious rationale is not only not new, but also bipartisan.
      </p>
      <p>
	As for the detention itself, the ACLU link above shows that Obama was detaining whole families, treating people fleeing crisis as a national security threat. Is this really so different from Trump's assertion that Mexicans are rapists, drug dealers, and murderers? One is more obviously racist, for sure. But I would argue that both paint our neighbors to the south as "alien", "foreign", "dangerous". And it would be ludicrous to not believe that this stems from a racial sentiment &mdash; I don't think we'd be talking about "invading hordes of Europeans" were there to be a sudden influx of immigrants from Europe. We'd think of it as natural, good. But these guys from south of the border? Oh yeah, they're <em>obviously</em> a security threat. It's sick.
      </p>
      <p>
	But of course, just "Vote Blue" and that will fix everything, right? Everything was great under Obama &mdash; families were detained together! That's so much better. My biggest fear is that when there's a Democrat in office, the people who supposedly care now will stop caring. Because everything's become about Trump the person. This policy is bad because <b>Trump</b> did it. But when Obama was doing something only slightly better, nobody cared. I don't remember Rachel Maddow openly weeping over this. I don't remember hours and hours of debates over how we could do something this cruel all over MSNBC and CNN. These people don't care. It's all a sham. Either they didn't know about these policies (hard to believe, since laypeople and activists certainly knew about them) or they did and didn't care. They're either shitty journalists or callous people who pretend to care when it suits the partisan narrative.
      </p>
      <p>
	So please, don't stop caring once Trump is out of office. The inhumane treatment of undocumented immigrants (including those seeking refugee status and asylum) started a long time ago and the rationales used to justify the treatment then are disturbingly similar to the one being used now.
      </p>
      <p>
	If you got here and would like to help, you can donate to <a href="https://refugeerights.org/donate/">IRAP</a> and/or the <a href="https://firrp.org/donate/">Florence Immigrant and Refugee Rights Project</a> by following the links.
      </p>
      <h2>
	Reducing plastic use (March 3, 2018)
      </h2>
      <p>
	Spurred by Frances' recent post, I wanted to document a couple of ways here that I've been trying to reduce my plastic usage as well as ask for more ideas.
      </p>
      <ol>
	<li>
	  I switched to using a <a href="https://www.brushwithbamboo.com/shop/bamboostraws/">bamboo straw</a>. The hardest thing is remembering to carry it with me. This is easy at the moment &mdash; since I usually have my backpack with me, I just keep it in there.
	</li>
	<li>
	  I have reusable shopping bags which I use whenever I go grocery shopping.
	</li>
	<li>
	  For vegetables and fruits, I use <a href="https://www.amazon.com/Earthwise-Reusable-Mesh-Produce-Bags/dp/B005E2QRPG/">mesh bags</a> and always prefer loose produce to packaged stuff. This has allowed me to eliminate plastic from my vegetable consumption and drastically reduce the plastic from my fruit consumption (I can't do much about berries).
	</li>
	<li>
	  For non-perishable goods, I've started buying in bulk, since that reduces the overall amount of plastic I use (volume versus surface area scaling) &mdash; I've found that <a href="https://nuts.com">Nuts.com</a> and <a href="https://spicesinc.com">Spices Inc</a> are pretty great for this.
	</li>
	<li>
	  I've switched from liquid soap (for showers) to bar soap, which eliminates the plastic involved there. I'm debating switching to bar soap for hand soap as well in the future, once my current supply of liquid soap is finished. I can use the same soap as shampoo, but I haven't found any good hair conditioner bars.
	</li>
	<li>
	  For tea, I usually buy loose-leaf or make sure the bags are made with natural fiber (the packaging for the tea itself is often cardboard, so that's usually not a concern).
	</li>
	<li>
	  I'm debating switching to lotion bars as well, but that would only be after my current supply is finished (and that will take a while...) &mdash; again, any recommendations?
	</li>
	<li>
	  My toothbrush has a replaceable head, so I don't ever need to throw out the entire brush &mdash; the only improvement would be such a system with brush heads made of natural fibers instead of plastic-derived materials...<a href="https://thegoodwellcompany.com/products/be">Be. Brush</a> comes to mind.
	</li>
	<li>
	  I'm planning on switching to a <a href="https://www.ifyoucare.com/household/compostable-trash-bags/">compostable trash bag</a> instead of the standard plastic ones. Does anyone have experience with these?
	</li>
	<li>
	  I've been trying to rely on containers made with PCR (post-consumer recycled) plastic as much as possible whenever plastic is the only option, but...that's not always possible :/
	</li>
	<li>
	  Instead of zip-locs, I've been using <a href="https://rezip.com/collections/food-on-the-go">(re)zip bags</a> which are meant to be reusable (so they're more durable and made from food-grade silicone).
	</li>
	<li>
	  I bought a <a href="https://stojo.co/collections/biggie">food-grade silicone collapsible cup</a> (I would recommend the one I linked to rather than the one I bought, which was only 12oz, too small to take to some places).
	</li>
	<li>
	  I have a glass water bottle I use pretty much everywhere (at this point, I just always have it in my backpack) &mdash; obviously metal or otherwise works here as well (especially if you keep dropping your water bottle).
	</li>
	<li>
	  I am now getting my bread from a local bakery &mdash; I bring my bulk foods bag from Whole Foods (it's a cloth bag with a drawstring tie) and have them put it in that.
	</li>
	<li>
	  When I get green chilies from my local Indian store, I bring my own bag and take it from the loose chilies he has (instead of the packaged ones).
	</li>
      </ol>
      <h2>
	Western countries and democracy (January 11, 2018)
      </h2>
      <p>
	Western countries only like democracies when they can chose the people running them. Western countries only like democracies when they allow companies from those countries to go in, exploit people, and plunder resources. Western countries only like democracies when they don't serve the people, but rather the elites and the corporations. Otherwise, those same countries who wax eloquently about the dangers of dictatorships and the need for democracy will launch an invasion on some pretext (real or made-up) and replace that democracy with a puppet state. Or, they ensure a stable state never forms in the first place. And, of course, this is tied in with capitalism and the ownership of our government by (big) corporations.
      </p>
      <p>
	Don't be distracted. What Donald Trump said today is not an anomaly &mdash; it is implicitly or explicitly believed by <b>at the very least</b> a large minority of the population. The reason these nations are getting screwed over is that companies and governments from all around the world are ensuring that either those governments are obsequious to other governments and corporations (and not actually working for their people) or that those governments don't exist in the first place. Fixing this requires deconstructing the paradigm that we have built.
      </p>
      <h2>
	War on Christians (December 4, 2017)
      </h2>
      <p>
	Now that the holiday season is officially upon us, I want to take some time to dispel a pervasive notion that has creeped into the public consciousness.
      </p>
      <p>
	There is no War on Christians. Christians are not being persecuted.
      </p>
      <p>
	Fact: The government, on all levels, is dominated by Christians.
      </p>
      <p>
	Fact: The government has celebrated Christmas, Easter, Lent, and so on. Every. Single. Year.
      </p>
      <p>
	Fact: Christians are the vast majority of the population. And have been. Since they killed off the Native Americans.
      </p>
      <p>
	If you are the vast majority, and have been every single year since this country's founding, you don't get to call yourself a victim. If you have dominated all levels of government since this country's founding, you don't get to call yourself a victim. If we're still debating birth control, abortion, and LGBTQ+ rights based on your religious beliefs, you don't get to call yourself a victim.
      </p>
      <p>
	Calling Christians victims and alleging that there is a War on Christians <b>here</b> is an insult to the places where Christians <b>do</b> actually face persecution. It is an insult to places where Christians are rounded up for the "crime" of being Christian.
      </p>
      <p>
	If Christians are suddenly rounded up and thrown in prison, or if they're forced to register into a Nazi-like database, or if they're singled out for special surveillance, or if they're forced to convert or die, <b>then</b> come and tell me there's a War on Christians and I will agree with you. But reforming our government to be more secular (and to ensure the separation of church and state) is not a War on Christians. It is just the fulfillment of the promise of our Constitution.
      </p>
      <h2>
	Military spending (September 24, 2017)
      </h2>
      <p>
	The most depressing news for me from the last couple of days was the <a href="https://www.cbsnews.com/news/senate-overwhelmingly-backs-bill-to-pump-700-billion-into-military/">$700 billion increase in military spending over the next 10 years</a>.
      </p>
      <p>
	If you take a look at the <a href="https://www.senate.gov/legislative/LIS/roll_call_lists/roll_call_vote_cfm.cfm?congress=115&session=1&vote=00199">vote distribution</a>, you'll see that the vast majority of both major parties voted for the bill. The same damn people who insisted (and sometimes <b>still</b> insist) that single-payer is too expensive, that tuition-free college is too expensive, that we can't afford to build our infrastructure &mdash; those same goddamn people had the nerve to vote for this bill. The same people who deride Sanders and other progressives as pie-in-the-sky idealists who don't know how to make actual policy vote for an increase in the military budget that would <a href="https://popularresistance.org/senate-military-spending-increase-alone-could-fund-free-college/"><b>more than cover</b></a> providing tuition-free college.
      </p>
      <p>
	And then there's the debate. Or rather, the lack thereof. There's no discussion of how we will pay for it &mdash; it's assumed that we'll "find a way". There's no discussion of whether it's needed, or whether there are better ways to spend the money. There's no discussion of what the military will spend it on.
      </p>
      <p>
	And there is very good reason to be suspicious of giving the military additional money &mdash; they have literally <a href="http://www.thefiscaltimes.com/2015/03/19/85-Trillion-Unaccounted-Should-Congress-Increase-Defense-Budget">"lost" $8.5 <b>trillion</b></a> (they don't know how the effing hell they spent it). And you're telling me that, given that, we should <b>increase</b> their damn budget? But again, there are absolutely zero questions. There aren't publicized debates or town hall discussions to see what the public thinks. There's no discussion of how this will be paid for. There's no discussion of whether this is the best goddamn use of our money.
      </p>
      <p>
	And the next time you want to praise the effing Democrats for being part of "The Resistance", just remember that they gave Trump's military $80 billion/year more to play with &mdash; the same military whose head (the President) they supposedly distrust and think is cuckoo, wacko, bonkers (oh wait, that must be my British English seeping in xD), etc. But apparently they trust him enough to let his military buy more weapons, rain death on more countries, and generally create more death, chaos, destruction, and terror in other countries.
      </p>
      <p>
	Remember that both the Democrats and Republicans voted for more terrorism. Republicans have the excuse that the President is "their" party. What the eff is the Democrats' excuse?
      </p>
      <h2>
	Hillary Clinton's interview (September 17, 2017)
      </h2>
      <p>
	So <a href="https://www.youtube.com/watch?v=tteLMfFDCb4">Hillary Clinton did an interview</a> recently.
      </p>
      <p>
	[1:57] "I had not drafted a concession speech! I had been working on a victory speech!"
      </p>
      <p>
	That...is kinda entitled. But, I suppose, if you believe in polling as your primary deity as she seems to (and, to be fair, as <em>many</em> do across the political spectrum), she had every reason to believe she would win.
      </p>
      <p>
	[2:15] "I just felt...this enormous let-down...this, kind of, loss of...feeling and direction and sadness."
      </p>
      <p>
	I understand this &mdash; especially if you've been campaigning for the last few months and thought you would win (again, based on polling).
      </p>
      <p>
	[3:56] "[Interviewer:] You specifically bought this house for a reason. [Clinton:] I did. [Interviewer:] And this was to be... [Clinton:] Well...I know a lot about what it takes to move a President and I thought I was going to win...[Narrator:] The Clintons had acquired the house next door to accomodate White House staff and security during a second Clinton administration."
      </p>
      <p>
	Okay, seriously? Look, I understand thinking you're going to win. But being <b>so</b> sure that you already start making preparations honestly smacks of entitlement. It smacks of smugness and this sense of "Of <b>course</b> I'm going to win!" which is honestly quite frustrating and off-putting. And for those who are angered by my reaction, do you really think we liberals and the media wouldn't lambast Trump if he had done the same thing? Of <b>course</b> we would.
      </p>
      <p>
	[5:40] "[Clinton:] He was quite successful in referencing a nostalgia that would give hope, comfort, settle grievances, for millions of people who were upset about gains that were made by others... [Interviewer:] What you're saying is millions of white people. [Clinton:] Millions of white people. Yeah. Millions of white people."
      </p>
      <p>
	Ah, fantastic. So I agree that <em>some</em> of this was due to Trump attracting that vote. But undeniably, part of it stemmed from his attacking trade deals, something Clinton couldn't or wouldn't do <a href="https://www.washingtonpost.com/news/the-fix/wp/2015/06/17/tracking-the-many-hillary-clinton-positions-on-trade/">consistently enough</a>. And that matters because many people <b>have</b> been hurt by free trade agreements, and Clinton did not offer a real solution for them. Neither did Trump, but he at least spoke to their hurt (as did Sanders). In fact, trade is one of the few things <a href="https://www.ontheissues.org/2016/Donald_Trump_Free_Trade.htm">Trump <b>has</b> been consistent on</a> (going all the way back to Reagan). I don't think protectionism of the sort Trump espouse(s/d) is the right answer, but certainly looking again at our free trade agreements is not something I'm against (I just have absolutely no confidence that Trump will actually make them better). I wonder if she addresses all of this later on in the interview.
      </p>
      <p>
	[6:18] "The forces that were at work, in 2016, were unlike anything I've ever seen or read about. It was a perfect storm!"
      </p>
      <p>
	Okay. Let's talk about Россия &mdash; I suppose we can't get away from it. At this point, it is undeniable that Trump's campaign <a href="https://www.businessinsider.com/donald-trump-jr-explains-why-he-met-with-russians-at-trump-tower-2017-9"><em>attempted</em> to collude with Russia</a>. But I think it is pretty amazing to see the "outrage" it's caused when <a href="https://www.huffingtonpost.com/entry/the-us-has-been-meddling-in-other-countries-elections-for-a-century-it-doesnt-feel-good_us_57983b85e4b02d5d5ed382bd">we have done the <b>exact same thing</b> in <em>many</em> countries (including Россия)</a>. So to pretend that this is somehow uniquely awful when <b>we have done the exact same shit</b> either shows a shockingly awful knowledge of history (in which case, read up!) or a deliberate attempt to ignore it. Also, as a side note, the <a href="https://www.politico.com/story/2017/01/ukraine-sabotage-trump-backfire-233446">Clinton campaign colluded with Ukrainian officials</a>.
      </p>
      <p>
	[6:38] "Oh I think the most important of the mistakes I made was using personal email."
      </p>
      <p>
	Absolutely no disagreement here, except for the assertion that it was one of her most important mistakes. But it certainly was a big mistake that was self-inflicted.
      </p>
      <p>
	[8:11] "Eleven days before the election. And it raised the spectre that somehow, the investigation was being reopened...it just stopped my momentum."
      </p>
      <p>
	Eh. I would disagree with this assessment. I think it certainly didn't help, but most of the people who already believed that she didn't do anything wrong continued to believe that. And people who believed she was a criminal continued to believe she was a criminal. I feel like trying to blame this for her loss is reaching a bit.
      </p>
      <p>
	[8:19] "At the same time he does that, about a closed investigation, there's an open investigation into the Trump campaign and their connections with Россия. You never hear a word about it. And when asked, later, he goes 'Well it was too close to the election'."
      </p>
      <p>
	For what it's worth, here is <a href="https://finance.yahoo.com/news/james-comey-explained-why-sent-162942157.html">Comey's sworn testimony</a> about the discrepancy. So Clinton seems to be misrepresenting his reasoning here.
      </p>
      <p>
	[8:58] "She writes 'his attacks caused lasting damage, making it harder to unify progressives in the general election and paving the way for Trump's "Crooked Hillary" campaign.'."
      </p>
      <p>
	Ugh. Okay, we have to talk about this, because apparently Clinton still wants us to. Let's look back to Clinton and her 2008 primary against Obama (which she ultimately lost).
      </p>
      <ul>
	<li>
	  Even when it was clear that Clinton would lose, she <a href="https://www.nbcnews.com/id/24953561/ns/politics-decision_08/t/clinton-refuses-concede-nomination/">refused to concede</a> in the hopes of gaining leverage against Obama when it came to policies
	</li>
	<li>
	  Clinton put <a href="https://www.nytimes.com/2008/06/05/us/politics/05superdelegates.html">intense pressure on superdelegates</a> to see if she could flip them through what was essentially a loyalty test
	</li>
	<li>
	  She literally fought tooth and nail using every trick in the book, once even <a href="https://www.motherjones.com/politics/2016/05/hillary-clinton-bernie-sanders-drop-out-election/">noting that Robert Kennedy was assassinated after winning the California primary</a> to justify staying in the race
	</li>
	<li>
	  She <a href="http://blogs.abcnews.com/politicalpunch/2008/05/clintons-contex.html">didn't buy the "party unity" argument</a> when she was the losing candidate
	</li>
      </ul>
      <p>
	Pretty much everything Clinton supporters threw at Sanders supporters (and sometimes at Sanders himself) was stuff Clinton said in 2008 when she was the losing candidate against Obama. So this notion that somehow, it was a 'more noble' fight when she was the losing candidate is complete and utter BS. It seems only the winner of the primary process cares about 'party unity'.
      </p>
      <p>
	As for Sanders setting up Clinton for Trump's "Crooked Hillary" comments: everything he said about her is true. She <b>did</b> take money from Wall Street. She <b>did</b> give speeches to Goldman Sachs. Note how no one really disputes that (because it's true). Many of us looked at that and said "Yeah, she's corrupt". And do people honestly think that Trump wouldn't have said that stuff if Bernie hadn't entered the race (the most extreme scenario)? Of <b>course</b> he would have. Because again, with respect to Sanders' critiques, everything was <b>true</b>. Saying he shouldn't have said those things is basically saying there shouldn't have been a real primary, that Sanders should have gone easy on Clinton, when that certainly wasn't true in 2008 when Clinton became the underdog. Again, this only seems to be a 'problem' when you're...the winning candidate of the primary?
      </p>
      <p>
	[9:22] "I understood that there were many Americans who, because of the financial crash, there was anger and there was resentment. I knew that. But I believed that it was my responsibility to try to offer answers to it not to fan it. I think, Jane, that it was a mistake because a lot of people didn't want to hear my plans. They wanted me to share their anger, and I should have done a better job of demonstrating: I get it."
      </p>
      <p>
	Sure, you shouldn't fan the anger. I get that. But the thing is that <b>she</b> didn't want to discuss her plans &mdash; the vast majority of her TV ads, for example, <a href="https://www.cnbc.com/2017/03/08/hillary-clintons-campaign-tv-ads-were-almost-entirely-policy-free.html">didn't discuss policy and went after Trump on moralistic grounds</a>. So to pretend that she wanted to discuss policies and that people didn't want to hear it is...disingenuous. At least as far as ads go, she certainly didn't seem to care about policy. Unfortunately, I can't find a similar analysis of her speeches versus Trump's speeches.
      </p>
      <p>
	[10:09] "[Clinton:] Well, I thought Trump was behaving in a deplorable manner. I thought a lot of his appeals to voters were deplorable. I thought his behavior, as we saw in the Access Hollywood tape, was deplorable. And there were a large number of people who didn't care &mdash; it did not matter to them. And he turned out to be a very effective reality TV star in our presidential campaign. [Interviewer:] You fed into that though. When you said 'basket of deplorables', you energized... [Clinton:] No, but they were already energized. [Interviewer:] But you offended some people who didn't personally feel deplorable at all. [Clinton:] I don't buy that. I don't buy that. I'm sorry I gave them a political gift of any kind, but I don't think it was determinitive."
      </p>
      <p>
	And here we have it again &mdash; Clinton goes after Trump based on his behavior. Doing that doesn't work &mdash; how has she not learned anything? Instead of saying "Well...maybe it was a bad idea to blanketly stereotype half of Trump's supporters" and thinking about how to reach out to the ones on the fence, she doubles-down on her earlier rhetoric. It's unhelpful rhetoric that stereotypes broad swathes of people while not gaining any new allies and alienates people on the fence (something she still refuses to admit).
      </p>
      <p>
	[11:24] "So in my debate prep, we practiced this &mdash; the young man playing Trump would stalk me. And I practiced keeping my composure. I practiced not getting rattled."
      </p>
      <p>
	Let's say we take this at face value, that being on the same stage as Trump after the Access Hollywood tape and having him be anywhere near you is awful.
      </p>
      <p>
	[11:52] "And so while I'm answering questions, my mind is going 'Okay, do I keep my composure, do I act like a President...'...[debate excerpt]...or do I wheel around and say 'Get out of my space! Back up you creep!' Well you know, I didn't do the latter, but I think in this time we're in, particularly in this campaign, maybe I missed a few chances."
      </p>
      <p>
	So I can't tell exactly, but it seems like she's blaming this in part for why she lost. If that is what she's doing, that's...weird. The thing is, if you take Trump as an expression of toxic masculinity and everything that comes with it (the entitlement over women's personal space, for example) &mdash; <b>that's ingrained in society</b>. Now of course I can't tell if she should have done that or not (and that's totally a personal decision), but I don't think it was that pivotal in the campaign. It certainly wasn't as pivotal as not campaigning significantly with labor unions and whatnot in the Rust Belt. It wasn't as pivotal as not running substantive ads. It wasn't as pivotal as taking the Rust Belt for granted.
      </p>
      <p>
	[12:25] "I am done with being a candidate, but I am not done with politics because I literally believe that our country's future is at stake."
      </p>
      <p>
	Yes, and you're standing in the way. You have shown that you <b>are</b> out of touch, that you <b>don't</b> get it. Progressives are already winning local seats around the country, <a href="https://www.thenation.com/article/a-progressive-electoral-wave-is-sweeping-the-country/">including in counties that went heavily for Trump</a>. So if you really want to defeat Trump, get out of the way and let the #Resistance succeed.
      </p>
      <h2>
	Terrorism (September 10, 2017)
      </h2>
      <p>
	I think one thing many people miss when it comes to "terrorism" is how the word is defined versus how it's used. Here's the dictionary definition:
      </p>
      <p>
	Terrorism: the systematic use of terror especially as a means of coercion (from Merriam-Webster)
      </p>
      <p>
	Terrorism: the use of violence or the threat of violence, especially against civilians, in the pursuit of political goals. (from The Free Dictionary)
      </p>
      <p>
	So this definition says nothing about <em>who</em> is doing the violence. But consider how it is (almost) always used: it is used to describe a violent act carried out by a non-state actor, usually a group of like-minded individuals with a specific political goal. But why is the use limited to non-state actors such as "terrorist groups" or individuals such as James Fields Jr? When we use drone strikes to remotely target people we call terrorists and end up killing civilians instead (<a href="https://www.ibtimes.com/nearly-90-those-killed-us-drones-were-not-intended-targets-during-five-month-span-2142183">90% of people killed by drone strikes were not intended targets during one 5 month span</a>, for example), why would that not count as terrorism? When we rain death and destruction almost continuously on 7 different countries simultaneously or take out heads of state, why is that not considered terrorism? In those cases, we are most certainly using violence (often against civilians in the case of the drone strikes) to achieve political ends (a change of government and the establishment of a more friendly regime, for example). So this fits the definitions given above, but "terrorism" is almost never used to describe it.
      </p>
      <p>
	This, of course, begs the question: Why would we not call this terrorism? Well, it's obvious. Calling what we terrorism would mean that <em>we</em> would be the #1 terrorist threat, not ISIS or Al Qaeda. Calling what we do terrorism would make the military contractors, the CIA, and the military itself complicit in that terrorism, which is unacceptable to those people. But fundamentally, calling what we do terrorism would imply that our actions aren't automatically legitimate, which is what <em>truly</em> frightens many people. The dogma of American Exceptionalism (and of the legitimacy of the state more generally) requires that everything the state does <em>must</em> be legitimate. And the world recognizes terrorism as illegitimate, which means calling what we do terrorism would mean our government (and countless others) is doing something illegitimate. But the fundamental fact remains that what we are doing <em>is</em> terrorism.
      </p>
      <h2>
	Self-segregation (August 29, 2017)
      </h2>
      <p>
	Y'all, let's talk about self-segregation for a second.
      </p>
      <p>
	First, what do I mean by "self-segregation"? I mean the tendency I've seen by some of my friends to unfriend those who harbor extreme views. Unfortunately, I've seen this often with my liberal friends unfriending or shunning Trump supporters. I'm not going to claim that it only happens on this side, though, because I don't have enough data on this stuff &mdash; I'll just talk about this phenomenon generally. I think that, on an aggregate level, this is misguided. Please hear me out before calling the mob on me 😉
      </p>
      <p>
	I think the people who do this have two main reasons for doing so. First, there is the mental health aspect &mdash; people don't constantly want to feel unsafe among their friend circle, so they kick out anyone who makes them feel unsafe. The second main reason, I think, is that people don't want these extremists to feel as though their views are valid &mdash; that is, this is meant to ostracize these people from society and make them feel as though they don't belong, that they don't have a place here.
      </p>
      <p>
	Before I discuss the first reason, though, we have to define what I mean by "unsafe". The way I'm defining it here is that feeling unsafe means your very identity or your worthiness as a person is question. For example, an African American or Jew probably feels unsafe around a neo-Nazi, regardless of whether that neo-Nazi is screaming at you at a protest or taking your order at a restaurant. In this way, kicking someone out of your friend circle because they make you feel unsafe is a form of self-defense, a way to keep your mental health from deteriorating. And honestly, this reason for unfriending someone is perfectly reasonable. I actually agree that if someone is constantly making you feel unsafe and isn't listening to what you're saying, you <b>should</b> cut them loose &mdash; they <b>aren't</b> your friend.
      </p>
      <p>
	However, while I think the second rationale is laudable, I think it will backfire. Here's why. Part of the reason white nationalists and neo-Nazis have seen a resurgence is because they've been cast as the "persecuted" ones. They can make the claim that everyone is out to get them. And while it is certainly true that they'll most likely have a similar claim no matter <b>what</b> we do, actually seeking to silence them and make sure they're not heard means people on the fence are easier to persuade. If you can point to actual instances of violence against you because of your views, it makes the rest of what you're saying seem more credible.
      </p>
      <p>
	There's another reason, too, to be skeptical that this will work. One of the ways people change their views is when they talk to people who disagree with them and they have a productive conversation. Is this always (or even mostly) going to be the case? Of course not. But that possibility certainly exists. By cutting out people who <b>are</b> extremists from your friend circle, you force them to self-segregate into an extremist group in which the echo chamber radicalizes people further. And if everyone who has ties to this person cuts them off, they have no moderating influences in their life and there are no restraints on how radical they become. So now, someone who may have been alt-right is now a full-blown neo-Nazi. That's not just unproductive, it's anti-productive.
      </p>
      <p>
	There's also a crucial distinction here between people of a certain movement and allies/advocates of that movement. For example, straight allies/advocates of the LGBTQ community or white allies/advocates of BLM. Allies and advocates are often in a position of privilege with respect to that specific movement, which means that when they speak out, people are more likely to listen to them. So while it can legitimately be dangerous/unsafe for people of a certain movement to keep ties to extremists, it is often far safer for allies and advocates of that movement to keep those ties. This is crucial because it is one way of retaining that moderating influence on these extremists while also ensuring your physical and mental safety is intact.
      </p>
      <p>
	So while it totally makes sense to unfriend someone for mental health reasons, I think we should retain those ties, <b>especially</b> if we claim to be allies/advocates of a movement. It's the only way, I think, to ensure that we don't create more extremists.
      </p>
      <h2>
	Slavery (August 23, 2017)
      </h2>
      <p>
	This notion that somehow only the South benefited from slavery is just bullshit. Their economy was more dependent on slavery, sure. But America as a whole benefited. It gave America the ability to build up an army, "expand" West (claiming more illegally stolen land for ourselves), "purchase" land from the French (as if it were the French's to sell in the first place), and so on. All that didn't only happen with the North's money. It took <em>all</em> of the money available in America. And for too long, America condoned the abhorrent institution of slavery to keep the economy going and make enough money to fuel the dreams of empire. So to pretend that everything we have &mdash; <b>everything</b> &mdash; isn't in some way connected to white supremacy and the institution of slavery is naïve at best and disingenuous at worst.
      </p>
      <p>
	White supremacy is the reason this country exists in the first place &mdash; Europeans who landed here thought they inherently had more claim to the land than Native Americans because of the color of their skin. White supremacy and slavery is the reason our economy grew at a rapid pace right after the country was created (stolen from Native Americans). White supremacy is the reason we (as a country) overlooked the institution of slavery (<b>surely</b> African Americans aren't <em>people</em>!) while it kept our economy growing. And all of our future successes &mdash; <b>all</b> of them &mdash; were derived from that first period of growing Westward (stealing more land from Native Americans because <b>duh</b> of course white people are superior to Native Americans) and a rapidly growing economy (thanks to slavery and the North's refusal to condemn it for a long time).
      </p>
      <p>
	We secured a place in the world's economy thanks to those things. We had more land to do with as we pleased thanks to those things. We were relatively isolated from hostile forces (including during WWI and WWII) thanks to those things. And everything &mdash; from the Industrial Revolution to our success after WWII &mdash; depends on those things being true. So to pretend that we'd still be here if white supremacy weren't a thing is the height of ignorance.
      </p>
      <p>
	This country was built on white supremacy, and far too many people still believe in it.
      </p>
      <h2>
	The Google memo (August 8, 2017)
      </h2>
      <p>
	Here is the <a href="https://gizmodo.com/exclusive-heres-the-full-10-page-anti-diversity-screed-1797564320">original memo</a>. I'm just going to try to comment on the whole thing. Bear with me here.
      </p>
      <p>
	Background: I actually agree with this &mdash; we <em>do</em> all have invisible biases, and we all <em>would</em> do well to examine ourselves to see what those biases are. Discussion with those who disagree with us (hello CCC!) can definitely help us figure out what those biases.
      </p>
      <p>
	Google's Biases: I don't know enough about Google to comment on this.
      </p>
      <p>
	Left Biases: Can confirm. Am a socialist 😉
      </p>
      <p>
	Right Biases: I presume these are decently correct.
      </p>
      <p>
	Rest of that paragraph: Again, I don't know enough about Google's culture specifically, so I can't really speak to that. I do know, though, that in many "left"-majority spaces, there isn't as much tolerance for dissenting views as I'd like. Notably, though, this applies to people on both sides of the nominal "left", which is interesting (see: basically any news show where the supposed "liberal" comes on the TV to berate Sanders supporters for not falling in line behind corporate Democrats). So while I agree it is a problem, I don't think it is only applied to people to the right of the nominal "left".
      </p>
      <p>
	Possible non-bias causes of the gender gap in tech: I'm skeptical of this, but I presume this person will back up these assertions (or at least explain which biological traits they're referring to) in the following paragraphs. One thing I'd like to point out is that all of these things seem testable, so it would be great if someone knows of any papers that back up this person's assertions. Also, "Many of these differences are small and there’s significant overlap between men and women, so you can’t say anything about an individual given these population level distributions.". If that is true, then we shouldn't see much of a discrepancy in the involvement of men and women in, say, tech (or any other industry).
      </p>
      <p>
	Personality differences: Okay, so these are bold claims without any papers or links to research to back them up. Also, they <em>do</em> come across as sexist, given that a standard trope has been "Women are highly emotional and men don't have any emotion at all" (for example).
      </p>
      <p>
	Men's higher drive for status: "Status is the primary metric that men are judged on, pushing many men into these higher paying, less satisfying jobs for the status that they entail." I actually agree with that, but I don't think it's biological as this person seems to imply. I do agree it is present in most cultures, but, for example, there were (are?) some matriarchal societies among Native Americans &mdash; did they also judge men and women based on similar metrics?
      </p>
      <p>
	Non-discriminatory ways to reduce the gender gap: Some of these things seem like valuable things to do anyway, but again, if the differences are small (as this person claimed before), then none of these should have a huge effect. For example, fostering cooperation is great, regardless of whether it gets more women into tech &mdash; it lowers stress and builds a sense of teamwork and cameraderie. The male gender role <b>is</b> currently inflexible, and we should <em>all</em> be working to dismantle that.
      </p>
      <p>
	The harm of Google's biases: If there are certain groups which are historically underrepresented in tech, then it makes sense to have programs specifically for them. That doesn't mean there shouldn't be other, more general programs, but I don't see the problem with having a set of classes designed specifically for people who are underrepresented. Regarding the reconsidering a group if it's not diverse enough...what does "too diverse" mean? That is, when <em>should</em> they reconsider? Regarding the OKRs, I'm not sure what this person is insinuating &mdash; maybe someone can provide more context/info? Regarding the Communism screed, I'll just say this: The current liberal ideology (most vociferously espoused by the Democrats) focuses on race and gender because they are bought by the same corporations that the GOP is bought by. They don't focus on class conflict precisely because that would require going against their donors and they don't want to do it. Now of course, there are some socialists (like myself) who see the world as an intersection of privilege of all types, from class privilege to racial privilege to gender privilege to abled privilege and so on and so forth. You might have certain privileges (I'm cis male, abled, and not too poor, for example) while lacking others (I'm brown-skinned, for example). So in this worldview, race and gender and all this other stuff plays a role, but it's no more or less important than class. Also, contrary to what some socialists believe, I don't think everything can be resolved by overthrowing capitalism. Doing so will certainly help, but it won't get us all the way there. Anyway, back to this memo.
      </p>
      <p>
	Why we're blind: I'm not sure why this person thinks liberals and progressives liked IQ tests, given that they were originally created to show that African Americans are inferior (as a race) to white Americans (this was initially used to deny them the ability to serve in the military). As for sex differences, I'd really need to see papers or other research showing what this person claims is true. I thought they might point to something in the memo, but that appears not to be the case. There are certainly some physical differences (due to hormonal differences), but the idea of "male" and "female" brains just isn't that accurate (https://www.newscientist.com/article/dn28584-a-welcome-blow-to-the-myth-of-distinct-male-and-female-brains/). As for having programs specifically for women, it makes sense if you accept the idea that, as a society, we have been discriminating against women for a long time. Just removing those barriers isn't enough, as things like wealth tend to accrue over time (which then changes the opportunities you have access to). As for society not giving a shit about men, <b>yes</b>. I agree with that. If a man needs support, he's usually fucked due to societal norms. I agree with that! But that doesn't mean we take away the programs we have in place to undo the decades and centuries of discrimination against women. Regarding PC culture, again, I agree that there is somewhat of a problem, but I can guarantee you that even this person (and probably most of you) would agree that there are some things that should probably have you ostracized. So it's not really that it's a thing &mdash; it's that you don't agree with the specific topics being placed off-limits.
      </p>
      <p>
	Suggestions:
      </p>
      <ul>
	<li>
	  De-moralize diversity: Eh. The problem is that equality of opportunity (the underlying issue for which diversity is being used as a short-hand metric) <b>is</b> a moral issue, and it should be one that most people agree on (even if we don't agree on how to get there).
	</li>
	<li>
	  Stop alienating conservatives: Sure. I do think that we need a more free atmosphere in terms of topics that are on-limits in most contexts.
	</li>
	<li>
	  Confront Google's biases: Sure? Again, I don't really know much about their culture, so I'd have to take this author at their word.
	</li>
	<li>
	  Stop restricting programs: Again, this totally depends on why those programs exist in the first place (most likely to correct for historical discrimination).
	</li>
	<li>
	  Costs and benefits of diversity: I don't think those things are equivalent, since one of those is a <b>good</b> for society (someone has a job, makes money, pays taxes, and generally is a productive member of society), while the others are unequivocally bad, and we should be trying to reduce them. But yes, in a way, we <em>should</em> try to equalize those metrics as well &mdash; by bringing down the proportion of men who fall into that stuff.
	</li>
	<li>
	  Psychological safety: YES.
	</li>
	<li>
	  De-emphasize empathy: Hell no. Empathy is what makes us human, and it's what actually forces us to care for others. De-emphasizing it will almost <em>certainly</em> make things worse.
	</li>
	<li>
	  Prioritize intention: The problem is that whether or not you intend something has no bearing on the outcome of that action. We <em>should</em> be more forgiving, but that doesn't mean we should stop trying to educate one another and try to avoid hurting other people (especially once you've been told that "x" bothers this person).
	</li>
	<li>
	  Human nature: I'd love to see some research on this. This person seems to appeal to 'science' without citing scientific articles or papers or anything. They just say "This is known to science" and don't bother to actually back up their claim.
	</li>
	<li>
	  Unconscious Bias training: I have absolutely no idea what that training says, so I can't comment on it.
	</li>
      </ul>
      <p>
	Welp. That went longer than I expected.
      </p>
      <h2>
	Ethical consumption (July 7, 2017)
      </h2>
      <p>
	I wanted to take some time to discuss something I've noticed recently.
      </p>
      <p>
	The first incident was a week or so ago when I was in a restaurant with my family and there happened to be M&Ms on the table. My sister asked if I wanted some and I declined, saying that I didn't feel good eating chocolate that had most likely been harvested with child labor (and possibly child slavery). My dad then told me to stop talking about it.
      </p>
      <p>
	The second incident happened yesterday when a couple of us went to the movies. Something very similar to the M&M incident happened, but regarding Godiva (which apparently might be a bit better than Mars at sourcing their chocolate?). And in this case, it was kind of seen as a joke or something that could be ignored.
      </p>
      <p>
	Now it's not that these people aren't liberals &mdash; my parents and sister are pretty liberal, as are my friends here at Brown. But a curious phenomenon happens when it comes to what we buy.
      </p>
      <p>
	We talk a big game about ensuring workers are treated fairly and ensuring the environment isn't destroyed, but when it comes to our own purchasing choices, we don't seem to care. Not only that, we balk when confronted with information which might possibly make that brand/product unpalatable. And often, the first reaction is to try to shut the other person down: "Stop talking about it", "You're a piece of crap", and so on.
      </p>
      <p>
	It's not for lack of information either &mdash; there are several apps that are convenient and easy to use, providing the information we ostensibly want at the tap of a button. Some examples are (Google Play Store links given):
      </p>
      <ul>
	<li><a href="https://play.google.com/store/apps/details?id=com.app.p5972GE">Better World Shopper</a></li>
	<li><a href="https://play.google.com/store/apps/details?id=com.buycott.android">Buycott</a></li>
	<li><a href="https://play.google.com/store/apps/details?id=au.org.goodonyou.goodonyou">Good On You</a></li>
      </ul>
      <p>
	Yet so many of us don't actually want to make those choices and consciously change our purchasing habits to reward companies that are sustainable, that treat their workers well, and so on. So many of us don't <b>want</b> that information because it shatters our image of that company.
      </p>
      <p>
	Here's the thing though: I don't think it's possible to care about people and the planet and <b>not</b> seek out this information. I feel like if you actually do care, you <b>should</b> seek out this information (again, available at the tap of a button). Like, if you don't care, that's another thing entirely &mdash; convincing you to buy sustainably if you don't care about the environment just doesn't make sense. But if you care about the environment &mdash; you donate to NWF and Sierra Club, recycle, and maybe even compost &mdash; why <b>wouldn't</b> you try to make better decisions as to the products you buy? Why <b>wouldn't</b> you boycott companies which are destroying rainforests or polluting the water? Why <b>wouldn't</b> you boycott companies which spew out huge amounts of CO2? Why <b>wouldn't</b> you support companies which source their materials sustainably? Why <b>wouldn't</b> you support companies which ensure they minimize water and air pollution? It just makes sense. And if you care about worker's rights, why <b>wouldn't</b> you boycott companies which employ slave labor? Why <b>wouldn't</b> you boycott companies which exploit the hell out of their workers? And why wouldn't you support companies which ensure their workers are treated well and aren't exposed to dangerous chemicals? Again, it just makes sense.
      </p>
      <p>
	I understand that in some cases, you don't have much of an option for various reasons. For example, I am currently insured by Independence Blue Cross, a company which gets an "X" from <a href="https://betterworldshopper.org">Better World Shopper</a> (they rank among the worst 20 companies in their database). But at this point in time, there isn't much I can do since I am under my parents' insurance. Even in the future, the insurance company that covers me will be decided by my employer, so I may still not have much of a choice. And maybe some of these products are very expensive. For example, sustainable textiles are generally pretty expensive (as I recently found out). But even there, you have gradations of companies &mdash; for example, some clothing companies are better than others at reasonable working conditions in their factories. If you end up buying from even a "C" or "B" company (on the Better World Shopper scale), that's still far better than a "D" or "F" (or "X") company.
      </p>
      <p>
	My point is that if you're a liberal who cares about this stuff, there's really no reason not to try to switch to better and more sustainable alternatives when possible. This also includes <a href="https://www.ran.org/banking_on_climate_change">banking</a> and <a href="https://www.fastcompany.com/40417097/how-to-invest-your-money-responsibly-sustainably-or-for-impact-theyre-not-the-same">investing</a>, including retirement accounts. Given that so many sustainable (and affordable!) alternatives exist, it seems somewhat hypocritical for someone who purportedly cares about these things to not make the switch.
      </p>
      <p>
	Thanks Joseph Fichera for getting me to think about this yesterday 😊
      </p>
      <h2>
	Philando Castille (June 23, 2017)
      </h2>
      <p>
	I think the lesson of the #PhilandoCastille murder and the subsequent verdict is that if you are a POC, you should literally never reach for anything, <b>even if you're asked to do so</b>. Simply existing and complying with the officer's order (to produce an ID) was enough to get him shot. Instead, indicate where the ID is and ask the officer to reach in and get it themselves.
      </p>
      <p>
	Seriously. I'm not even sure what else the options are at this point if you're a POC, especially a (young?) black male (but this also applies to (young?) brown males like myself). Don't run away from the officer. Don't run towards the officer. Don't "look suspicious". Don't own a gun (since declaring it while being a POC will heighten tensions). Don't be disrespectful. Don't get angry (no matter what). Don't talk back to the officer. Comply with everything the officer says. None of this helped Mr. Castille. He was still shot. Except wait…he owned a gun. Oh, but it was registered and he declared it to the officer <b>as is required by law</b>. This whole thing is sickening because he did everything the officer asked him to, got shot anyway, and then the jury <b>acquitted</b> the officer of all charges. They watched the video filmed right after the officer murdered Mr. Castille and said "eh". They watched the dashcam video (that was newly released after the trial) and said "There's still probable cause for the officer to have been afraid for his life". My only question is: what could he have done? If what Philando did &mdash; following the rules, declaring the firearm <b>he was licensed to carry</b> as per the law, remaining calm, and reaching for his ID &mdash; was enough to get him murdered, what could he have done to prevent the officer from shooting him? I think changing his skin color would have helped. And that is the sickening reality of racism in America today.
      </p>
      <h2>
	Discussion of the AHCA (March, 14, 2017)
      </h2>
      <p>
	People <b>will</b> lose coverage under the AHCA. And note that all of these numbers are relative to current coverage levels (not absolute numbers).
      </p>
      <p>
	Donald Trump has come out <a href="https://www.newsmax.com/Politics/Trump-AHCA-Healthcare/2017/03/11/id/778177/">in favor</a> of this bill. Let us look at the promises he made. Interestingly, healthcare doesn't appear anywhere on the <a href="https://www.whitehouse.gov/america-first-energy">Issues section</a> of the official Whitehouse page and the issues pages have been taked off of the Donald Trump <a href="https://donaldjtrump.com">website</a>, so I will have to rely on <a href="https://assets.donaldjtrump.com/Healthcare_Reform.pdf">this archive</a> that's buried somwhere within the assets folder of his website. Let's take a look.
      </p>
      <p>
	According to <b>him</b> (in that document): "By following free market principles and working together to create sound public policy that will broaden healthcare access, make healthcare more affordable and improve the quality of the care available to all Americans." That is his ideal system. But as the <a href="https://www.cbo.gov/publication/52486">CBO estimates</a> show, this legislation (which he supports) will <b>reduce</b> healthcare access and average premiums will most likely go up.
      </p>
      <p>
	That being said, does this legislation hit on all the points that he advocates in that PDF? I'm using <a href="https://www.speaker.gov/general/american-health-care-act-fact-sheet">this fact sheet</a> as a source.
      </p>
      <ul>
	<li>
	  Repeal Obamacare &mdash; check.
	</li>
	<li>
	  Allow sale of insurance across state lines &mdash; nope.
	</li>
	<li>
	  Block-grant Medicaid (that's what "giving states greater flexibility" means) &mdash; check.
	</li>
	<li>
	  Enhance HSAs &mdash; check.
	</li>
	<li>
	  Deduct premiums from taxes &mdash; nope.
	</li>
	<li>
	  Price transparency &mdash; nope.
	</li>
	<li>
	  Remove barriers to entry for drug providers &mdash; nope.
	</li>
      </ul>
      <p>
	So not <b>only</b> does it explicitly do the opposite of what Trump promised (according to <b>his own words</b>), it <b>also</b> doesn't do many of the things he advocated for. Even if you're Donald Trump, this is a terrible bill.
      </p>
      <p>
	Also note that I only used an external source <b>once</b> in this post (to show that he's come out in favor of the bill). This is all based entirely on what he has advocated for as well as the actual text of the bill (and the promises of its most vocal supporter).
      </p>
      <h2>
	What a Trump presidency might mean (November 9, 2016)
      </h2>
      <p>
	I've seen a lot of posts going around about what a Trump presidency might mean for them and I wanted to jump in a bit. This is going to be a fairly long post, but I feel I need to just kind of get this out there. I've mostly been posting sarcastic stuff or exaggerated stuff about Trump's victory. But I wanted to honestly discuss what the results of this election tell me.
      </p>
      <p>
	The one silver lining is that Clinton won the popular vote. So yes, she won the popular vote and lost the electoral vote.
      </p>
      <p>
	I would like to think that I and people like me (people with brown skin) are welcome in this country. But with the election of Trump, I'm not so sure. Pretty much half the country seems to endorse the rhetoric that "Muslims" (read: people with brown and black skin) should be placed under increased surveillance in this country. Pretty much half the country seems to endorse the rhetoric that "Muslims" should not be trusted. Of course my friends all know me as an atheist Hindu, but would a cop on the street be able to tell that? Would a random person on the street be able to tell that? If I were not already in my PhD program, I would very likely consider moving to a different country &mdash; Canada, most likely. I know many people say this as an election joke, but given what this country has stated about me and people who look like me? I would very much like to act on that right now.
      </p>
      <p>
	I am fearful for my LGBTQ friends who will have to deal with the ramifications of a Trump/Pence ticket. Pence has been <b>virulently</b> anti-LGBTQ, and Trump hasn't been much better. Pence supports conversion therapy, which is basically electric fucking torture, to "cure the gays". Trump has said he'll put people on SCOTUS who will overturn marriage equality. But again, I feel this needs to be looked at in a broader context. It was quite clear throughout the campaign that Trump and Pence were not pro-LGBTQ. So what does it say when this country elected these guys to lead it? What does it say about the <b>culture</b> of this country when Trump/Pence can run on this platform and win <b>handily</b>?
      </p>
      <p>
	I could go on and on, but I think you get the idea. I understand that there are probably many people who voted for Trump out of sheer hate for Clinton. And you know what? I actually kinda get that. But whether or not you <b>meant</b> to endorse his platform and positions by voting for him, that's exactly what you did. And at this point, I'm not sure how I can still live in this country while recognizing that so much of the country cares so much about voting against someone who's corrupt that they voted in...someone who's corrupt (in a different sense) <b>and</b> strictly worse for LGBTQ people, brown Americans, women, Latino Americans, and so on and so forth.
      </p>
      <p>
	Again, I understand that there are quite a few people who voted for Trump out of sheer hate for Clinton. But now I'm begging you. Please, stand with your LGBTQ friends and family against bigotry. Please, stand with your brown friends and family (yes, there are brown people who voted for Trump) against bigotry and stereotyping. Please, stand with your Latino friends and family against bigotry and stereotyping.
      </p>
      <p>
	Show me that this is merely a repudiation of Clinton, not an endorsement of Trump.
      </p>
      <h2>
	Free trade and colonialism (July 26, 2016)
      </h2>
      <p>
	There are many arguments against free trade deals that we've seen.
      </p>
      <p>
	One of the most popular is that they bleed jobs from here and ship them overseas. While that is <b>somewhat</b> true, as has been pointed out many times, automation is a much bigger factor in the loss of many manufacturing jobs, both here and abroad. Another one applies specifically to the TPP and concerns privacy rights, IP rights, and letting corporations usurp government regulations by challenging them in a "court".
      </p>
      <p>
	But I want to address today another objection, one that I haven't seen much. One of the standard arguments for free trade deals is that they will "create jobs", if not here, then at least in other countries. They will help them "develop", they say. They will "lift people out of poverty". They will help them make use of their resources and labor.
      </p>
      <p>
	It may not look like it, but it's a rehashing of the same old tired argument that white people (or countries and companies run by white people) will come in to "save" the poor savages and help them "develop". When you say that these multinational corporations will bring jobs and bring technology, it implies that the native people are not able to do it on their own. It implies that they're not able to actually make anything, that they need guidance from the generous white people.
      </p>
      <p>
	When trade deals are used to justify multinational corporations swooping in, undercutting any local businesses that exist, and tying all local labor to their particular model and particular way of doing things, those same deals prevent any significant local industries from developing to compete. Those same deals prevent local talent from flourishing. Those same deals allow multinational corporations to act like parasites, sucking any talent that exists from the local people and preventing any real industry from developing there.
      </p>
      <p>
	This then further justifies the narrative that those people are incapable of doing anything and thus they need multinational corporations to come in to provide the technology and everything else, and that those people are only good as laborers.
      </p>
      <p>
	And then, we look at those countries and think "Why...how backward those countries are!". No shit, Sherlock. If you have outside companies coming in, destroying any local industries that exist, sucking all the labor out of the locals, and keeping most of the profit, <b>of course</b> there won't be much money to really develop anything local. Of <b>course</b> there won't be any money to feed towards infrastructure and scientific progress and god knows how many other things.
      </p>
      <p>
	Not to mention that many times those same corporations are as much drivers of corruption as they are victims.
      </p>
      <p>
	Trade is good. Obviously not everyone can produce everything and be self-sustaining. But we <b>have</b> to place limits on how corporations take advantage of resources, including labor. And we <b>have</b> to realize that "free trade" is intimately linked to the colonialist attitudes that preceded it.
      </p>
      <p>
	#WhiteMansBurden
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
