<!DOCTYPE html>
<html>
  <head>
    <title>
      My Photos
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" href="../main.css" rel="stylesheet"/>
    <?php
       $URL = $_SERVER["SERVER_NAME"];
       print '<base href="http://'.$URL.'" />';
    ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <?php
       $images = array();
       $captions = array();
       $handle = opendir('.');
       while(false != ($entry = readdir($handle))) {
         if(strpos($entry, ".jpg") !== false) {
           $images[] = $entry;
           $captions[] = substr($entry, 0, -4);
         }
       }
       sort($images);
       sort($captions);
       $max = count($images);
       $img = 0;
       if($_POST["img"] != null) {
         $img = $_POST["img"];
       }
       $prev = $img - 1;
       $next = $img + 1;
       if($prev < 0) {
         $prev = $max + $prev;
       }
       if($next >= $max) {
         $next = $max - $next;
       }
       $imgurl = strtr($images[$img], array(' ' => '%20'));
       print '<div id="content">';
       print '<div id="picture">';
       print '<form name="f1">';
       $size = getimagesize($images[$img]);
       if($size[0] < $size[1]) {
         print '<div id="vimage">';
       }
       if($size[0] >= $size[1]) {
         print '<div id="himage">';
       }
       print '<a id="photo_url" href="photos/'.$imgurl.'" target="_blank" type="image/jpeg">';
       print '<img id="photo" alt="Loading photo..." src="photos/'.$imgurl.'" class="photogallery"/>';
       print '</a>';
       print '</div>';
       print '<br />';
       print '<div id="caption">'.$captions[$img].'</div>';
       print '<br />';
       print '<button type="submit" name="img" value="'.$prev.'" formmethod="post"> &lt;&lt; </button>';
       print '<button type="submit" name="img" value="'.$next.'" formmethod="post"> &gt;&gt; </button>';
       print '</form>';
       print '</div>';
       print '<hr />';
       include('../footer.html');
       print '</div>';
       ?>
  </body>
</html>
